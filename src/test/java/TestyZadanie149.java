import com.company.Zadanie149.Zadanie149;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestyZadanie149 {
    Zadanie149 zadanie149 = new Zadanie149();

    @Test
    public void sprawdzenieMetodyDlaPoprawnychWartosci() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(2, 10, 5);
        Assertions.assertTrue(wynik);
    }

    @Test
    public void sprawdzenieGdyLiczbaJestPrzedPrzedzialem() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(0, 10, -1);
        Assertions.assertFalse(wynik);
    }

    @Test
    public void sprawdzenieGdyLiczbaJestZaPrzedzialem() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(0, 10, 11);
        Assertions.assertFalse(wynik);

    }

    @Test
    public void sprawdzenieGdyPrzedzialJestBledny() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(5, 2, 4);
        Assertions.assertFalse(wynik);

    }

    @Test
    public void sprawdzenieGdyStartIStopSaRowne() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(0, 0, 5);
        Assertions.assertFalse(wynik);

    }

    @Test
    public void sprawdzenieGdyWszystkieWartosciSaRowne() {
        boolean wynik = zadanie149.czyLiczbaJestWPrzedziale(3, 3, 3);
        Assertions.assertTrue(wynik);
    }
}
