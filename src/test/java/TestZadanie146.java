import com.company.Zadanie146;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie146 {
    //dodanie adnotacji testowej
    @Test
    public void czyZwzracaLiczby() {
        double wynik = Zadanie146.zwrocKwotePoOpodatkowaniu(100, 23);//jakie dane wrzucamy
        Assertions.assertEquals(123.0, wynik);   //jakie powinien byc wynik
    }

    @Test
    public void sprawdzCoJesliPodatekJestUjemny() {
        double wynik = Zadanie146.zwrocKwotePoOpodatkowaniu(100, -23);
        Assertions.assertEquals(100, wynik);
    }

    @Test
    public void sprawdzCoJesliPodatekJestPowyzej100() {
        double wynik = Zadanie146.zwrocKwotePoOpodatkowaniu(100, 100);
        Assertions.assertEquals(200, wynik);
    }

    @Test
    public void sprawdzCoJesliPodatekPowyzej100() {
        double wynik = Zadanie146.zwrocKwotePoOpodatkowaniu(100, 101);
        Assertions.assertEquals(100, wynik);
    }

    @Test
    public void sprawdzCojJesliPodatekRowny0() {
        double wynik = Zadanie146.zwrocKwotePoOpodatkowaniu(100, 0);
        Assertions.assertEquals(100, wynik);
    }
}
