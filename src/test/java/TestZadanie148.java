import com.company.Zadanie148.SumaElementow;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie148 {

    private SumaElementow sumaElementow = new SumaElementow();

    @Test
    public void sprawdzenieDlaPoprawnegoZakresuLiczb() {
        int[] tablica = {1, 2, 3, 4, 5, 6};
        Integer wynik = sumaElementow.ileElementowZsumowac(tablica, 9);
        Assertions.assertEquals(Integer.valueOf(4), wynik);
    }

    @Test
    public void sprawdzenieJesliNieZnalezionoLiczby() {
        int[] tablica = {1, 2, 3, 4};
        Integer wynik = sumaElementow.ileElementowZsumowac(tablica, 11);
        Assertions.assertNull(wynik);
    }

    @Test
    public void sprawdzenieJesliTablicaJestPusta() {
        int[] tablica = {};
        Integer wynik = sumaElementow.ileElementowZsumowac(tablica, 11);
        Assertions.assertNull(wynik);
    }

    @Test
    public void sprawdzenieJesliPierwszaLiczbaSpelniaZalozenie() {
        int[] tablica = {5, 6, 3, 4, 5};
        Integer wynik = sumaElementow.ileElementowZsumowac(tablica, 4);
        Assertions.assertEquals(Integer.valueOf(1), wynik);
    }

    @Test
    public void sprawdzenieJesliTablicaJestNull() {
//        int[] tablica = {5, 2, 3, 4, 5};
        Integer wynik = sumaElementow.ileElementowZsumowac(null, 1);
        Assertions.assertNull(wynik);
    }

}
