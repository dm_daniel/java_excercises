import com.company.Zadanie150.Zadnie150;
import com.sun.xml.internal.bind.v2.TODO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie150 {

    Zadnie150 zadnie150 = new Zadnie150();


    @Test
    public void czyMetodaDzialaDlaPoprawnychWartosci() {
        int spodziewanaDlugosc = 4;
        int[] tablicaSpodziewana = {10, 11, 12, 13};   //tablica spodziewana
        int[] wynik = zadnie150.zwrocTablice(spodziewanaDlugosc);
        Assertions.assertEquals(spodziewanaDlugosc, wynik.length);
        Assertions.assertArrayEquals(tablicaSpodziewana, wynik);
    }

    @Test
    public void czyZwrociPustaTabliceGdyPrzekaze0() {
        int[] tablicaSpodziewana = {};
        int[] wynik = zadnie150.zwrocTablice(0);
        Assertions.assertArrayEquals(tablicaSpodziewana, wynik);
    }

    @Test
    public void coZwrociDlaWartosciUjemnych() {
        //porownaj z wyrzuceniem wyjątku
        Assertions.assertThrows(NegativeArraySizeException.class,       //obsluga blędu ujemny rozmiar tablicy
                //wyrażenie lambda, programowanie funkcyjne
                () -> {
                    zadnie150.zwrocTablice(-1);
                });
    }
//TODO poprawic po zajeciach z JVM
//    @Test
//    public void coZwrociDlaWartosciMaxymalnej() {
//        int spodziewanaDlugosc = Integer.MAX_VALUE;
//        int[] wynik = zadnie150.zwrocTablice(spodziewanaDlugosc);
//        Assertions.assertThrows(OutOfMemoryError.class,
//                () -> zadnie150.zwrocTablice(spodziewanaDlugosc));
//        Assertions.assertEquals(spodziewanaDlugosc, wynik.length);
//    }
}
