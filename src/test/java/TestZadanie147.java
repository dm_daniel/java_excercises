import com.company.Zadanie147.Kalkulator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie147 {
    Kalkulator kalk = new Kalkulator();

    //Testy dla dodawania 2 liczb
    @Test
    public void sprawdzCzyDodajePoprawnie() {
        Integer wynik = kalk.dodawanieDwochLiczb(5, 5);
        Assertions.assertEquals(Integer.valueOf(10), wynik);
    }

    @Test
    public void sprawdzCzyDodajePoprawnieJesliPierwszaWartoscJestNullem() {
        Integer wynik = kalk.dodawanieDwochLiczb(null, 5);
        Assertions.assertEquals(Integer.valueOf(5), wynik);
    }

    @Test
    public void sprawdzCzyDodajePoprawnieJesliDrugaWartoscJestNullem() {
        Integer wynik = kalk.dodawanieDwochLiczb(8, null);
        Assertions.assertEquals(Integer.valueOf(8), wynik);
    }

    @Test
    public void sprawdzCzyDodajePoprawnieJesliDwieWartosciSaNullami() {
        Integer wynik = kalk.dodawanieDwochLiczb(null, null);
        Assertions.assertNull(wynik);       //spodziewamy się wartości nullowej w tym miejscu
    }

    @Test
    public void sprawdzCzyDodajePoprawnieJesliLiczbySaOgromne() {
        Integer wynik = kalk.dodawanieDwochLiczb(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Assertions.assertEquals(Integer.valueOf(-2), wynik);
    }

    //testowanie drugiej metody dla sumowania wielu liczb
    @Test
    public void sprawdzCzyPoprawnieSumujeWieleLiczb() {
        Integer wynik = kalk.dodawanieWieluLiczb(5, 10, 15);
        Assertions.assertEquals(Integer.valueOf(30), wynik);
    }

    @Test
    public void sprawdzCoGdyTablicaJestPusta() {
        Integer wynik = kalk.dodawanieWieluLiczb(null);
        Assertions.assertNull(wynik);
    }

    @Test
    public void sprawdzCoGdyNicNiePrzekazuje() {
        Integer wynik = kalk.dodawanieWieluLiczb();
        Assertions.assertEquals(Integer.valueOf(0), wynik);
    }
    //Testy dla odejmowania
    @Test
    public void sprawdzCzyDobrzeOdejmuje(){
        Integer wynik=kalk.odejmowanieDwochLiczb(10,10);
        Assertions.assertEquals(Integer.valueOf(0),wynik);
    }
    @Test
    public void sprawdzGdyNULL1(){
        Integer wynik=kalk.odejmowanieDwochLiczb(null,10);
        Assertions.assertEquals(Integer.valueOf(-10),wynik);
    }
    @Test
    public void sprawdzGdyNULL2(){
        Integer wynik=kalk.odejmowanieDwochLiczb(50,null);
        Assertions.assertEquals(Integer.valueOf(50),wynik);
    }
    @Test
    public void sprawdzObydwieSaNULL(){
        Integer wynik=kalk.odejmowanieDwochLiczb(null,null);
        Assertions.assertNull(wynik);
    }
}
