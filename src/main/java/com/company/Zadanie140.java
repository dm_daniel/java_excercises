package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

/*
*ZADANIE #140*
Utwórz metodę która przyjmuje tablicę liczb i szuka największej z nich.
W pliku tekstowym ma być parametr od którego zależy czy cokolwiek będzie wyświetlane na konsoli czy nie.

Jeśli *tak*, metoda powinna *wyświetlić* zawartość całej tablicy oraz *wyświetlić* znalezioną liczbę*.
Największa liczba to:  15
Zbiór: [1, 4, 2, -9, 1, 15]

Jeśli *nie*, na konsoli nie powinien pojawić się żaden napis.
 */
public class Zadanie140 {

    public static void main(String[] args) {

        int[] tab = new int[]{1, 4, 2, 100, -9, 1, 15};
        if (czyWyswietlacInfo()) {
            System.out.println(zwracanieMax(tab));
            System.out.println(Arrays.toString(tab));
        }

    }

    private static int zwracanieMax(int[] tablica) {
        int max = tablica[0];

        for (int liczba : tablica) {
            if (liczba > max) {
                max = liczba;
            }
        }
        return max;
    }

    private static boolean czyWyswietlacInfo() {
        try {
            FileReader reader = new FileReader("pliki/Zadanie140.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            return !line.isEmpty();     //jeśli linia jest niepusta zwracam true
        } catch (Exception e) {
            System.out.println("pusty plik lub coś innego");
            return false;
        }
    }
}
