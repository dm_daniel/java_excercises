package com.company.zadanie162WzorzecBuilder;

public class Zadanie162 {
    public static void main(String[] args) {
        User user1 = new User.UserBuilder()
                .setName("Marek")
                .setSurname("Kolodziej")
                .setAge(35)
                .setGender(true)
                .build();       //buduje obiekt

        System.out.println(user1);
    }
}
