package com.company.zadanie162WzorzecBuilder;

public class User {
    //pola klasy bulildera musza być takie same jak klasy zewnetrznej
    //używamy metod klasy wewnętrznej
    private String name;
    private String surname;
    private int age;
    private boolean isMan;

    //przypisuje pola z buildera do pól user
    private User(UserBuilder userBuilder) {     //konstruktor, przyjmuje klasę wewnetrzną
        name = userBuilder.name;
        surname = userBuilder.surname;
        age = userBuilder.age;
        isMan = userBuilder.isMan;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", isMan=" + isMan +
                '}';
    }

    //wewnatrz klasy tworze kolejną klasę   (do static odwołuje się bez konieczności tworzenia obiektu).
    // klasy statyczne służą tylko dla klas wewnetrznych, dzięki temu mogę utworzyć obiekt klasy wewnętrznej bez tworzenia klasy zewnętrznej
//ta klasa wewnetrzna odpowiada za budowanie obiektu
    static class UserBuilder {       //muszą być te same pola
        private String name;
        private String surname = "brak";        //gdy nie dostanie żadnego nazwiska w mainie, wyswietli wartość domyślną "brak"
        private int age;
        private boolean isMan;

        //********tworzenie setterów w klasie wewnętrznej dla kolejnych pól
        UserBuilder setName(String name) {  //metoda zwraca user buildera
            this.name = name;
            return this;  //zwracam obiekt w którym jestem
        }

        UserBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        UserBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        UserBuilder setGender(boolean isMan) {
            this.isMan = isMan;
            return this;
        }
        //*********

        User build() {  //metoda budująca,ostatnia którą wywołujemy.(nie musi się nazywać build,[z kad pobrać,dokąd pobrać...])
            return new User(this);  //zwraca nowego użytkownika z wartościami które podaliśmy
        }
    }
}
