package com.company;
/*
*ZADANIE #54*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca sumę elementów z pominięciem największego.
> dla `[1, 2, 3, 10, 4, 5, 6, 11]`
> zwróci `31` (ponieważ pomija `11`)
 */

public class Zadanie54 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 1, 2, 9, 6};
        System.out.println(zwrocSumeElementow(tab));
    }

    static double zwrocSumeElementow(int[] tablica) {
        int max = wartoscNajwieksza(tablica);
        double liczbaElementow = 0.0;
        int sumaElementow = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] != max) {
                liczbaElementow++;
                sumaElementow += tablica[i];
            }
        }
        return sumaElementow / liczbaElementow;
    }

    static int wartoscNajwieksza(int[] tablica) {
        int max = tablica[0];

        for (int element : tablica) {
            if (element > max) {
                max = element;
            }
        }
        return max;


    }
}
                
