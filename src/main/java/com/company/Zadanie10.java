package com.company;
/*
*ZADANIE #10*
Utwórz metodę, do której przekazujesz jeden parametr i zwracana jest informacja czy liczba jest większa (bądź równa) od `20`
>Czyli dla `6` zwróci `false`,
>
>a dla `22` zwróci `true`
 */

public class Zadanie10 {
    public static void main(String[] args) {
        System.out.println(trueOrFalse(5));
        System.out.println(trueOrFalse(20));
        System.out.println(trueOrFalse(50));
        System.out.println();
        System.out.println(metoda2(5));
        System.out.println(metoda2(20));
        System.out.println(metoda2(50));

        int liczba = 1;
        boolean wynik = trueOrFalse(liczba);
        if (wynik) {
            System.out.println(" Liczba " + liczba + "większa od 20");
        } else {
            System.out.println("Liczba " + liczba + " mniejsza od 20");
        }

    }

    static boolean trueOrFalse(int x) {
        if (x >= 20) {
            return true;
        } else {
            return false;
        }
    }

    static boolean metoda2(int x) {
        return x >= 20;
    }
}

