package com.company.zadanie173HashcodeEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Zadanie173 {
    public static void main(String[] args) {
        Student student1 = new Student(1, "Janek");
        Student student2 = new Student(1, "Janek");

        System.out.println("Student hashcode1 = " + student1.hashCode());
        System.out.println("Student hashcode2 = " + student2.hashCode());
        System.out.println("Czy 2 obiekty są równe?: " + student1.equals(student2));

        List<Student> list = new ArrayList<>();
        list.add(student1);
        list.add(student2);
        System.out.println("Rozmiar Listy: " + list.size());
        System.out.println("Czy Lista zawiera studenta:"
                + list.contains(new Student(1, "Janek")));

        Set<Student> set = new HashSet<>();
        set.add(student1);
        set.add(student2);
        System.out.println("Rozmiar Seta: " + set.size());
        System.out.println("Czy Lista zawiera studenta:"
                + set.contains(new Student(1, "Janek")));

        Student student3 = new Student(2, "Włodzimierz");
        list.add(student3);
        System.out.println("Rozmiar Listy z nowym studentem " + list.size());
        set.add(student3);
        System.out.println("Rozmiar Seta z nowym studentem " + set.size());

        System.out.println("Zawartość list: " + list);
        System.out.println("Zawartość seta: " + set);
    }
}
