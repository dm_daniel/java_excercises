package com.company.zadanie173HashcodeEquals;

import java.util.Objects;
import java.util.Random;

public class Student {
    private int id;
    private String name;
    private int age;
    private int yearOfStudy;
    private int isStudent;

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

//    @Override
//    public int hashCode() {
//        //liczba pierwsza
//        int hash = 7;
//        //przemnażamy min przez 31 zeby uniknąć duplikatów
//        //hash to unikatowy numer wyliczony na bazie pól
//        hash = 31 * hash + id;
//        hash = 31 * hash + (name == null ? 0 : name.hashCode());
//        return hash;
//    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == this) {     //czy ten przekazany jako parametr jest rowny z tym ktory chce porównać
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (obj instanceof Student) {
//            Student student = (Student) obj;        //rzutowanie obj na Student
//            //porównanie identyfikatorów i imion ze sobą
//            return student.id == this.id && student.name.equals(this.name);
//        } else {
//            return false;
//        }
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (getId() != student.getId()) return false;
        if (age != student.age) return false;
        if (yearOfStudy != student.yearOfStudy) return false;
        if (isStudent != student.isStudent) return false;
        return getName() != null ? getName().equals(student.getName()) : student.getName() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + yearOfStudy;
        result = 31 * result + isStudent;
        return result;
    }
}

