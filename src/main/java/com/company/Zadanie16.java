package com.company;
/*
Utwórz metodę, do której przekazujesz trzy parametry - pierwsze dwa to jest przedział. Metoda powinna zwrócić
informację czy trzeci parametr znajduje się w tym przedziale
Dla `1, 10, 5` zwróci `true`
>
> Dla `1, 10, 15` zwróci `false`
>
> Dla `-10, 0, 3` zwróci `false`
>
> Dla `-10, 10, 0` zwróci `true`
>
> Dla `4, 8, 4` zwróci `true`
>
> Dla `3, 3, 3` zwróci `true`
 */

public class Zadanie16 {
    public static void main(String[] args) {
        System.out.println(czyWPrzedziale(1,10,5));
        System.out.println(czyWPrzedziale(1,10,15));
        System.out.println(czyWPrzedziale(-10,0,3));
        System.out.println(czyWPrzedziale(-10,10,0));
        System.out.println(czyWPrzedziale(4,8,4));
        System.out.println(czyWPrzedziale(3,3,3));

    }
    static boolean czyWPrzedziale(int pierwsza,int druga,int trzecia){
        return trzecia<=druga && trzecia>=pierwsza;

    }
}
