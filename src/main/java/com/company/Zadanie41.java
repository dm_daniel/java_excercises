package com.company;
/*
*ZADANIE #41*
Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
> Dla `4` zwróci: `[10, 11, 12, 13]`
>
> Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
 */

import java.util.Arrays;

public class Zadanie41 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(zwrocTab(6)));
    }

    static int[] zwrocTab(int rozmiar) {

        int[] tablica = new int[rozmiar];
        int wartosc = 10;
        for (int pozycja = 0; pozycja < rozmiar; pozycja++) {
            tablica[pozycja] = wartosc;     // lub tablica[pozycja]=pozycja + 10 i wtedy nie potrzebujemy deklarowac wartosci
            wartosc += 1;
        }

        return tablica;
    }

}
