package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Zadanie100 {
    /*
    *ZADANIE #100*
Utwórz metodę, która przyjmuje listę (`List`)
np. `ArrayList` elementów typu `String` a następnie zwraca listę w odwróconej kolejności.
     */
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>(Arrays.asList("Ala", "ma", "kota"));
        System.out.println(odwrocElementyListy(lista));
    }

    static List<String> odwrocElementyListy(List<String> lista) {
        Collections.reverse(lista);

        return lista;
    }
}
