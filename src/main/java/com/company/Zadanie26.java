package com.company;
/*
Utwórz metodę, do której przekazujesz jeden parametr i *zwraca* sumę wszystkich elementów od `1` do podanej liczby
> dla `3` zwróci `1 + 2 + 3 = 6`
>
> dla `5` zwróci `15` bo `1 + 2 + 3 + 4 + 5 = 15`
>
> dla `11` zwróci `66` bo `1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 = 66`
 */

public class Zadanie26 {
    public static void main(String[] args) {
        System.out.println(sumaElementow(3));
        System.out.println(sumaElementow(5));
        System.out.println(sumaElementow(11));


    }

    static int sumaElementow(int parametr) {
        int suma = 0;
        for (int x = 1; x <= parametr; x++) {
            suma += x;     //suma zwiekszam o x    , s=s+x
        }
        return suma;
    }
}
