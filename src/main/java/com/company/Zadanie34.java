package com.company;
/*
*ZADANIE #34*
Utwórz metodę, która przyjmuje 3 parametry i wyświetla odpowiednie wartości.
Pierwsze dwa to krańce przedziału. Trzeci to “skok” pomiędzy liczbami.
Gdy przekazany przedział będzie błędny, metoda ma wyświetlić komunikat.
> Dla `1, 10, 3` wyświetli `1, 4, 7, 10`
 */

public class Zadanie34 {
    public static void main(String[] args) {
        krancePrzedzialu(5, 1, 3);

    }

    static void krancePrzedzialu(int start, int stop, int skok) {
        if (start > stop) {
            System.out.println("Podano bledny przedzial");
            return;
        } else if (stop - start < skok) {
            System.out.println("skok jest za duży");
            return;
        }
        for (int i = start; i <= stop; i = i + skok) {
            System.out.println(i);
        }


    }
}


