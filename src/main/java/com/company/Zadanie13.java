package com.company;
/*
Utwórz metodę, do której przekazujesz trzy parametry a ona *zwróci* największy z nich z nich.
>Czyli dla `1, 2, 3` zwróci `3`,
>
>a dla `3, 23, 10` zwróci `23`
 */

public class Zadanie13 {
    public static void main(String[] args) {
        System.out.println(max(28, 10, 5));
        System.out.println(max(1, 28, 5));
        System.out.println(max(1, 2, 5));
    }

    static int max(int b, int c, int d) {
        if (b >= c && b >= d) {                 // ||lub  &&i
            return b;
        } else if (c >= b && c >= d) {
            return c;
        } else {
            return d;
        }
    }
}