package com.company;

/*Utwórz metodę, która przyjmuje wartość produktu oraz procent podatku jaki należy naliczyć, a następnie zwraca kwotę po opodatkowaniu.
        > Dla przekazanych parametrów `100, 23`,
        >
        >powinno zwrócić `123`
        */
public class Zadanie9 {
    public static void main(String[] args) {

        double cena = cenaBrutto(100, 23);
        System.out.println(cena);

        double cena2 = cenaBrutto(500, 23);
        System.out.println(cena2);

    }

    static double cenaBrutto(int wartosc, int podatek) {
        return wartosc + wartosc * (podatek * 0.01);
    }
}
