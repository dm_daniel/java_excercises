package com.company;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
*ZADANIE #139*
Utwórz metodę która pobiera od użytkownika login i hasło,
a następnie sprawdza czy podane dane są prawidłowe (weryfikując to z plikiem, w którym wartości rozdzielone są spacją)
admin admin123
mojLogin 123456
 */
public class Zadanie139 {
    public static void main(String[] args) {
        try {
            List<Pair<String, String>> list = odczytajPlik("pliki/Zadanie139.txt");
            System.out.println(czyPodaneDaneSaPrawidlowe(list));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Pair<String, String>> odczytajPlik(String sciezka) throws IOException {
        //lista par
        List<Pair<String, String>> lista = new ArrayList<>();
        FileReader reader = new FileReader(sciezka);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String linia = bufferedReader.readLine();

        while (linia != null) {
            String[] tablica = linia.split(" ");
            if (tablica.length == 2) {
                Pair<String, String> p = new Pair<>(tablica[0], tablica[1]);
                lista.add(p);
            }
            linia = bufferedReader.readLine();
        }
        return lista;
    }

    private static boolean czyPodaneDaneSaPrawidlowe(List<Pair<String, String>> list) {
        Scanner scanner = new Scanner(System.in);
        boolean czyPoprawnie = false;   //boolean ma domyślnie wartość false

        for (int ileRazy = 0; ileRazy < 3; ileRazy++) {

            System.out.println("Podaj login: ");
            String login = scanner.nextLine();
            System.out.println("Podaj hasło: ");
            String haslo = scanner.nextLine();
            czyPoprawnie = (sprawdzenieLoginHaslo(list, login, haslo));
            if (czyPoprawnie) {
                break;
            }
            System.out.println("Błędne dane, podaj Ponownie...");
        }

        return czyPoprawnie;
    }

    private static boolean sprawdzenieLoginHaslo(List<Pair<String, String>> list, String login, String haslo) {
        for (Pair<String, String> stringPair : list) {
            if (stringPair.getKey().equals(login) && stringPair.getValue().equals(haslo)) {
                return true;

            }
        }
        return false;
    }
}
