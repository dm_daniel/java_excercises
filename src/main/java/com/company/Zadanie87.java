package com.company;

/*
*ZADANIE #87*
Utwórz metodę, która przyjmuje dwa parametry - pierwszy jest ciągiem znaków (typu `String`),
a drugi jest poszukiwaną literą (jako `char`).

Metoda ma zwrócić informację ile razy występuje podana litera (jako `int`)
 */
public class Zadanie87 {
    public static void main(String[] args) {
        System.out.println(ileRazyWystepujeZnak("Szedł Sasha suchą szosą", 's'));
    }

    static int ileRazyWystepujeZnak(String wyraz, char znak) {

        int iloscPowtorzen = 0;

        for (int pozycja = 0; pozycja < wyraz.length(); pozycja++) {
            if (wyraz.toLowerCase().charAt(pozycja) == znak) {
                iloscPowtorzen++;
            }
        }
        return iloscPowtorzen;
    }
}
