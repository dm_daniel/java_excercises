package com.company;

/*
*ZADANIE #144*
Sprawdź czy wyraz jest palindromem przy wykorzystaniu *rekurencji*.
https://pl.wikipedia.org/wiki/Palindrom#Polski (edited)
 */
public class Zadanie144 {
    public static void main(String[] args) {
        System.out.println(czyToPalindrom("Kajak"));
        System.out.println(zmianaToLowerCase("Kajak"));

    }

    private static boolean czyToPalindrom(String wyraz) {
        if (wyraz.length() == 0 || wyraz.length() == 1) {
            return true;
        } else if (wyraz.charAt(0) == wyraz.charAt(wyraz.length() - 1)) {
            return czyToPalindrom(wyraz.substring(1, wyraz.length() - 1));
        } else {
            return false;
        }
    }

    private static boolean zmianaToLowerCase(String wyraz) {
        return czyToPalindrom(wyraz.toLowerCase());
    }
}
