package com.company;

public class Zadanie2 {
    public static void main(String[] args) {        //metoda main

        System.out.println("Ala ma\b kota");    //backspace
        System.out.println("Ala ma\n kota");    //złamanie linijki
        System.out.println("Ala ma\t kota");    // tabulator
        System.out.println("Ala ma\\ kota");    // wstawienie \
        System.out.println("Ala ma\\\\ kota");    // wstawienie \\

        System.out.println("mam czerwone but\\y"); //sam dodaje ukośnik

        /* mam czerwone but\y */
    }

}
