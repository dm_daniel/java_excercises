package com.company.Zadanie167BlokiInicjalizacyjne;

class Samochod extends Pojazd {
    //pole statyczne w klasie piszemy z wielkiej
    final static Integer KOD_KRAJU = 52;

    {
        System.out.println("Blok inicjalizacyjny klasy Samochód");
    }

    static {
        System.out.println("STATYCZNY Blok inicjalizacyjny klasy Samochód  ");
    }

    {
        System.out.println("Blok inicjalizacyjny klasy Samochód Numer 2");
    }


    Samochod() {
        System.out.println("Konstruktor klasy Samochodu");
    }
}
