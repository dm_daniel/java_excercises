package com.company.Zadanie167BlokiInicjalizacyjne;

public class Zadanie167 {
    public static void main(String[] args) {

        Samochod samochod = new Samochod();     //najpierw odezwie się bloki statyczne,potem blok inicjalizacyjny klasy(wywoła się przed konstruktorem),potem konstruktor pojazdd potem blok inicjalizacyjny klasy Samochód,potem konstruktor samochod, bo samochod dziedziczy po pojazd
        System.out.println(" \n\n\n");
        System.out.println(Samochod.KOD_KRAJU);     //przy odwołaniu się do wartości,
        System.out.println(Samochod.KOD_KRAJU);
        //statyczne bloki inicjalizacyjne wywołują się tylko 1 raz dla danej klasy! (jeśli wcześniej już się wywołały)
    }
}
