package com.company.Zadanie167BlokiInicjalizacyjne;

class Pojazd {
    //bloki inicjalizacyjne wykonują się 1 po drugim, przed konstruktorami, wywoływane są też przed konstruktorami
    //kolejność ma znaczenie
    //bloki statyczne używane są jako pierwsze
    {
        System.out.println("Blok inicjalizacyjny klasy Pojazd");
    }

    static {
        System.out.println("STATYCZNY Blok inicjalizacyjny klasy Pojazd  ");
    }

    {
        System.out.println("Blok inicjalizacyjny klasy Pojazd Numer 2");
    }

    Pojazd() {
        System.out.println("Konstruktor Klasy Pojazd");
    }
}
