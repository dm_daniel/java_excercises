package com.company;
/*
*ZADANIE #12*
Utwórz metodę, do której liczbe a ona wyświetla informacje czy przekazana wartość jest:
- mniejsza od 50
- równa 50
- większa od 50
 */

public class Zadanie12 {
    public static void main(String[] args) {
        metoda(10);
        System.out.println();
        metoda(50);
        System.out.println();
        metoda(100);

    }

    static void metoda(int x) {
        int liczba = 50;
        if (x < liczba) {
            System.out.printf("Liczba %s jest mniejsza niz %s", x, liczba);
        } else if (x == liczba) {
            System.out.printf("Liczba %s jest równa %s", x, liczba);
        } else {
            System.out.printf("Liczba %s jest wieksza niz %s", x, liczba);
        }

    }
}
