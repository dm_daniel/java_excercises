package com.company;

/*
*ZADANIE #141*
Wyświetl 5 razy napis `Hello World` przy wykorzystaniu *rekurencji*.
 */
public class Zadanie141 {
    private static int licznik = 0;

    public static void main(String[] args) {
        wyswietlNapis();
    }

    private static void wyswietlNapis() {
        licznik++;
        if (licznik <= 5) {
            System.out.println("HelloWorld " + licznik);
            //rekurencja
            wyswietlNapis();
        }
    }
}
