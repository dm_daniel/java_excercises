package com.company;
/*
*ZADANIE #127*
Utwórz metodę która przyjmuje niepustą liste lub tablicę
a następnie w *nieskończonej* pętli je wyświetla (zaczynając od pozycji `0`).
Pętla ma zostać przerwana po wyjściu poza zakres listy (tzn. po przekroczeniu jej rozmiaru). (edited)
 */

import java.util.ArrayList;
import java.util.List;

public class Zadanie127 {
    public static void main(String[] args) {

        int[] tab = {5, 6, 7, 8, 6, 5, 4, 6, 0, 1};
        wyswietlTablice(tab);

    }

    static void wyswietlTablice(int[] tablica) {
        int indeks = 0;
        while (true) {
            try {
                System.out.print(tablica[indeks++]);
            } catch (Exception e) {
                System.out.print(" osiągnięto koniec tablicy");
                break;
            }
        }
    }
}