package com.company;
/*
*ZADANIE #88*
Utwórz metodę, która przyjmuje ciąg znaków, a odwraca każdy z wyrazów i zwraca całość jako jeden napis.
> DLa `"ala ma kaca"` zwróci `"ala am acak"`
> DLa `"bo dzisiaj jest sobota"` zwróci `"ob jaisizd tsej atobos"`
 */

public class Zadanie88 {
    public static void main(String[] args) {
        System.out.println(odwracaWyrazy("Kotek"));
        System.out.println(odwracaSlowaWWyrazach("Ala ma kota"));
    }

    static String odwracaSlowaWWyrazach(String zdanie) {

        String noweZdanie = "";

        String[] wyrazy = zdanie.split(" ");
        for (int pozycjaWTablicy = 0; pozycjaWTablicy < wyrazy.length; pozycjaWTablicy++) {
            noweZdanie += odwracaWyrazy(wyrazy[pozycjaWTablicy]) + " ";
        }
        //trim przetnie ostatnią przerwę którą dodajemy w pętli
        return noweZdanie.trim();
    }

    static String odwracaWyrazy(String wyraz) {
        String nowyWyraz = "";
        for (int pozycjaLitery = 0; pozycjaLitery < wyraz.length(); pozycjaLitery++) {
            //dodajemy znak na początku a nie na końcu
            //charAt wyciaga znak z danej pozycji
            nowyWyraz = wyraz.charAt(pozycjaLitery) + nowyWyraz;
        }
        return nowyWyraz;
    }

}
