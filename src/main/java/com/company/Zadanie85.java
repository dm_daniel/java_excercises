package com.company;

/*
*ZADANIE #85*
Utwórz metodę, która przyjmuje imię i nazwisko a następnie zwraca w formie napisu login, który składa się z:
>- 3 liter imienia
>- 3 liter nazwiska
>- liczby, która jest sumą długości imienia i nazwiska

> Dla `"Maciej Czapla"`, zwróci `"maccza12"`
> Dla `"Anna Nowak"`, zwróci `"annnow9"`
> Dla `"Jan Kowalski"`, zwróci `"jankow11"`
 */
public class Zadanie85 {
    public static void main(String[] args) {

        System.out.println(zwrocLogin("Daniel Mikołajczyk"));
    }

    static String zwrocLogin(String nazwa) {

        String[] tablica = nazwa.split(" ");
        return (tablica[0].substring(0, 3)
                + tablica[1].substring(0, 3)
                + (tablica[0] + tablica[1]).length()).toLowerCase();        //lub nazwa.length -1

    }
}
