package com.company;

public class ZadanieRekrutacyjne1 {
    public static void main(String[] args) {
        //zrobiłem sam
//        System.out.println(solution(6000, 7000));

        //zrobione na zajeciach
        long A = 100;
        long  B = 5_000_000_000L;
        System.out.println(howManyTimesSquare(A, B));
    }

    //zrobiłem sam
    static int solution(int a, int b) {
        int licznik = 0;
        int bufor = 0;
        double x;

        etykieta1:
        for (double i = a; i <= b; i++) {
            bufor = 0;
            x = i;
            while (true) {
                if (Math.sqrt(x) % 1 == 0) {
                    bufor++;
                    x = Math.sqrt(x);
                    System.out.println(x);
                } else {
                    if (bufor > licznik) {
                        licznik = bufor;
                        bufor = 0;
                    }
                    continue etykieta1;
                }
            }
        }
        return licznik;
    }

    //zrobione na zajęciach
    static int howManyTimesSquare(long A, long B) {

        int MaxSquareCount = 0;

        for (long number = A; number <= B; number++) {
            int counter = squareRootCounter(number);

            if (counter > MaxSquareCount) {
                MaxSquareCount = counter;
            }
        }
        return MaxSquareCount;
    }

    static int squareRootCounter(long number) {
        int howManyTimesSqrt = 0;

        double sqrt = Math.sqrt(number);
        while (sqrt % 1 == 0) {
            howManyTimesSqrt++;
            sqrt = Math.sqrt(sqrt); //pierwiastek od poprzedniej wartosci
        }

        return howManyTimesSqrt;
    }
}
