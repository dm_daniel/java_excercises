package com.company;
/*
*ZADANIE #83*
Utwórz metodę, która przyjmuje wyraz, a następnie zwraca informacje czy zawiera on samogłoski czy nie.
 */

public class Zadanie83 {
    public static void main(String[] args) {        // main jest static wiec metody też muszą być statyczne żeby ich użyć

        String wyraz1 = "KAJAK";

        System.out.println(czyZawieraSamogloski(wyraz1));
        System.out.println(czyZawieraSamogloski2(wyraz1));
    }

    static boolean czyZawieraSamogloski(String wyraz) {

        String[] tablicaSamoglosek = new String[]{"a", "e", "i", "o", "u", "y"};
        for (String litera : tablicaSamoglosek) {
            if (wyraz.toLowerCase().contains(litera)) {
                return true;
            }
        }
        return false;
    }

    static boolean czyZawieraSamogloski2(String wyraz) {
        String wyrazZmniejszony = wyraz.toLowerCase();
        return wyrazZmniejszony.contains("a")
                || wyrazZmniejszony.contains("e")
                || wyrazZmniejszony.contains("i")
                || wyrazZmniejszony.contains("o")
                || wyrazZmniejszony.contains("u")
                || wyrazZmniejszony.contains("y");
    }

}
