package com.company;
/*
*ZADANIE #82*
Utwórz metodę, która przyjmuje ciąg znaków (gdzie wyrazy rozdzielone są znakiem spacjami) a następnie zwraca najdłuższy z nich
> Dla `"Ala ma kota."`, zwróci `"kota"` (gdyż jest najdłuższym wyrazem)
 */

public class Zadanie82 {
    public static void main(String[] args) {
        String s = "Ala ma kota.";
        System.out.println(zwrocNajdluzszy(s));
        System.out.println(zwrocNajdluzszy2(s));
    }

    static String zwrocNajdluzszy(String zdanie) {
        int najdluzszy = 0;
        String[] tablica = zdanie.split(" ");
        String result = "";
        for (String element : tablica) {
            if (element.length() > najdluzszy) {
                najdluzszy = element.length();
                result = element;
            }
        }
        return result;
    }

    static String zwrocNajdluzszy2(String zdanie) {     //krótsza wersja
        String[] tablica = zdanie.split(" ");
        String result = "";
        for (String element : tablica) {
            if (element.length() > result.length()) {
                result = element;
            }
        }
        return result;
    }


}

