package com.company.zadanie79;
/*
*ZADANIE #79*
Utwórz klasę `Rownanie` służącą do policzenia równania `a^2 + b^3 + c^4`. Klasa powinna zawierać:
>*pola:*
>* `a`, `b`, `c`
>
>*kostruktory:*
>* bezparametrowy
>* 3-parametrowy
>
>*metody:*
>* liczaca wartosc rownania
>* przyjmującą liczbę a następnie zwracająca informację (`boolean`) czy wartość równania przekroczyła podaną liczbą (jako parametr)
 */

import java.util.Scanner;


public class Zadanie79 {
    public static void main(String[] args) {
        //nowy obiekt klasy Scanner -- typ nazwa
        Scanner s = new Scanner(System.in);
        Rownanie rownanie = new Rownanie();
        System.out.print("A: ");
        rownanie.setA(s.nextInt()); //do pola zostanie wrzucona wartość która podam z klawiatury

        System.out.print("B: ");
        rownanie.setB(s.nextInt());

        System.out.print("C: ");
        rownanie.setC(s.nextInt());

        System.out.println(rownanie);

        System.out.println(rownanie.oblicz());

        rownanie.wyswietlRownanie();
        System.out.println(rownanie.czyPrzekroczyla(50));
    }

}
