package com.company.zadanie79;

public class Rownanie {

    private int a;
    private int b;
    private int c;

    //konstruktor bezparametrowy (nie znamy parametrow, będziemy je wpisywać
    Rownanie() {
    }

    Rownanie(int a, int b, int c) {
        this.a = a;     //weź z tego obiektu pole o nazwie a i przypisz do a
        this.b = b;
        this.c = c;
    }

    //settery - metody do ustawienia wartości(nadpisujemy wartości)
    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setC(int c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "Rownanie{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }

    double oblicz() {
        //metoda pow jest statyczna bo wywołuje ją na klasie Math. ( nie tworze nowego obiektu )
        return Math.pow(a, 2) + Math.pow(b, 3) + Math.pow(c, 4);        //moge przekazać do metody dane o większym zakresie...int zmieści się w double
    }

    void wyswietlRownanie() {
        System.out.printf("%s^%s + %s^%s + %s^%s = %s", a, 2, b, 3, c, 4, oblicz());
    }

    boolean czyPrzekroczyla(int liczba) {
        System.out.println();
        return liczba < oblicz();
    }
}
