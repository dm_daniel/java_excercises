package com.company;

/*
*ZADANIE #61*
Utwórz metodę, która przyjmuje conajmniej DWA parametry - boolean oraz dowolną ilość (większą od 0) liczb (typu `int`).
Gdy pierwszy parametr ma wartość `true` metoda zwraca największą przekazaną liczbą, a gdy `false` najmniejszą.
 */
public class Zadanie61 {
    public static void main(String[] args) {
        System.out.println(zwrocNajwiekszaLiczbe(true, 1, 2, 3, 4, 5, -1, -2, -3, -4, -5));
        System.out.println(zwrocNajwiekszaLiczbe(false, 1, 2, 3, 4, 5, -1, -2, -3, -4, -5));

    }

    static int zwrocNajwiekszaLiczbe(boolean czyNajwieksza, int... cyfry) {
        int max = cyfry[0];
        int min = cyfry[0];

        for (int element : cyfry) {
            if (element > max) {
                max = element;
            }
            if (element < min) {
                min = element;
            }
        }
        return czyNajwieksza ? max : min;

    }
}
