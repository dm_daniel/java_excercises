package com.company;
/*
Utwórz metodę, która wyświetli wszystkie liczby całkowite z przedziału `10` a `20`.
 */

public class Zadanie22 {
    public static void main(String[] args) {
        wyswietlCalkowite();
        System.out.println();
        whileWyswietlCalkowite();
        System.out.println();
        doWhileWyswietlCalkowite();

    }

    static void wyswietlCalkowite() {
        for (int i = 10; i < 21; i++) {
            System.out.print(i + " ");

        }

    }

    static void whileWyswietlCalkowite() {  //to samo z petla while
        int i = 10;
        while (i <= 20) {
            System.out.print(i + " ");
            i++;
        }

    }

    static void doWhileWyswietlCalkowite() { //to samo z petla do while
        int i = 10;
        do {
            System.out.print(i + " ");
            i++;

        } while (i <= 20);
    }
}
