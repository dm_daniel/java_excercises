package com.company.zadanie175WątkiKontoBankowe;

public class Konto {
    private int numer;
    private String wlascicel;
    private volatile int stanKonta;

    public Konto(int numer, String wlascicel, int stanKonta) {
        this.numer = numer;
        this.wlascicel = wlascicel;
        this.stanKonta = stanKonta;
    }

    public int getNumer() {
        return numer;
    }

    public String getWlascicel() {
        return wlascicel;
    }

    public int getStanKonta() {
        return stanKonta;
    }

    @Override
    public String toString() {
        return String.format("Właściciel: %s Numer: %s StanKonta: %s", wlascicel, numer, stanKonta);
    }

    synchronized void wplac(int kwota) {
        stanKonta += kwota;
    }

    synchronized void wyplata(int kwota) {
        if (kwota > stanKonta) {
            throw new IllegalArgumentException("Brak środków na koncie");   //zły przekazany argument,w konstruktorze daje wiadomość
        }
        stanKonta -= kwota;
    }

}
