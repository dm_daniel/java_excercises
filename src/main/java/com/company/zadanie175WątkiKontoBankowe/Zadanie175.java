package com.company.zadanie175WątkiKontoBankowe;

public class Zadanie175 {
    public static void main(String[] args) {
        method1();
    }

    private static void method1() {
        Konto konto = new Konto(1, "Daniel", 20000);
        final int ileRazy = 1000;
        final int kwota = 25;

        //jako parametr konstruktora wrzuciłem klase anonimową Runnable
        Thread wplac = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ileRazy; i++) {
                    konto.wplac(kwota);
                }
            }
        });

        Thread wyplac = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ileRazy; i++) {
                    konto.wyplata(kwota);
                }
            }
        });
        System.out.println(konto);

        wplac.start();
        wyplac.start();

        try {
            wplac.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            wyplac.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(konto);
    }

}
