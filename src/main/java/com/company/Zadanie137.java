package com.company;
/*
*ZADANIE #137*
Utwórz metodę która odczytuje zawartości ze Scannera (do momentu podania pustej linii)
i zapisuje wartości do pliku
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Zadanie137 {
    public static void main(String[] args) {
        try {
            zapisDoPliku();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void zapisDoPliku() throws IOException {
        Scanner scan = new Scanner(System.in);

        FileWriter writer = new FileWriter("pliki/Zadanie137.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(writer);


        while (true) {
            System.out.println("Wprowadz coś...");
            String linia = scan.nextLine();
            if (linia.equals("")) {
                break;
            } else {
                bufferedWriter.write(linia);
                bufferedWriter.newLine();
                //alternatywa
//                bufferedWriter.write("\n");
            }
        }
        //zrzuć elementy do pliku w danym momencie
//        bufferedWriter.flush();
        //zamknięcie pracy z plikiem I ZAPISANIE
        bufferedWriter.close();
        System.out.println("zakończono dodawanie danych...");
    }
}
