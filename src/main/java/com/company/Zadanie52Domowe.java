package com.company;

import java.util.Arrays;

public class Zadanie52Domowe {
    public static void main(String[] args) {
        int[] tablicaPierwsza = {  1, 2, 9, 6};
        int[] tablicaDruga = {     3, 4, 5, 5};
        int[] tablicaTrzecia = {1, 5, 8, 7, 9};
        int[] tablicaCzwarta = {2, 5, 7, 9, 3};

        int[] wynik = sumujTablice(tablicaPierwsza, tablicaDruga,
                tablicaTrzecia, tablicaCzwarta);
        System.out.println(Arrays.toString(wynik));

        int[] nowaTablica = usunPustePozycje(wynik);
        System.out.println(Arrays.toString(nowaTablica));

    }

    static int[] usunPustePozycje(int[] tablica) {
        int licznik = 0;
        for (int indeks = 0; indeks < tablica.length; indeks++) {
            if (tablica[indeks] == 0) {
                licznik++;
            } else {
                break;
            }
        }
        int[] nowaTablica = new int[tablica.length - licznik];
        for (int indeks = 0; indeks < nowaTablica.length; indeks++) {
            nowaTablica[indeks] = tablica[indeks + licznik];
        }
        return nowaTablica;
    }


    static int[] sumujTablice(int[] tablicaPierwsza, int[] tablicaDruga,
                              int[] tabicaTrzecia, int[] tablicaCzwarta) {

        int[] tablicaPomocnicza = {tablicaPierwsza.length - 1, tablicaDruga.length - 1,
                tabicaTrzecia.length - 1, tablicaCzwarta.length - 1};
        int[] tablicaWynikowa = new int[10];

        for (int indeks = tablicaWynikowa.length - 1; indeks > 0; indeks--) {
            int suma = tablicaWynikowa[indeks];
            if (tablicaPomocnicza[0] >= 0) {
                suma += tablicaPierwsza[tablicaPomocnicza[0]];
                tablicaPomocnicza[0]--;
            }
            if (tablicaPomocnicza[1] >= 0) {
                suma += tablicaDruga[tablicaPomocnicza[1]];
                tablicaPomocnicza[1]--;
            }
            if (tablicaPomocnicza[2] >= 0) {
                suma += tabicaTrzecia[tablicaPomocnicza[2]];
                tablicaPomocnicza[2]--;
            }
            if (tablicaPomocnicza[3] >= 0) {
                suma += tablicaCzwarta[tablicaPomocnicza[3]];
                tablicaPomocnicza[3]--;
            }
            if (suma > 9) {
                tablicaWynikowa[indeks] = suma % 10;
                tablicaWynikowa[indeks - 1] = suma / 10;
            } else {
                tablicaWynikowa[indeks] = suma;
            }
        }

        return tablicaWynikowa;
    }
}
