package com.company;
/*
*ZADANIE #53*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca różnicę pomiędzy największym a najmniejszym elementem.
 */

public class Zadanie53 {
    public static void main(String[] args) {
        int[] tablica = new int[]{15, 7, 4, 10, 1, 20};
        System.out.println(zwrocRoznice(tablica));
    }

    static int zwrocRoznice(int[] tablica) {
        int max = tablica[0];
        int min = tablica[0];

        for (int index = 0; index < tablica.length; index++) {
            if (tablica[index] > max) {
                max = tablica[index];
            }
            if (tablica[index] < min) {
                min = tablica[index];
            }
        }
        int roznica = max - min;
        return roznica;
    }

}
