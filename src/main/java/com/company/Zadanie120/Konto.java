package com.company.Zadanie120;

public class Konto {
    protected double stan;
    private String wlasciciel;
    private int numerKonta;
    private static int idKonta = 0;           //jeśli pole jest statyczne to przechowuje wartość przez caly czas działania programu

    public Konto(String wlasciciel) {
        this.wlasciciel = wlasciciel;
        numerKonta = idKonta++;           //uzytkownik nie nadaje numeru tylko sam będzie się przypisywał
    }

    @Override
    public String toString() {
        return String.format("Właścicielem konta (numer_konta: %s) jest : %s. Stan konta = %s",
                numerKonta, wlasciciel, stan);
    }


    double pokazStanKonta() {
        return stan;
    }

    void wplataNaKonto(double wplata) {
        stan += wplata;
    }

    boolean wyplataZKonta(double wyplata) {
        if (stan >= wyplata) {
            stan -= wyplata;
            return true;
        }
        return false;
    }

    //metoda statyczna w klasie GenericClass, wywołujemy ją na klasie GenericClass, a nie na obiektach klasy GenericClass(np osoba1..
    static void przelewanieSrodkow(Konto nadawca, Konto odbiorca, double kwotaPrzelewu) {   //metode statyczna wywolujemy na klasie
        if (nadawca.wyplataZKonta(kwotaPrzelewu)) {
            odbiorca.wplataNaKonto(kwotaPrzelewu);
        }
    }

    public int getNumerKonta() {
        return numerKonta;
    }

}
