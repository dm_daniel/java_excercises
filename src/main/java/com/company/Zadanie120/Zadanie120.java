package com.company.Zadanie120;
/*
*ZADANIE #120*
Utwórz klasę `Konto`, która zawiera pola `stan`, `numer`, `wlasciciel`
oraz metody umożliwiające wpłatę, wypłatę, przelewanie środków, sprawdzenie stastus.

Utwórz klasę `KontoOszczednosciowe` (która dziedziczy po `Konto`), które dodatkowo zawiera pole `procentOszczednosci`.

Utworz klasę `KontoFirmowe`  (która dziedziczy po `Konto`), które dodatkowo zawiera pole `oplataZaTransakcje`. (edited)
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie120 {
    public static void main(String[] args) {

        Konto konto1 = new Konto("Daniel");
        Konto konto2 = new Konto("Kamil");
        Konto konto3 = new Konto("Jarek");
        konto1.wplataNaKonto(200.0);

        boolean rezultat = konto1.wyplataZKonta(100);
        if (rezultat) {
            System.out.println("Tranzakcja sie powiodła");
        } else {
            System.out.println("Tranzakcja nie udana!!!");
        }

        Konto.przelewanieSrodkow(konto1, konto2, 15);
        System.out.println(konto1);
        System.out.println(konto2);
        System.out.println(konto3);
        System.out.println();
        KontoFirmowe kontoFirmowe = new KontoFirmowe("Dariusz", 0.2);
        kontoFirmowe.wplataNaKonto(1000);
        kontoFirmowe.wyplataZKonta(300);
        System.out.println(kontoFirmowe);
        System.out.println();
//        konto1.przelewanieSrodkow(konto1,konto2,5);
        Konto.przelewanieSrodkow(konto1, kontoFirmowe, 5);
        System.out.println(kontoFirmowe);
        System.out.println();
        Konto.przelewanieSrodkow(kontoFirmowe, konto1, 5);
        System.out.println(konto1);

        List<Konto> listaKont = Arrays.asList(
                konto1, konto2, konto3, kontoFirmowe);

        for (Konto kontoZListy : listaKont) {
            if (kontoZListy instanceof KontoFirmowe) { // czy to jest obiekt tej klasy ?
                System.out.print("Jest to konto firmowe");
            } else {
                System.out.print("jest to zwykle konto");
            }
            System.out.println("(" + kontoZListy.getNumerKonta() + ")" + kontoZListy.pokazStanKonta());
        }

    }
}
