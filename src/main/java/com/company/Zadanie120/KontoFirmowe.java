package com.company.Zadanie120;

public class KontoFirmowe extends Konto {

    private double oplataZaTransakcje;

    public KontoFirmowe(String wlasciciel, double oplataZaTransakcje) {
        super(wlasciciel);  //wywolywany konstruktor nadklasy(konto)
        this.oplataZaTransakcje = oplataZaTransakcje;
    }

    @Override
    boolean wyplataZKonta(double wyplata) {
        double sumaPoOplacie = (wyplata + (wyplata * oplataZaTransakcje));
        if (stan >= sumaPoOplacie) {
            stan -= sumaPoOplacie;
            return true;
        }
        return false;
    }

}
