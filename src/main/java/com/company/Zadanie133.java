package com.company;
/*
*ZADANIE #133*
Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku,
 a następnie wyświetla zawartość tego pliku na konsolę.
  Zrealizuj zadanie odczytując dane “linia po linii”
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;   //wyjątek plik nie znaleziony
import java.io.FileReader;
import java.io.IOException; //problem przy odczycie lub zapisie do pliku

public class Zadanie133 {
    public static void main(String[] args) {
        try {
            wyswietlZawartoscPliku("pliki/zadanie133.txt");
        } catch (IOException e) {
            System.out.println("błąd odczytu z pliku");
        }
    }

    static void wyswietlZawartoscPliku(String sciezka) throws IOException {
        FileReader plik = new FileReader(sciezka);      //file reader zczytuje po liniach z pliku ( akceptuje polskie znaki)
        BufferedReader buforOdczytu = new BufferedReader(plik);

        String linia = buforOdczytu.readLine();   //odczytujemy całą linie z pliku

        while (linia != null) {
            System.out.println(linia);
            linia = buforOdczytu.readLine();
        }

    }
}
