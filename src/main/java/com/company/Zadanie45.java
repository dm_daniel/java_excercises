package com.company;
/*
Utwórz metodę, w której użytkownik podaje z klawiatury rozmiar tablicy, a następnie wszystkie elementy (jako `int`):
>
Podaj rozmiar tablicy:  4
Liczba[1]:  88
Liczba[2]:  12
Liczba[3]:  -7
Liczba[4]:  195
Podana tablica to: [88, 12, -7, 195]
Suma elementów wynosi: 288```
 */

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie45 {
    public static void main(String[] args) {
        sumaElementowTablicy();

    }

    static void sumaElementowTablicy() {
        System.out.print("Podaj rozmiar tablicy: ");
        Scanner scan = new Scanner(System.in);        //obiekt klasy scanner,korzystamy z zewnętrznej bilbioteki - obiekt pozwoli odczytywac dane z klawiatury
        int rozmiarTablicy = scan.nextInt();    //prosze o wczytanie kolejnej liczby z klawiatury
        int[] tablica = new int[rozmiarTablicy];
        int suma = 0;
        for (int i = 0; i < rozmiarTablicy; i++) {
            System.out.print("Liczba " + (i + 1) + ":");
            tablica[i] = scan.nextInt();
            suma += tablica[i];
        }
        System.out.println("Podana tablica to: " + Arrays.toString(tablica));
        System.out.println("Suma elementów wynosi:" + " " + suma);
    }
}
