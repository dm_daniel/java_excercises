package com.company;

import java.util.Arrays;

/*
*ZADANIE #50*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma zwrócić informację ile razy w tablicy występuje liczba podana jako drugi parametr.

Utwórz drugą metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma usunąć wszystkie elementy równe podanemu parametrowi (tzn. gdy podamy `11` to ma usunąć z tablicy wszystkie `11`-ki)
> Dla `([1, 2, 2, 4, 5],  2)`
> zwróci `[1, 4, 5]`
 */
public class Zadanie50 {
    public static void main(String[] args) {
        int[] tablica = new int[]{0, 0, 1, 1, 1, 0, 5, 7, 7, 0, 0};
        System.out.println(ileRazyWystepuje(tablica, 0));
        System.out.println(Arrays.toString(usunLiczbeZTablicy(tablica, 0)));        //usuwa zera z tablicy


    }

    static int ileRazyWystepuje(int[] tablica, int liczba) {            //obczaic jak to dziala za pomocą continue
        int suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == liczba) {
                suma += 1;
            }
        }
        return suma;
    }

    static int[] usunLiczbeZTablicy(int[] tablica, int usuwanaLiczba) {     //sprobowac napisac to za pomocą continue
        int iloscWystapien = ileRazyWystepuje(tablica, usuwanaLiczba);
        int[] tablicaNowa = new int[tablica.length - iloscWystapien];
        int indeksWNowejTablicy = 0;
        for (int index = 0; index < tablica.length; index++) {
            if (usuwanaLiczba != tablica[index]) {
                tablicaNowa[indeksWNowejTablicy] = tablica[index];      //do nowej tablicy przypisz wartosci ze starej tablicy
                indeksWNowejTablicy++;          //przesuwamy pozycje w nowej tablicy
            }
        }
        return tablicaNowa;
    }
}
