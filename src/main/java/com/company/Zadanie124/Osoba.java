package com.company.Zadanie124;

public class Osoba implements PrzedstawSie {    //implementuje interfejs(metody z interfejsu przestaw się)
    private String imie;
    private String nazwisko;
    private int wiek;
    private Plec plec;


    public Osoba(String imie, String nazwisko, int wiek, Plec plec) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.plec = plec;
    }

    @Override
    public String ZwrocPelneDane() {
        return String.format("Nazywam się %s %s, mam %s lat", imie, nazwisko, wiek);
    }

    @Override
    public String ZwrocImie() {
        return String.format("Mam na imie %s", imie);
    }

    @Override
    public int zwrocWiekDoEmerytury() {
//        alternatywa:
//        if (Plec.MEZCZYZNA == plec) {
//            return 67 - wiek;
//        } else {
//            return 65 - wiek;
//        }
        return (Plec.MEZCZYZNA == plec ? 67 : 65) - wiek;
    }

    @Override
    public String toString() {
        return ZwrocPelneDane();
    }

    enum Plec {     //typ wyliczeniowy
        MEZCZYZNA, KOBIETA
    }


}
