package com.company.Zadanie124;

public class Pracownik extends Osoba {
    private Double pensja;  //typ klasowy żeby mieć domyślnie nulla

    public Pracownik(String imie, String nazwisko, int wiek, double pensja, Plec plec) {
        super(imie, nazwisko, wiek, plec);  //wywołuje jeden z konstruktorów nadklasy (mamy 1)
    }


    @Override
    public String ZwrocPelneDane() {
        if (pensja != null) {
            return super.ZwrocPelneDane() + "i zarabiam " + pensja;
        } else {
            return super.ZwrocPelneDane();
        }
    }

    //ustaw penjse setter napisany z ręki
    void ustawPensje(Double pensja) {
        this.pensja = pensja;
    }


}
