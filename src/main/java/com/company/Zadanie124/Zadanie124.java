package com.company.Zadanie124;

import static com.company.Zadanie124.Osoba.Plec.KOBIETA;
import static com.company.Zadanie124.Osoba.Plec.MEZCZYZNA;

/*
Maciej Czapla - TRENER [12:32 PM]
*ZADANIE #124*
Utwórz klasy `GenericClass` (z polami `imie`, `nazwisko`, `wiek`) i `Pracownik` (dziedzicząca z Osoby, z polem `pensja`)
oraz utwórz interfejs `PrzedstawSie` w którym będzie metody `wyswietlPelneDane()`, `wyswietlImie()` oraz `zwrocWiekDoEmerytury()`
 */
public class Zadanie124 {
    public static void main(String[] args) {
        Osoba o1 = new Osoba("Marian", "Kowalski", 30, MEZCZYZNA);
        Osoba o2 = new Osoba("Anna", "Nowak", 54, KOBIETA);

        System.out.println(o1.ZwrocPelneDane());
        System.out.println(o1.zwrocWiekDoEmerytury());
        System.out.println(o2.ZwrocPelneDane());
        System.out.println(o2.zwrocWiekDoEmerytury());

        //nie wyswietli pensji
        Pracownik p1 = new Pracownik("Maciek", "Motylewski", 24, 2500.50, MEZCZYZNA);
        System.out.println(p1.ZwrocPelneDane());

        p1.ustawPensje(25_000.0);
        System.out.println(p1.ZwrocPelneDane());

    }

}
