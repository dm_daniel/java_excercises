package com.company;

import java.util.Arrays;

/*
Utwórz metodę, która przyjmuje dwie tablice.
Metoda ma zwrócić tablicę z sumą kolumn podanych tablic. To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
*Przyjęte założenia:*
- Tablice mogą być różnych długości!
- Liczby sumujemy jak w “dodawaniu pisemnym”, tzn. gdy wartość jest większa od `9` to `1` “idzie dalej”.
>
 [1,  2,  3,  4]
   [ 6,  7,  8]
[1,  9,  1,  2]
*/
public class Zadanie52Test {
    public static void main(String[] args) {

        int[] tablica1 = new int[]{9, 9, 9, 9};
        int[] tablica2 = new int[]{9, 9, 9};

        System.out.println(Arrays.toString(sumujTablice(tablica1, tablica2)));

    }

    static int[] sumujTablice(int[] tab1, int[] tab2) {
        int[] tablicaKrotsza = tab1.length <= tab2.length ? tab1 : tab2;
        int[] tablicaDluzsza = tab1.length > tab2.length ? tab1 : tab2;
        int[] tablicaKoncowa = new int[tablicaDluzsza.length + 1];
        int roznicaDlugosci = tablicaDluzsza.length - tablicaKrotsza.length;
        int bufor = 0;  //zmienna dla 1 w pamięci

        //dla tablic równych dlugosci
        if (tablicaDluzsza.length == tablicaKrotsza.length) {
            for (int i = tablicaDluzsza.length - 1; i >= 0; i--) {
                if (tablicaDluzsza[i] + tablicaKrotsza[i] + bufor < 10) {
                    tablicaKoncowa[i] = tablicaDluzsza[i] + tablicaKrotsza[i] + bufor;
                    if (bufor == 1) {
                        bufor--;
                    }
                } else if (tablicaDluzsza[i] + tablicaKrotsza[i] + bufor > 10) {
                    tablicaKoncowa[i] = (tablicaDluzsza[i] + tablicaKrotsza[i] + bufor) % 10;
                    if (bufor == 1) {
                        bufor--;
                    }
                    bufor++;
                } else if (tablicaDluzsza[i] + tablicaKrotsza[i] + bufor == 10) {
                    tablicaKoncowa[i] = 0;
                    if (bufor == 1) {
                        bufor--;
                    }
                    bufor++;
                }
            }
        }
        //dla tablic różnych różnych długości
        if (tablicaDluzsza.length > tablicaKrotsza.length) {
            for (int i = tablicaKrotsza.length - 1; i >= 0; i--) {
                if (tablicaDluzsza[i + roznicaDlugosci] + tablicaKrotsza[i] + bufor < 10) {
                    tablicaKoncowa[i + roznicaDlugosci + 1] = tablicaDluzsza[i + roznicaDlugosci] + tablicaKrotsza[i] + bufor;
                    if (bufor == 1) {
                        bufor--;
                    }
                } else if (tablicaDluzsza[i + roznicaDlugosci] + tablicaKrotsza[i] + bufor > 10) {
                    tablicaKoncowa[i + roznicaDlugosci + 1] = (tablicaDluzsza[i + roznicaDlugosci] + tablicaKrotsza[i] + bufor) % 10;
                    if (bufor == 1) {
                        bufor--;
                    }
                    bufor++;
                } else if (tablicaDluzsza[i + roznicaDlugosci] + tablicaKrotsza[i] + bufor == 10) {
                    tablicaKoncowa[i + roznicaDlugosci + 1] = 0;
                    if (bufor == 1) {
                        bufor--;
                    }
                    bufor++;
                } else if (tablicaDluzsza[roznicaDlugosci] + tablicaKrotsza[0] + bufor >= 10) {
                    if (bufor == 1) {
                        bufor--;
                    }
                    bufor++;
                }
            }

            //przepisanie dłuższej tablicy + ewentualnie + bufor jestli jest 1
            for (int i = roznicaDlugosci - 1; i >= 0; i--) {
                if (roznicaDlugosci - 1 + bufor >= 10) {
                    tablicaKoncowa[i + 1] = (tablicaDluzsza[i] + bufor) % 10;
                    bufor = 0;
                } else if (roznicaDlugosci - 1 + bufor + bufor < 10) {
                    tablicaKoncowa[i + 1] = tablicaDluzsza[i] + bufor;
                    bufor = 0;
                }
            }
        }
        return tablicaKoncowa;
    }
}