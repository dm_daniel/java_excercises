package com.company;
/*
*ZADANIE #84*
Utwórz metodę, która przyjmuje wyraz, a następnie zwraca środkowy element.
W przypadku nieparzystej długości ma zostać zwrócony wyraz długości `1`, a dla parzystych długości `2`
> Dla `"Ala"`, zwróci `"l"`
> Dla `"Anna"`, zwróci `"nn"`
 */

public class Zadanie84 {
    public static void main(String[] args) {

        System.out.println(zwrocSrodkowyElement("Piwo"));
        System.out.println(zwrocSrodkowyElement("KOT"));
        System.out.println();
        System.out.println(zwrocSrodkowyElement2("Piwo"));
        System.out.println(zwrocSrodkowyElement2("kot"));
        System.out.println();
        System.out.println(zwrocSrodkowyElement3("piwo"));
        System.out.println(zwrocSrodkowyElement3("kot"));
    }

    static String zwrocSrodkowyElement(String wyraz) {

        String[] nowaTablica = wyraz.split("");
        int pozycjaSrodkowa = wyraz.length() / 2;

        if (wyraz.length() % 2 == 0) {
            return nowaTablica[pozycjaSrodkowa - 1] + nowaTablica[pozycjaSrodkowa];
        } else {
            return nowaTablica[pozycjaSrodkowa];

        }
    }

    static String zwrocSrodkowyElement2(String wyraz) {
        int pozycjaSrodkowa = wyraz.length() / 2;
        return wyraz.substring(
                wyraz.length() % 2 == 0 ? pozycjaSrodkowa - 1 : pozycjaSrodkowa,
                pozycjaSrodkowa + 1);
    }

    static String zwrocSrodkowyElement3(String wyraz) {
        int pozycjaSrodkowa = wyraz.length() / 2;

        if (wyraz.length() % 2 == 0) {
            return wyraz.substring(pozycjaSrodkowa - 1, pozycjaSrodkowa + 1);
        }
        return wyraz.substring(pozycjaSrodkowa, pozycjaSrodkowa + 1);
    }
}
