package com.company.Zadanie150;

/*
*ZADANIE #150*
Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
> Dla `4` zwróci: `[10, 11, 12, 13]`
>
> Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
 */
public class Zadnie150 {


    public int[] zwrocTablice(int rozmiar) {
        int[] tablica = new int[rozmiar];

        for (int i = 0; i < rozmiar; i++) {
            tablica[i] = 10 + i;
        }
        return tablica;
    }
}
