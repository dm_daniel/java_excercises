package com.company;
/*
Maciej Czapla - TRENER [1:08 PM]
*ZADANIE #104*
Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. `ArrayList`) oraz indeks za którym należy “przeciąć” tablicę i zwrócić drugą część.
> Dla `3` oraz `<1, 7, 8, 22, 10, -2, 33>`
> powinno zwrócić `<10, -2, 33>`
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie104 {
    public static void main(String[] args) {

        List<Integer> lista = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
        System.out.println(przetnijTablice(lista, 7));

    }

    private static List<Integer> przetnijTablice(List<Integer> lista, int indexPrzeciecia) {

        List<Integer> nowaLista = new ArrayList<>();

        for (int index = indexPrzeciecia + 1; index < lista.size(); index++) {
//            nowaLista.add(lista.get(index));          alternatywa
            int liczba = lista.get(index);
            nowaLista.add(liczba);
        }

        return nowaLista;
    }
}
