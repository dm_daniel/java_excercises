package com.company;
/*
*ZADANIE #101*
Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania `-1` a następnie zwraca ich listę (np. wykorzystując implementację `ArrayList`) (edited)
+ liczymy średnią
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie101 {
    private static final int KONIEC = -1;               //liczba przeniesiona do stałych (magic number) za jakiś czas nie bedzie wiadomo dlaczego -1

    public static void main(String[] args) {

        List<Integer> lista = zwrocListe();                 //ctrl+alt+v
        System.out.println(lista);
        System.out.println(zwrocSrednia(lista));
    }

    static List<Integer> zwrocListe() {             //metoda jest static wiec może korzystać tylko z pól statycznych
        List<Integer> nowaLista = new ArrayList<>();
        Scanner s = new Scanner(System.in);
        int liczba;

        while (true) {
            System.out.print("Podaj liczbe");
            liczba = s.nextInt();              //metoda pobierajaca wartosci
            if (liczba == KONIEC) {             //warunek przerwania podawania liczb z klawiatury
                break;
            }
            nowaLista.add(liczba);              //dodanie liczby do listy
        }
        return nowaLista;
    }

    static double zwrocSrednia(List<Integer> lista) {
        double srednia = 0;
        for (Integer i : lista) {
            srednia += i;
        }
        srednia = srednia / lista.size();
        return srednia;
    }

}
