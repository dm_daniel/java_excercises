package com.company;
/*
*ZADANIE #27*
Utwórz metodę, która przyjmuje jeden parametr oraz wyświetla daną “kolumnę tabliczki mnożenia”:
>Dla `15` wyświetli(!)
>`15 * 1  = 15`
>`15 * 2  = 30`
>
>`...`
>
>`15 * 9  = 135`
>`15 * 10 = 150`
 */

public class Zadanie27 {
    public static void main(String[] args) {
        kolumnaTabliczkiMnozenia(15);
    }

    static void kolumnaTabliczkiMnozenia(int parametr) {

        for (int rzad = 1; rzad <= 10; rzad++) {
            System.out.printf("%s * %s\t = %s\n", rzad, parametr, rzad * parametr);

        }
    }
}
