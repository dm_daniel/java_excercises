package com.company;
/*
*ZADANIE #63*
Utwórz metodę, która przyjmuje dwuwymiarową tablicę.
Metoda ma zwracać tablicę która będzie zawierała sumy wszystkich kolumn.
 */

import java.util.Arrays;

public class Zadanie63 {
    public static void main(String[] args) {
        int[][] tablica = {{5, 20, 100}, {5, 20, 100}, {5, 20, 100}, {5, 20, 100}, {5, 20, 100}, {5, 20, 100}};
        System.out.println(Arrays.toString(zwrocSumeKolumn(tablica)));
    }

    static int[] zwrocSumeKolumn(int[][] tablica) {
        int[] sumaKolumn = new int[tablica[0].length];  //dlugosc ktoregokolwiek z wiersza np [0] (bo wszystkie są równe)

        for (int wiersz = 0; wiersz < tablica.length; wiersz++) {
            for (int kolumna = 0; kolumna < tablica[0].length; kolumna++) {     //tablica[0] = dlugosc 0 wiersza (pierwszego od góry)
                sumaKolumn[kolumna] += tablica[wiersz][kolumna];
            }
        }
        return sumaKolumn;

    }
}
