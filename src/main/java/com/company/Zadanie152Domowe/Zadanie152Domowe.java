package com.company.Zadanie152Domowe;
/*
*ZADANIE #152*
*NAJPIERW TESTY POTEM IMPLEMENTACJA*

Utwórz metodę w klasie `Person`/`GenericClass` z *ZADANIA #151*, która zwraca *login* w formie napisu, który składa się z:
>- 3 liter imienia
>- 3 liter nazwiska
>- liczby, która jest sumą długości imienia i nazwiska

> Dla `"Maciej Czapla"`, zwróci `"maccza12"`
> Dla `"Anna Nowak"`, zwróci `"annnow9"`
> Dla `"Jan Kowalski"`, zwróci `"jankow11"`


*Najpierw napisz testy które sprawdzą zachowanie metody gdy:*
- nie jest ustawione *imię* (w loginie ma być zastąpione znakami `XXX`)
- nie jest ustawione *nazwisko* (w loginie ma być zastąpione znakami `YYY`)
- nie jest ustawione ani *imię* ani *nazwisko* (metoda ma zwrócić `null`)
- *imię* lub *nazwisko* jest krótsze niż 3 znaki (w loginie ma być “dopełnione” znakiem `Z`, czyli np. dla `xi ping` powinno zwrócić `xiZpin6`)
 */

public class Zadanie152Domowe {
    public static void main(String[] args) {

    }
}
