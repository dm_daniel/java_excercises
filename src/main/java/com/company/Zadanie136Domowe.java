package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
*ZADANIE #136 - PRACA DOMOWA*
W załączniku jest plik z loginami i hasłami. Waszym zadaniem jest zrobienie analizy tego pliku oraz napisanie metod które *ZWRÓCĄ*:

- `[double]` informację jaki % osób miało taki sam login jak hasło (np. `anka40:anka40`)
- `[int]` informację ilu użytkowników “wyciekło”
- `[int]` informację ilu użytkowników NIE miało cyfry w haśle
- `[double]` informację jaki % użytkowników nie używało dużych liter w haśle
- `[int]` informację ile użytkowników miało hasło krótsze niż 5 znaków (sprawdzana długość hasła powinna być parameterem metody)
- `[Map<Integer, Integer>]` informację jaki był rozkład długości haseł (ile razy wystapiła długość 1, ile 2, ile 3...)
- `[List<String>]` listę 10 najpopularniejszych haseł
- `[Map<String, Integer>]`  ile razy wystąpiła każda ze skrzynek mailowych (oczywiście jeśli loginem był mail) (edited)
 */

public class Zadanie136Domowe {
    public static void main(String[] args) throws IOException {

        System.out.println(odczytajPlik("pliki/passwd.txt"));
        System.out.println(procentOsobZtakimSamymLoginemIChaslem("pliki/passwd.txt"));

        System.out.println(ileKontBezCyfryWHasle("pliki/passwd.txt"));

        System.out.println(hasloKrotszeNizPiecZnakow(odczytajPlik("pliki/passwd.txt"), 5));

        System.out.println(rozkladDlugosciHasel(odczytajPlik("pliki/passwd.txt")));
    }

    static List<String> odczytajPlik(String sciezka) throws IOException {

        BufferedReader buffer = new BufferedReader(new FileReader(sciezka));
        List<String> lista = new ArrayList<>();
        String linia = buffer.readLine();
        while (linia != null) {
            lista.add(linia);
            linia = buffer.readLine();
        }
        return lista;
    }

    //- `[double]` informację jaki % osób miało taki sam login jak hasło (np. `anka40:anka40`)
//- `[int]` informację ilu użytkowników “wyciekło”
    private static double procentOsobZtakimSamymLoginemIChaslem(String sciezkaDoPliku) throws IOException {

        BufferedReader buffer = new BufferedReader(new FileReader(sciezkaDoPliku));
        int liczbaLini = 0;
        int liczbaKont = 1;
        String linia = buffer.readLine();

        while (linia != null) {
            liczbaLini++;
            String[] tablica = linia.split(":");
            if (tablica[0].equals(tablica[1])) {
                liczbaKont++;
            }
            linia = buffer.readLine();
        }
        System.out.println("Wyciekło " + liczbaLini + " kont.");
        System.out.print("Procent kont o takim samym haśle i loginie: ");
        return ((double) 100 * liczbaKont) / liczbaLini;
    }

    //- `[int]` informację ilu użytkowników NIE miało cyfry w haśle
    private static int ileKontBezCyfryWHasle(String scezkaDoPliku) throws IOException {
        BufferedReader buffer = new BufferedReader(new FileReader(scezkaDoPliku));
        String linia = buffer.readLine();
        int liczbaLini = 0;
        int liczbaKontBezCyfryWHasle = 0;

        while (linia != null) {
            liczbaLini++;
            String[] tablica = linia.split(":");
            if (tablica[1].matches("[a-zA-z]{1,}")) {       //".*[0-9].*"  gdziekolwiek zawiera cyfrę
                liczbaKontBezCyfryWHasle++;
            }
            linia = buffer.readLine();
        }
        System.out.print("Ilość kont z hasłem bez liter: ");
        return liczbaKontBezCyfryWHasle;
    }

    //TODO
    //  - `[double]` informację jaki % użytkowników nie używało dużych liter w haśle
    private static double jakiProcentUzytykownikowNieUzywaloDuzychLiterWHasle(List<String> lista) {
        int konto = 0;
        int liczbaKont = lista.size();
        System.out.println(liczbaKont);

        for (String s : lista) {

        }
        System.out.println(konto);
        return konto;
    }

    //- `[int]` informację ile użytkowników miało hasło krótsze niż 5 znaków (sprawdzana długość hasła powinna być parameterem metody)
    private static int hasloKrotszeNizPiecZnakow(List<String> lista, int dlugoscHasla) {
        int ilosc = 0;

        for (String s : lista) {
            String[] tab = s.split(":");
            if ((tab[1].length() < 5)) {
                ilosc++;
            }
        }
        return ilosc;
    }

    //- `[Map<Integer, Integer>]` informację jaki był rozkład długości haseł (ile razy wystapiła długość 1, ile 2, ile 3...)
    private static Map<Integer, Integer> rozkladDlugosciHasel(List<String> lista) {
        Map<Integer, Integer> mapa = new HashMap<>();
        Integer[] tablica = new Integer[10];

        for (String s : lista) {
            int liczbaWystapien = 0;
            String[] tab = s.split(":");

        }
        return mapa;
    }
}
