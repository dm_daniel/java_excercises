package com.company;
/*
*ZADANIE #17*
Utwórz metodę, do której przekazujesz wartość logiczną,
 a ona *wyświetli* napis `Tak`, w przypadku przekazania
  `true` oraz `Nie` w przypadku przekazania `false`

 */

public class Zadanie17 {
    public static void main(String[] args) {
        wartoscLogiczna();

    }

    static void wartoscLogiczna() {
        int liczba = 21;
        System.out.println(liczba > 20 ? "Tak" : "Nie");
        String komunikat = liczba > 22 ? "Tak" : "Nie";
        System.out.println(komunikat);
    }
}
