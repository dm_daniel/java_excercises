package com.company.Zadanie157;

import java.io.*;

public class Zadanie157 {
    public static void main(String[] args) {

        Wiadomosc wiadomosc = new Wiadomosc("Przywitanie", "Siemanko");

        zapisDoPliku(wiadomosc);
        Wiadomosc odczytanaWiadomosc = (odczytZPliku());
        System.out.println(odczytanaWiadomosc);
    }

    static Wiadomosc odczytZPliku() {
        try (
                FileInputStream file = new FileInputStream("pliki/zadanie157.txt");
                ObjectInputStream input = new ObjectInputStream(file);
        ) {
            Wiadomosc wiadomosc = (Wiadomosc) input.readObject();
            return wiadomosc;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    static void zapisDoPliku(Wiadomosc wiadomosc) {
        try {
            FileOutputStream file = new FileOutputStream("pliki/zadanie157.txt");
            ObjectOutputStream output = new ObjectOutputStream(file);
            output.writeObject(wiadomosc);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void zapisDoPliku2(Wiadomosc wiadomosc) {
        //try with resources (obiekty są same zamykane - nie potrzeba output.close)
        try (
                FileOutputStream file = new FileOutputStream("pliki/zadanie157.txt");
                ObjectOutputStream output = new ObjectOutputStream(file);
        ) {
            output.writeObject(wiadomosc);
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }

}
