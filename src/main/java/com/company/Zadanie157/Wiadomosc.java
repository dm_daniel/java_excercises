package com.company.Zadanie157;

import java.io.Serializable;

public class Wiadomosc implements Serializable {
    transient private String title;     // transient nie podlega serializacji
    private String content;

    public Wiadomosc(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Override
    public String toString() {
        return "Wiadomosc{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
