package com.company;
/*
*ZADANIE #116*
Utwórz metodę, która przyjmuje jeden parametr - maksymalną liczbę.
Metoda powinna zwrócić mapę, gdzie kluczami będą kolejne liczby od `1` do podanej liczby (jako parametr).
Wartościami tej mapy będzie informacja, przez jakie liczby jest ona podzielna.
>
...
10 -> 1, 2, 5, 10
11 -> 1, 11
12 -> 1, 2, 3, 4, 6, 12
13 -> 1, 13
14 -> 1, 2, 7, 14
...```
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Zadanie116 {
    public static void main(String[] args) {
        Map<Integer, Set<Integer>> mapka = mapaZDzielnikamiKlucza(12);
        ladnieWyswietl(mapka);
    }

    static Map<Integer, Set<Integer>> mapaZDzielnikamiKlucza(int max) {
        Map<Integer, Set<Integer>> mapka = new HashMap<>();

        for (int klucz = 1; klucz <= max; klucz++) {
            Set<Integer> secik = secikDzielnikow(klucz);
            mapka.put(klucz, secik);
        }
        return mapka;
    }

    static Set<Integer> secikDzielnikow(int liczbaDoSprawdzenia) {
        Set<Integer> secik = new HashSet<>();

        for (int dzielnik = 1; dzielnik <= liczbaDoSprawdzenia; dzielnik++) {
            if (liczbaDoSprawdzenia % dzielnik == 0) {
                secik.add(dzielnik);
            }
        }
        return secik;
    }

    static void ladnieWyswietl(Map<Integer, Set<Integer>> mapka) {

        for (Map.Entry<Integer, Set<Integer>> entry : mapka.entrySet()) {
            System.out.print(entry.getKey() + " -> ");
            for (Integer integer : entry.getValue()) {
                //przechodzimy po wartości mapy, który w tym przypadku jest setem
                // entry.getValue najedz z ctrl

                System.out.print(integer + ", ");
            }
            System.out.println();
        }
    }
}
