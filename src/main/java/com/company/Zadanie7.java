package com.company;
//*ZADANIE #7*
//Utwórz metodę, do której przekazujesz dwa parametry a ona zwróci ich sumę
//> Dla `2` oraz `11` zwróci `13`
//>
//> Dla `-10` oraz `3` zwróci `-7`

public class Zadanie7 {
    public static void main(String[] args) {

        int resultOne = method(5, 3);
        System.out.println("Wynik: " + resultOne);

        int resultTwo = method(100, 200);
        System.out.println("Wynik: " + resultTwo);

        System.out.println("Wynik: " + method(40, 80));

    }

    static int method(int numOne, int numTwo) {

        return numOne + numTwo;
    }
}
