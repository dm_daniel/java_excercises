package com.company;
/*
*ZADANIE #111*
Utwórz metodę, która przyjmuje jeden parametr - mapę w postaci
`String : String`. Metoda ma wyświetlić zawartość przekazanej mapy (w postaci `"PL" -> "Polska"`)
 */

import java.util.HashMap;
import java.util.Map;

public class Zadanie111 {
    public static void main(String[] args) {

        wyswietlMape(tworzyMape()); //odwołuje się po kluczu
        System.out.println();
        wyswetlMape2(tworzyMape());

    }

    static Map<String, String> tworzyMape() {
        Map<String, String> mapa = new HashMap<>();
        mapa.put("PL", "Polska");
        mapa.put("DE", "Niemcy");
        mapa.put("NL", "Holandia");

        return mapa;
    }

    static void wyswietlMape(Map<String, String> mapa) {

        for (String klucz : mapa.keySet()) {
            System.out.println(klucz + " -> " + mapa.get(klucz));
        }
    }

    static void wyswetlMape2(Map<String, String> mapa) {
        for (Map.Entry<String, String> entry : mapa.entrySet()) {       //wyciagam zbiór wierszy przechowywanych pod Map.Entry
            //pobieranie set par (klucz i wartosc)
            System.out.println(entry.getKey() + " -> " + entry.getValue());
        }
    }

}
