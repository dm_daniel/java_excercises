package com.company;
//problem liczb zmiennoprzecinkowych( nie są dokładne) !
public class Zadanie145Ciekawostka {
    public static void main(String[] args) {
//        metoda1();
//        System.out.println();
//        metoda2();
//        System.out.println();
//        metoda3();
//        method4();
//        method5();
//        method6();
    }

    //wartości zmiennoprzecinkowe nie są zawsze odpowiednio zamieniane(podają najbliższa wartość dlatego po przecinku dostajemy wartośći różne)
    private static void metoda1() {
        for (float i = 0; i < 1; i += 0.1f) {
            System.out.printf("%.2f oraz %s \n", i, i);
        }
    }

    private static void metoda2() {
        for (double i = 0; i < 1; i += 0.1d) {
            System.out.printf("%.2f oraz %s\n", i, i);
        }
    }

    //te wartości otrzymałem z metod 2 i 1(przy sprawdzeniu w tej metodzie wartości te się nie równają)
    private static void metoda3() {
        System.out.println(1d == 0.9999999999999999f);
        System.out.println(1f == 0.9999999999999999);
        System.out.println(0.70d == 0.70000005);
        System.out.println(0.70f == 0.70000005);
    }

    private static void method4() {
        for (float i = 10; i > 0; i -= 0.1f) {
            System.out.printf("%.2f oraz %s\n", i, i);

        }
    }

    private static void method5() {
        double sum = 0;
        for (int i = 0; i < 100000; i++) {
            sum += 0.1d;
        }
        System.out.println(sum);
    }

    private static void method6() {
        float f = 0.1f;
        double d = 0.1d;

        System.out.printf("float:  %.50f\n", f);
        System.out.printf("double:  %.50f\n", d);
        System.out.println(0.1f==0.10000000149011612000000000000000000000000000000000);
    }
}
