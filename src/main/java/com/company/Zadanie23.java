package com.company;
/*
*ZADANIE #23*
Utwórz metodę, w której *wyświetlisz co drugą* liczbę z przedziału `0` do `14` (krańcowy zakres przedziału ma być drugim parametrem metody)
> Dla `0, 4` wyświetli `0, 2, 4`
>
> Dla `6, 14` wyświetli `6, 8, 10, 12, 14`
 */

public class Zadanie23 {
    public static void main(String[] args) {
        wyswietlCoDruga(6, 14);
        System.out.println();
        doWhileWyswietlCoDruga(0, 5);

    }

    static void wyswietlCoDruga(int poczatek, int granica) {
        for (int i = poczatek; i <= granica; i = i + 2) {
            System.out.println(i + " ");

        }
    }

    static void doWhileWyswietlCoDruga(int start, int stop) {
        int i = start;
        do {
            System.out.println(i + " ");
            i += 2;
        } while (i <= stop);
    }
}
