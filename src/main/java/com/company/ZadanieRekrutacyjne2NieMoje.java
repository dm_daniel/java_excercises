package com.company;
import java.util.ArrayList;
import java.util.List;

/*
Załozenia:
*w tablicy występuje przynajmniej jedno wzgórze i jedna dolina (na poczatku i na koncu)
 */
public class ZadanieRekrutacyjne2NieMoje {
    final static private int LICZBA_STARTOWA_ZAMKOW = 2;

    public static void main(String[] args) {
        int[] tablica = {2, 2, 3, 4, 3, 3, 2, 2, 1, 2, 5};
        int[] tablica2 = {1, 1, 6, 6, 8, 1, 5, 4, 4, 3, 3, 2, 2, 2};
        System.out.println(solution(tablica));
    }

    public static int solution(int[] tablica) {
        int counter = LICZBA_STARTOWA_ZAMKOW;
        List<Integer> listaBezPowtorzen = new ArrayList<>();

        for (int i = 0; i < tablica.length - 1; i++) {
            if (tablica[i] != tablica[i + 1]) {
                listaBezPowtorzen.add(tablica[i]);
            }
        }
        if (tablica[tablica.length - 1] != tablica[tablica.length - 2]) {
            listaBezPowtorzen.add(tablica[tablica.length - 1]);
        }

        for (int i = 1; i < listaBezPowtorzen.size() - 1; i++) {
            if (listaBezPowtorzen.get(i) > listaBezPowtorzen.get(i + 1)
                    && listaBezPowtorzen.get(i) > listaBezPowtorzen.get(i - 1)) {
                counter++;
            }
            if (listaBezPowtorzen.get(i) < listaBezPowtorzen.get(i + 1)
                    && listaBezPowtorzen.get(i) < listaBezPowtorzen.get(i - 1)) {
                counter++;
            }
        }
        return counter;
    }
}