package com.company;
/*
*ZADANIE #56*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma zwrócić *ile elementów* (idąc po kolei od *PRAWEJ*) należy zsumować by przekroczyć podany (jako drugi) parametr
dla `([1,2,3,4,5,6],  9)`
należy zwrócić `2`
 */

public class Zad56 {
    public static void main(String[] args) {
        //               0  1  2  3  4  5
        int[] tablica = {1, 2, 3, 4, 5, 6};
        System.out.println(ileElementowZsumowac(tablica, 15));
    }

    static int ileElementowZsumowac(int[] tab, int granica) {
        int suma = 0;

        for (int i = tab.length - 1; i >= 0; i--) {
            suma += tab[i];
            if (suma >= granica) {
                return tab.length - i;
            }
        }

        return -1;
    }
}