package com.company;
/*
Zadanie 24:
Utwórz metodę, do której przekazujesz jeden parametr a następnie wyświetlasz tyle wielokrotności liczby `10`
> Dla `4` wyświetli `10, 20, 30, 40`
>
> Dla `7` wyświetli `10, 20, 30, 40, 50, 60, 70`
 */

public class Zad24 {
    public static void main(String[] args) {
        forWyswietlWielokrotnosc(4);
        whileWyswietlWielokrotnosc(4);
        doWhileWyswietlWielokrotnosc(5);
        doWhileWyswietlWielokrotnosc(7);
    }

    static void forWyswietlWielokrotnosc(int parametr) {
        for (int i = 1; i <= parametr; i++) {
            System.out.print(i * 10 + " ");
        }
        System.out.println();
    }

    static void whileWyswietlWielokrotnosc(int parametr) {
        int i = 1;
        while (i <= parametr) {
            System.out.print(i * 10 + " ");
            i += 1;
        }
        System.out.println();
    }

    static void doWhileWyswietlWielokrotnosc(int parametr) {
        int i = 1;
        do {
            System.out.print(i * 10 + " ");
            i += 1;
        } while (i <= parametr);
        System.out.println();
    }
}