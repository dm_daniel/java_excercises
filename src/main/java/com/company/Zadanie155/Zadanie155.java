package com.company.Zadanie155;

/*
*ZADANIE #155* - Klasy anonimowe
Utwórz klasą `GenericClass` z polami `imie` i `wiek`
oraz dodaj metodę `przedstawSie()` która zwraca `String`-a (edited)
 */
public class Zadanie155 {
    public static void main(String[] args) {
        //metoda1();
        metoda2();
    }

    static void metoda1() {
        Osoba osoba1 = new Osoba("Radek", 20);
        String info = osoba1.przedstawSie();
        System.out.println(info);
    }

    //klasa anonimowa(nadpisanie metody przedstaw się)
    static void metoda2() {
        String info = new Osoba("Daniel", 30) {     //od tego momentu należy to traktować jako zawartość nowego pliku osoba$1.class
            @Override
            String przedstawSie() {
                return String.format("(anonymous) Cześć, Jestem %s mam %s lat",     //nie mamy dostępu do pól(imie,wiek), więc stworzyłem gettery w klasie osoba
                        getImie(), getWiek());
            }
        }.przedstawSie();   //na obiekcie (nadpisanej metody) wywołałem metodę przedstaw się
        System.out.println(info);
    }

}
