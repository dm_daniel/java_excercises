package com.company.zadanie169TypOpcjonalny;

import java.util.Optional;

public class Car {
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public Optional<Integer> getAge() { //typ w którym opcjonalnie będzie jakaś wartość (może być pusto)
        return Optional.ofNullable(age);
    }
}
