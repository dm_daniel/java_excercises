package com.company.zadanie169TypOpcjonalny;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Zadanie169 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
        method4();
    }

    private static void method4() {
        List<Integer> list = Arrays.asList(
                1, 6, 8, 55, 70, 5, 4, 21, 150);

        Optional<Integer> first = list.stream()
                .filter(number -> number > 100)
                .findFirst();

////można też zrobić or elsa
//        Integer second = list.stream()
//                .filter(number -> number > 100)
//                .findFirst()
//                .orElse(0);


        if (first.isPresent()) {
            System.out.println("tak, znaleziono liczbę - " + first.get());
        } else {
            System.out.println("żadna liczba nie spełnia warunku");
        }
    }

    private static void method3() {
        Optional<Double> firstResult = safeDivision(2, 1);
        System.out.println(firstResult.isPresent()); //czy jest obecny
        double wynik = firstResult.orElse(0D);       //mowie jaki to ma być typ-D dotyczy doubli
        System.out.println(wynik);

        Optional<Double> secondResult = safeDivision(7, 0);
        System.out.println(secondResult.isPresent());
        double secondDividingResult = secondResult.orElse(0D);
        System.out.println(secondDividingResult);
    }

    private static Optional<Double> safeDivision(double first, double second) {
        if (second == 0) {
            return Optional.empty();    //jesli second ==0 zwracam Optional empty
        } else {
            return Optional.of(first / second);  //jeśli second nie jest 0 dzielę pierwsze przez drugi
        }
    }

    private static void method2() {
        Car car = new Car();
        Optional<Integer> age = car.getAge();

//zawsze chce miec wartość nawet jak w środku jest null
//nie sprawdzam czy coś jest nullem, tylko mam metodę or else co w przypadku, gdy tej wartości nie ma(gdy nie ma wieku)
        int realReal = age.orElse(-1);

//sprawdzam czy jakaś wartość (rózna od null) jest w środku
        if (age.isPresent()) {
            int real = age.get();
        }
    }

    private static void method1() {
        Car car = new Car();
        System.out.println(car.getName());
        if (car.getName() != null) {
            String carName = car.getName().toLowerCase();
        }
    }
}
