package com.company.zadanie95;
/*
*ZADANIE #95*
Utwórz typ wyliczeniowy `TypWaluty` zawieająca opcje `USD`, `EUR`, `PLN` wraz z opisem (`Dolar`, `Euro`, `Polski Złoty`).
Utwórz klasę `Waluta`, gdzie jednym z pól będzie ten typ (oraz pola `kursSprzedazy`, `kursKupna`).
 */

public class Zadanie95 {
    public static void main(String[] args) {

        Waluta w = new Waluta(4.11, 4.36, TypWaluty.EUR);
        System.out.println(w);
        w.pokazZysk(1000);
        System.out.println();
        System.out.println(TypWaluty.USD.getOpis());
        System.out.println(TypWaluty.USD.getKraj());
        System.out.println(w.getTypWaluta().getOpis());
        System.out.println(w.getTypWaluta().getKraj());

    }
}
