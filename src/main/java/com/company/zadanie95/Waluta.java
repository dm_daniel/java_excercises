package com.company.zadanie95;

public class Waluta {
    //pola w klasie
    private double kursKupna;
    private double kursSprzedazy;
    private TypWaluty typWaluta;

    Waluta(double kursSprzedazy, double kursKupna, TypWaluty typWaluta) {
        this.kursKupna = kursKupna;
        this.kursSprzedazy = kursSprzedazy;
        this.typWaluta = typWaluta;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s",
                kursKupna,
                kursSprzedazy,
                typWaluta.getOpis());

    }

    void pokazZysk(double kwota) {
        double przelicznik = kwota / kursKupna;
        System.out.printf("Za %s zł możesz kupić %.2f %s ",      //printf = print format, .2f 2 miejsca po przecinku
                kwota,
                przelicznik,
                typWaluta.getOpis());
    }

    public TypWaluty getTypWaluta() {
        return typWaluta;
    }
}
