package com.company.zadanie95;

enum TypWaluty {

    //opcje
    USD("dolar", "USA"), EUR("euro", "UE"), PLN("polski złoty", "Polska");

    //pole  przypisują się w automatycznie, w zależności od ustawieniach w konstruktorze
    private String opis;
    private String kraj;

    //konstruktor
    TypWaluty(String opis, String kraj) {
        this.opis = opis;
        this.kraj = kraj;

    }

    public String getOpis() {
        return opis;


    }

    public String getKraj() {
        return kraj;
    }
}
