package com.company;
/*
*ZADANIE #37*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca sumę wszystkich elementów
 */

public class Zadanie37 {
    public static void main(String[] args) {
        int[] tablica = new int[]{10, 5, 0};
        System.out.println(zwrocSume(tablica));
        System.out.println(zwrocSume2(tablica));
        System.out.println(sredniaWartosc(tablica));
    }

    static int zwrocSume(int[] tablica) {
        int suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            suma += tablica[i];
        }
        return suma;
    }

    static int zwrocSume2(int[] tablica) {
        int suma = 0;
        for (int liczba : tablica) {    //iter, (petla szybsza niz for)
            suma += liczba;
        }
        return suma;
    }

    static double sredniaWartosc(int[] tablica) {               //czy na pewno srednia wartosc?
        return zwrocSume2(tablica) / (double) tablica.length;
    }
}