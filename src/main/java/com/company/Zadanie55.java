package com.company;
/*
*ZADANIE #55*
Utwórz metodę, która przyjmuje  dwie tablice.
Metoda ma zwrócić nową tablicę w której elementy będą wstawione na zmianę.
>Dla `[5, 1, 10, 1, 89, -9]`
>oraz [-2, 2, 7]`
>Ma zwrócić `[5, -2, 1, 2, 10, 7, 1, 89, -9]`

>Dla `[1, 1, 1, 1, 1]`
>oraz `[2, 2, 2]`
>Ma zwrócić `[1, 2, 1, 2, 1, 2, 1, 1]`
 */

import java.util.Arrays;

public class Zadanie55 {
    public static void main(String[] args) {
        int[] tablica1 = new int[]{1, 1, 1, 5, 9, 1, 3, 4, 5};
        int[] tablica2 = new int[]{2, 2, 2, 2};
        System.out.println(Arrays.toString(polaczTablice(tablica1, tablica2)));
    }

    static int[] polaczTablice(int[] tablica1, int[] tablica2) {
        int[] tablicaKoncowa = new int[tablica1.length + tablica2.length];
        int[] dluzszaTablica = tablica1.length > tablica2.length ? tablica1 : tablica2; //zwroc wartosc dluzszej tab.
        int[] krotszaTablica = tablica1.length < tablica2.length ? tablica1 : tablica2; //zwroc wartosc krotszej tab.
        int indexDluzszejTablicy = krotszaTablica.length;

        for (int i = 0; i < krotszaTablica.length; i++) {
            tablicaKoncowa[i * 2] = tablica1[i];        //uzupelniamy co drugą pozycje w tablicy końcowej
        }

        for (int i = 0; i < krotszaTablica.length; i++) {
            tablicaKoncowa[i * 2 + 1] = tablica2[i];    //uzupelniamy brakujace pozycje w tablicy
        }
//te dwie petle wypelniaja trzecia tablice do części wspólnej

        for (int i = krotszaTablica.length * 2; i < tablicaKoncowa.length; i++) {   // *2 bo jest to dwókrotna długość przedziału
            tablicaKoncowa[i] = dluzszaTablica[indexDluzszejTablicy];   //do tablicy koncowej przypisujemy wartość pod indexem z dluższej tablicy
            indexDluzszejTablicy++;
        }

        return tablicaKoncowa;
    }
}
