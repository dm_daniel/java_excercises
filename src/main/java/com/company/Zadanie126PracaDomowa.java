package com.company;
/*
Maciej Czapla - TRENER [2:53 PM]
*ZADANIE #126 - PRACA DOMOWA*
Utwórz metodę która przyjmuje tablicę, a następnie przy wykorzystaniu `Scannera` użytkownik może podać indeks parametru.
W przypadku indeksu poza zakresem (tzn. gdy wystąpi wyjątek) zapytaj użytkownika o indeks ponownie. (edited)
 */


import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie126PracaDomowa {
    public static void main(String[] args) {

        int[] tablica = new int[]{5, 20, 55, 150};

        obslugaBledu(tablica);
    }

    private static void obslugaBledu(int[] tablica) {

        Scanner s = new Scanner(System.in); //klasa System in jest źródłem wejscia(nowy obiekt klasy skanner, in jest polem w klasie System)

        while (true) {
            System.out.println("Podaj liczbe... ");

            try {
                int index = s.nextInt();
                System.out.printf("Na pozycji %s jest liczba %s", index, tablica[index]);
                break;
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Podano zły indeks");
            } catch (InputMismatchException e) {            //obsługa błedu gdy oczekuje int-a a podaję string(np) (gdy wartośćz klawiatury nie może zostać przekonwertowana na int)
                System.out.println("Nie podałeś liczby");
                s.nextLine();       //zdejmuje wartość przypisaną do indexu (niedoskonałość scanerra - gdy podam tu liczbę, zachowa tą wartość wiece trzeba ją wyczyścic z pamęci)
            }
        }
    }
}
