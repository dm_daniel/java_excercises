package com.company;

public class StringiEquals {
    public static void main(String[] args) {
        String a = "kot";
        String b = "kot";
        System.out.println(a==b);

        String c = new String("kot");
        System.out.println(a == c);
        System.out.println(a.equals(c));

        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
        System.out.println(c.hashCode());

        String d = "k";
        System.out.println(d.hashCode());

        d+="ot";
        System.out.println(d.hashCode());

    }
}