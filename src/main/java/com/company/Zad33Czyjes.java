package com.company;

/*
Utwórz metodę, która wyświetli prostokąt o podanych
wymiarach (użytkownik podaje jego wymiary jako parametry)
XXXXXXXXXX
X        X
X        X
XXXXXXXXXX```
 */
public class Zad33Czyjes {
    static String znak = "X ";           //pole w klasie, ktore jest dostępne w kazdej metodzie
    static String przerwa = "  ";

    public static void main(String[] args) {

        pokaKwadrat(6, 6);

    }

    static void pokaKwadrat(int szer, int wys) {
        pokaWiersz(szer);

        for (int j = 1; j < wys - 1; j++) {
            System.out.print(znak);
            for (int x = 1; x < szer-1; x++) {

                System.out.print(przerwa);
            }
            System.out.println(znak);
        }
        pokaWiersz(szer);

    }

    static void pokaWiersz(int szer) {
        for (int i = 1; i <= szer; i++) {
            System.out.print(znak);
        }
        System.out.println();
    }

}