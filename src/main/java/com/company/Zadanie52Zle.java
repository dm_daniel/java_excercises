package com.company;

import java.util.Arrays;

/*
```

```
Utwórz metodę, która przyjmuje dwie tablice.
Metoda ma zwrócić tablicę z sumą kolumn podanych tablic. To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
*Przyjęte założenia:*
- Tablice mogą być różnych długości!
- Liczby sumujemy jak w “dodawaniu pisemnym”, tzn. gdy wartość jest większa od `9` to `1` “idzie dalej”.

>
[1,  2,  3,  4]
   [ 6,  7,  8]
[1,  9,  1,  2]
*/
public class Zadanie52Zle {
    public static void main(String[] args) {
        int[] tablica1 = new int[]{0, 9, 5, 5, 5};
        int[] tablica2 = new int[]{      5, 9, 8,};


        System.out.println(Arrays.toString(sumujKolumnyTablic(tablica1, tablica2)));

    }

    static int[] sumujKolumnyTablic(int[] tablica1, int[] tablica2) {
        int[] tablicaKrotsza = tablica1.length <= tablica2.length ? tablica1 : tablica2;
        int[] tablicaDluzsza = tablica1.length > tablica2.length ? tablica1 : tablica2;
        int[] tablicaKoncowa = new int[tablicaDluzsza.length + 1];
        int bufor = 0;

        for (int i = tablicaDluzsza.length - 1; i >= 0; i--) {
            tablicaKoncowa[i + 1] = tablicaDluzsza[i];
        }

        for (int i = tablicaKrotsza.length - 1; i >= 0; i--) {
            int sumaElementowZTablic = tablicaDluzsza[i] + tablicaKrotsza[i];

            if (sumaElementowZTablic >= 10 && bufor == 0) {
                tablicaKoncowa[i + 1] = sumaElementowZTablic - 10;
                bufor++;

            } else if (sumaElementowZTablic >= 10 && bufor == 1) {
                tablicaKoncowa[i + 1] = sumaElementowZTablic - 10 + bufor;
//                if (bufor == 0)
//                    bufor++;
            } else if (sumaElementowZTablic < 9 && bufor == 0) {
                tablicaKoncowa[i + 1] = sumaElementowZTablic;
            } else if (sumaElementowZTablic < 9 && bufor == 1) {
                tablicaKoncowa[i + 1] = sumaElementowZTablic + bufor;
                bufor--;
            } else if (sumaElementowZTablic == 9 && bufor == 1) {
                tablicaKoncowa[i + 1] = 0;
                bufor--;
                bufor++;
            } else if (sumaElementowZTablic == 9 && bufor == 0) {
                tablicaKoncowa[i + 1] = sumaElementowZTablic;
            }
            if (tablicaDluzsza[0] + tablicaKrotsza[0] > 9 || tablicaDluzsza[0] + tablicaKrotsza[0] == 9 && bufor == 1) {
                tablicaKoncowa[0] = 1;
            }
        }
        return tablicaKoncowa;
    }
}

