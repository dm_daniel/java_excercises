package com.company.zadanie172TryWithResources;

public class TextManager implements AutoCloseable {

     TextManager() {
        System.out.println("===Początek tekstu===");
    }

    @Override
    public void close() {
        System.out.println("======Koniec tekstu=====");
    }

    void show(String text) {
        System.out.println(text);
    }
}
