package com.company.zadanie172TryWithResources;

public class Zadanie172 {
    public static void main(String[] args) {

        try (TextManager text = new TextManager()) {
            text.show("Dziś jest niedziela");
            text.show("Miesiąc Grudzień");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
