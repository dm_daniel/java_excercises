package com.company;
/*
*ZADANIE #60 - EXTRA*
Utwórz metodę, która przyjmuje tablicę liczb. Metoda ma zwrócić  nową tablicę wartości logicznych (`boolean`),
gdzie wartości dodatnie (oraz `0`) zostaną zastąpione `true`, a ujemne `false`.
>Dla  `[2, 3, -6, 0, -7, -99]`
> zwróci `[true, true, false, true, false, false]`
 */

import java.util.Arrays;

public class Zadanie60_extra {
    public static void main(String[] args) {
        int[] tablica = new int[]{-1, 2, 5, -2, -10, -1, -2};
        System.out.println(Arrays.toString(zwrocTablice(tablica)));
    }

    static boolean[] zwrocTablice(int[] tablica) {
        boolean[] tablicaKoncowa = new boolean[tablica.length];

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] >= 0) {
                tablicaKoncowa[i] = true;
            } else tablicaKoncowa[i] = false;
        }
        return tablicaKoncowa;
    }
}
