package com.company;
/*
    *ZADANIE #101*
Utwórz metodę, która przyjmuje listę (`List`)
np. `ArrayList` elementów typu `String` a następnie zwraca listę w odwróconej kolejności.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie100_1 {
    public static void main(String[] args) {
        List<String> listaString = new ArrayList<>(Arrays.asList("Ala", "ma", "kota"));

        System.out.println(odwrocListe(listaString));
        System.out.println(odwrocListe2(listaString));
    }

    static List<String> odwrocListe(List<String> lista) {
        List<String> nowaLista = new ArrayList<>();

        for (int i = 0; i < lista.size(); i++) {
            nowaLista.add(0, lista.get(i));     //elementy beda sie przemieszczac a nie nadpisywać
        }
        return nowaLista;
    }

    static List<String> odwrocListe2(List<String> lista) {
        List<String> nowaLista = new ArrayList<>();

        for (String i : lista) {
            nowaLista.add(0, i);
        }
        return nowaLista;
    }

}
