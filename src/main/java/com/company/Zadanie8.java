package com.company;
/*
*ZADANIE #8*
Utwórz metodę, która zwróci trzecią potęgę przekazywanej liczby.
>Dla `2` zwróci `8` (bo `2^3 = 2 * 2 * 2 = 8`)
>
>a dla `3` zwróci `27` (bo `3^3 = 3 * 3 * 3 = 27`)
 */

public class Zadanie8 {
    public static void main(String[] args) {

        int result = potegowanie(2);
        System.out.println(result);
        System.out.println("Wynik:" + potegowanie(4));
        System.out.println(potegowanie(10));

    }

    static int potegowanie(int a) {  //musi być static bo main też jest static
        System.out.print(a + "->");
        return a * a * a;       //return zakończa pracę metody

    }
}
