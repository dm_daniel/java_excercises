package com.company;

public class Zadanie3 {
    public static void main(String[] args) {
        System.out.println(1 + 1);          // rezultat 2
        System.out.println(1 + 1 + "1");    //rezultat "21" jako napis
        System.out.println("1" + 1 + 1);    //wykonał pierwszą operacje "1" + 1, następnie "11"+1 = rezultat "111" jako napis
        System.out.println("1" + (1 + 1));    //najpierw wykonuje operacje w głębszym nawiasie, a naspętpinie dodaje 1 z przodu i zamienia na tekst
        System.out.println(2 * 7);
        System.out.println(185 / 2);

    }
}
