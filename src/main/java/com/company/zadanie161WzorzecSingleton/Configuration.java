package com.company.zadanie161WzorzecSingleton;

//przykład singletona
//w dowolnym miejscu aplikacji gdy wywołamy ten obiekt, będzie on taki sam
//przez cały czas życia programu pracuje na 1 i tej samej instancji tej klasy
//poniższe komentarze są do wymogów utworzeniaT singletona
//obiekt tworzony jest tylko 1 raz, ale można zmieniać jego wartość (za każdym razem odwołuje się do tej samej półki z danymi)

public class Configuration {

    private int ulubionaLiczba;
    private static Configuration instance = null;   //klasa przechowuje instancję samej siebie (pole jest statyczne wiec jest dostepne przez caly czas dzialania programu)

    private Configuration() {       //prywatny konstruktor
    }

    public static Configuration getInstance() {     //musi być metoda, która wyda nam ten jedyny obiekt( jeśli wywołamy 2 raz zwróci nam stary(wceśniej utworzony)
        if (instance == null) {     //przy każdym kolejnym wykonaniu instance nie bedzie juz nullem
            instance = new Configuration();//wywołujemy konstruktor
            System.out.println("Obiekt tworzony pierwszy raz");

        } else {
            System.out.println("Pole jest już utworzone");
        }
        System.out.println(instance.hashCode());    //wyswietlam haszkod tego obiektu,który jest zawsze taki sam(bo jest to 1 ten sam obiekt)
        return instance;
    }

    public int getUlubionaLiczba() {
        return ulubionaLiczba;
    }

    public void setUlubionaLiczba(int ulubionaLiczba) {
        this.ulubionaLiczba = ulubionaLiczba;
    }
}
