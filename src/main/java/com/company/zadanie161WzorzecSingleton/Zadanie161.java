package com.company.zadanie161WzorzecSingleton;

public class Zadanie161 {
    public static void main(String[] args) {
        Configuration config = Configuration.getInstance();         //tworzymy obiekt ale zablokowaliśmy konstruktor, wiec nie może byc new Configuration( używamy więc metode get instance)
        config.setUlubionaLiczba(7);

        wyswitlLiczbe();
        config.setUlubionaLiczba(69);
        wyswitlLiczbe();
    }

    private static void wyswitlLiczbe() {               //w dowolnym miejscu programu mam dostęp do ulubionej liczby
        Configuration config = Configuration.getInstance();
        System.out.println(config.getUlubionaLiczba());     //pobieramy wartość pola
    }
}
