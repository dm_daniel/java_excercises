package com.company;

/*
Utwórz metodę, która wyświetli prostokąt o podanych
wymiarach (użytkownik podaje jego wymiary jako parametry)
XXXXXXXXXX
X        X
X        X
XXXXXXXXXX```
 */
public class Zadanie33 {
    static String znak = "X ";           //pole w klasie, ktore jest dostępne w kazdej metodzie
    static String przerwa = "  ";

    public static void main(String[] args) {

        pokazProstokat(6);
       // pokazWiersz(5);
    }

    static void pokazProstokat(int rozmiar) {    //ta metoda wyswietli srodkowe Xksy
        pokazWiersz(rozmiar);
        for (int j = 0; j < rozmiar - 2; j++) {    //zmniejszam rozmiar o 2 bo petla musi sie wykonac 2 razy zeby wyswietlic brakujace Xksy
            System.out.print(znak);
            for (int x = 0; x < rozmiar - 2; x++) {
                System.out.print(przerwa);
            }
            System.out.println(znak);
        }

        pokazWiersz(rozmiar);

    }

    static void pokazWiersz(int rozmiar) {      //ta metoda wyswietli XXXXXX(pierwszy wiersz
        for (int i = 1; i <= rozmiar; i++) {
            System.out.print(znak);
        }
        System.out.println();
    }

}

//    static void pokaKwadrat(int szer, int wys) {
//        pokaWiersz(szer);
//
//        for (int j = 0; j < wys - 2; j++) {
//            System.out.print(znak);
//            for (int x = 0; x < szer - 2 ; x++) {
//
//                System.out.print(przerwa);
//
//            }
//            System.out.println(znak);
//
//
//        }
//
//        pokaWiersz(szer);
//
//    }
//
//    static void pokaWiersz(int wys) {
//        for (int i = 1; i <= wys; i++) {
//            System.out.print(znak);
//        }
//        System.out.println();
//    }