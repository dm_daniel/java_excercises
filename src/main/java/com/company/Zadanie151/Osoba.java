package com.company.Zadanie151;

/*
*ZADANIE #151*
Utwórz klasę `Person` (`GenericClass`) posiadającą
>*pola reprezentujące:*
>* imie
>* nazwisko
>* wiek
>* czyMezczyzna

>*konstruktory*

>*metody służące do:*
> - zwrócenia imienia i nazwiska (tylko jeśli są ustawione!!)
> - przedstawienia się (czyli zwraca `Witaj, jestem Adam Nowak, mam 20 lat i jestem mężczyzną`)
> - sprawdzenia czy osoba jest pełnoletnia
> - zmiany wieku
> - zmiany imienia
> - zwrócenia wieku do emerytury


*ZADANIE #152*
*NAJPIERW TESTY POTEM IMPLEMENTACJA*

Utwórz metodę w klasie `Person`/`GenericClass` z *ZADANIA #151*, która zwraca *login* w formie napisu, który składa się z:
>- 3 liter imienia
>- 3 liter nazwiska
>- liczby, która jest sumą długości imienia i nazwiska

> Dla `"Maciej Czapla"`, zwróci `"maccza12"`
> Dla `"Anna Nowak"`, zwróci `"annnow9"`
> Dla `"Jan Kowalski"`, zwróci `"jankow11"`


*Najpierw napisz testy które sprawdzą zachowanie metody gdy:*
- nie jest ustawione *imię* (w loginie ma być zastąpione znakami `XXX`)
- nie jest ustawione *nazwisko* (w loginie ma być zastąpione znakami `YYY`)
- nie jest ustawione ani *imię* ani *nazwisko* (metoda ma zwrócić `null`)
- *imię* lub *nazwisko* jest krótsze niż 3 znaki (w loginie ma być “dopełnione” znakiem `Z`, czyli np. dla `xi ping` powinno zwrócić `xiZpin6`)
 */

public class Osoba {
    private String imie;
    private String nazwisko;
    private int wiek;
    private boolean czyMezczyzna;

    public Osoba() {
    }

    public Osoba(String imie, String nazwisko, int wiek, boolean czyMezczyzna) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyMezczyzna = czyMezczyzna;
    }

    public String pobierzPelneDane() {
        if (nazwisko == null && imie == null) {
            return null;
        } else if (nazwisko == null) {
            return imie;
        } else if (imie == null) {
            return nazwisko;
        } else {
            return imie + " " + nazwisko;
        }
    }

    public boolean czyOsobaJestPelnoletnia() {
        return wiek > 17;
    }

    public int ileLatDoEmerytury() {
        if (wiek == 0) {
            return -1;
        }

        int wyliczonyWiekDoEmerytury = (czyMezczyzna ? 67 : 65) - wiek;

        return wyliczonyWiekDoEmerytury > 0 ? wyliczonyWiekDoEmerytury : 0;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public boolean isCzyMezczyzna() {
        return czyMezczyzna;
    }


    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setWiek(int wiek) {
        if (wiek > 0) {
            this.wiek = wiek;
        }
    }

    public void setCzyMezczyzna(boolean czyMezczyzna) {
        this.czyMezczyzna = czyMezczyzna;
    }

    public String zwrocLogin() {
        if (imie == null && nazwisko == null) {
            return null;
        } else if (imie == null) {
            return "XXX" + nazwisko.toLowerCase().substring(0, 3) + String.valueOf(nazwisko.length());
        } else if (nazwisko == null) {
            return imie.toLowerCase().substring(0, 3) + "YYY" + String.valueOf(imie.length());
        } else if (imie.length() < 3 || nazwisko.length() < 3) {
            String x = imie.toLowerCase();
            String y = nazwisko.toLowerCase();
            for (int i = x.length(); i <= 2; i++) {
                x += "Z";
            }

            for (int i = y.length(); i <= 2; i++) {
                y += "Z";
            }
            return x.substring(0, 3) + y.substring(0, 3) + String.valueOf((x.length() + y.length()));
        } else
            return imie.toLowerCase().substring(0, 3) + nazwisko.toLowerCase().substring(0, 3) + String.valueOf(imie.length() + nazwisko.length());
    }

}
