package com.company.Zadanie118;

public class Osoba {
    //pola w klasie
    private String imie;
    private int wiek;

    //konstruktor
    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    //metoda, która ma dostęp do pól w tej klasie
    String przedstawiSie() {
//        return "Cześć jestem " + imie + " i mam " + wiek + " lat.";   <--to jest okej.
        return String.format("Mam na imie %s i mam %s lat",
                imie, wiek);
    }

    //metoda toString
    @Override                       //metoda nie pochodzi od nas(rodzic ją posiada a my zmieniamy jej działanie)
    public String toString() {      //z klasy object
        return przedstawiSie();
    }
}
