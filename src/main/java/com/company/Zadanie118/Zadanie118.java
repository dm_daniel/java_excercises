package com.company.Zadanie118;

/*
*ZADANIE #118*
Utwórz klasę `GenericClass`, która zawiera pola `imie`, `wiek`.
Następnie utwórz klasę `Pracownik`, która zawiera dodatkowe pola `pensja`.
Następnie utwórz klasę `Kierownik`, która rozszerza klasę `Pracownik` i posiada dodatkowe pole `premia`.
Utwórz obieky wszystkich klas oraz przetestuj działanie.
 */
public class Zadanie118 {
    public static void main(String[] args) {
        Osoba osoba1 = new Osoba("Daniel", 28);     //new GenericClass (wywołujemy konstruktor)
        System.out.println(osoba1.przedstawiSie());     //wywołałem przedstawienie jawnie
        Osoba osoba2 = new Osoba("Krzysiu", 25);
        System.out.println(osoba2);         //nadpisałem (overrride) metodę

        Pracownik pracownik1 = new Pracownik("Adam", 35, 2000);
        System.out.println(pracownik1);     // nie znalazl u siebie to stringa, więc poszedl to rodzica i go tam znalazł

        System.out.println(pracownik1.zwrocPensjeZaXMiesiecy(5));

        Kierownik kieronik1 = new Kierownik("Maciek", 55, 5000);
        System.out.println(kieronik1.zwrocPensjeZaXMiesiecy(3));
        kieronik1.ustawPremie(500);
        System.out.println(kieronik1.zwrocPensjeZaXMiesiecy(3));

        System.out.println(kieronik1.przedstawiSie());
    }
}
