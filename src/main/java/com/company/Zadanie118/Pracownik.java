package com.company.Zadanie118;

public class Pracownik extends Osoba {  //Pracownik dziedziczy po Osobie
    private int pensja;

    public Pracownik(String imie, int wiek, int pensja) {
        super(imie, wiek);  //super wywołuje sobie konstruktor nadklasy (w tym przypadku nadklasa jest GenericClass)
        this.pensja = pensja;
    }

    @Override
    String przedstawiSie() { //metoda pochodzi z rodzica(z klasy GenericClass)
        return super.przedstawiSie() + " i zarabiam " + pensja + " PLN";
    }

    @Override
    public String toString() {  //metoda pochodzi z rodzica(rodzicem jest nadklasa object)
        return przedstawiSie();
    }

    int zwrocPensjeZaXMiesiecy(int liczbaMiesiecy) {
        return liczbaMiesiecy * pensja;
    }
}
