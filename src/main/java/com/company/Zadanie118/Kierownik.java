package com.company.Zadanie118;

public class Kierownik extends Pracownik {
    //pole w klasie
    private int premia;

    public Kierownik(String imie, int wiek, int pensja) {
        super(imie, wiek, pensja);
    }

    void ustawPremie(int premia) {
        this.premia = premia;
    }

    //napdisujemy metodę z nadklasy (dodajemy premię za X miesięcy)
    @Override
    int zwrocPensjeZaXMiesiecy(int liczbaMiesiecy) {
        return super.zwrocPensjeZaXMiesiecy(liczbaMiesiecy) + premia * liczbaMiesiecy;
    }

    //nadpisuje przedsta w sięw kierowniku (dodajemy informację o premii, której nie ma w klasach wyżej)
    @Override
    String przedstawiSie() {
        return super.przedstawiSie() + "\tmoja premia wynosi" + premia;
    }
}
