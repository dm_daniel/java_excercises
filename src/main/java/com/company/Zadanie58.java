package com.company;
/*
*ZADANIE #58*
Utwórz metodę, która wykorzystuje *varargs* by przekazać do metody dowolną, większą od zera, liczbę elementów typu `String` i zwrócić jeden napis sklejony z nich.
 */

public class Zadanie58 {
    public static void main(String[] args) {
        System.out.println(sklejNapis("Ala " + "ma "+ "kota "+ "1 "+ "asdad "+ "xyz"));
    }

    static String sklejNapis(String wyrazPierwszy, String... napis) {   //typ...nazwa
        String napisKoncowy = wyrazPierwszy + " ";  //dodajac wyraz pierwszy wymusilismy warunek ze nie moze byc 0
        for (String element : napis) {
            napisKoncowy += element + " ";
        }
        return napisKoncowy;
    }
}
