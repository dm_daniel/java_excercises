package com.company.zadanie180BibliotekaGuava;

import com.google.common.base.CaseFormat;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zadanie180 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
        method6();
    }

    private static void method6() {
        File file = new File("pliki/zadanie132.txt");

//wybieram klase Files od googla(są 2)
        try {
            List<String> list = Files.readLines(file, Charsets.UTF_8);
            System.out.println(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void method5() {
        String nick = "AlaMaKota";
        String name = CaseFormat
                .UPPER_CAMEL
                .to(CaseFormat.LOWER_UNDERSCORE, nick);

        System.out.println(nick);
        System.out.println(name);
    }

    //BiMapa    -   wartości muszą być unikalne, klucze zduplikowane się nadpisują
    private static void method4() {
        BiMap<Integer, String> map = HashBiMap.create();
        map.put(1, "Adam");
        map.put(2, "Kuba");
//        map.put(3, "Adam");   - duplikat wartości!


        System.out.println(map);
    }

    //Multimapa
    private static void method3() {
        //Map<Integer, Set<String>>
        Multimap<Integer, String> multimap = HashMultimap.create();     //w multimapie mogą powtarzać sięklucze
        multimap.put(31, "styczen");
        multimap.put(31, "luty");

        multimap.put(10, "lipiec");

        multimap.put(2, "maj");
        multimap.put(2, "maj");

        System.out.println(multimap);
        System.out.println("Miesiące które mają klucz 2: " + multimap.get(2));

    }

    //Łączenie elementów mapy
    private static void method2() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Poniedziałek");
        map.put(2, "Wtorek");
        map.put(3, "środa");
        map.put(4, "Czwartek");

        String joined1 = Joiner.on(", ")
                .withKeyValueSeparator(" -> ")
                .join(map);

        System.out.println(joined1);
    }

    //Łączenie napisów:
    private static void method1() {
        String[] imiona = {"Adam", null, null, "Ola", "Jan", null, "Darek"};

        String joined1 = Joiner
                .on(", ")
                .skipNulls()        //pomijanie wartości 'null'
                .join(imiona);

        System.out.println(joined1);

        String joined2 = Joiner
                .on(" | ")
                .useForNull(" -BRAK- ")
                .join(imiona);

        System.out.println(joined2);
    }
}
