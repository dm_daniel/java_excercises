package com.company;
/*
Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy ) oraz wyświetla “choinkę”
> Dla `4` wyświetli:
>
1
12
123
1234```
>
> Dla `6` wyświetli:
>
1
12
123
1234
12345
123456```
*/

public class Zad31 {
    public static void main(String[] args) {

        forChoinka(4);
        whileChoinka(4);
    }

    static void forChoinka(int wiersze) {
        for (int i = 1; i <= wiersze; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }

    static void whileChoinka(int wiersze) {
        int i = 1;
        int j = 1;
        while (i <= wiersze) {
            while (j <= i) {
                System.out.print(j);
                j++;
            }
            j = 1;
            i++;
            System.out.println();
        }
    }
}

