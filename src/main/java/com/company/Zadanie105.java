package com.company;
/*
*ZADANIE #105*
Utwórz metodę, która przyjmuje listę liczb,
a następnie zwraca listę, która będzie zawierała dwie listy.
Na pozycji `0` mają być elementy (wartości) parzyste, a na pozycji `1` elementy nieparzyste.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie105 {
    public static void main(String[] args) {
        List<Integer> lista1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        System.out.println(podzielListe(lista1));

    }

    static List<List<Integer>> podzielListe(List<Integer> lista) {      //lista która zawiera listy
        List<Integer> listaParzystych = new ArrayList<>();
        List<Integer> listaNiearzystych = new ArrayList<>();

        for (Integer element : lista) {
            if (element % 2 == 0) {
                listaParzystych.add(element);
            } else {
                listaNiearzystych.add(element);
            }
        }

        return new ArrayList<>(Arrays.asList(listaParzystych, listaNiearzystych));      //tworzymy nową liste do której wprowadzamy 2 listy
    }

}
