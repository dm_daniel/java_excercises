package com.company;
/*
*ZADANIE #27*
Utwórz metodę, która przyjmuje jeden parametr oraz wyświetla daną “kolumnę tabliczki mnożenia”:
>Dla `15` wyświetli(!)
>`15 * 1  = 15`
>`15 * 2  = 30`
>
>`...`
>
>`15 * 9  = 135`
>`15 * 10 = 150`
 */

public class Zad27 {
    public static void main(String[] args) {
        //     forWyswietlTabliczke(15);
        //  whileWyswietlTabliczke(4);
        doWhileWyswietlTabliczke(15);
    }

    static void forWyswietlTabliczke(int parametr) {
        for (int i = 1; i <= 10; i++) {
            System.out.printf("%s * %s = %s", parametr, i, parametr * i);
            System.out.println();
        }
    }

    static void whileWyswietlTabliczke(int parametr) {
        int i = 1;
        while (i <= 10) {
            System.out.printf("%s * %s = %s\n", parametr, i, parametr * i);
            i += 1;
        }
        System.out.println();
    }

    static void doWhileWyswietlTabliczke(int parametr) {
        int i = 1;
        do {
            System.out.printf("%s*%s=%s\n", parametr, i, parametr * i);
            i += 1;
        } while (i <= 10);
    }
}
