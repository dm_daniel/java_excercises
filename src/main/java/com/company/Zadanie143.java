package com.company;

/*
*ZADANIE #143*
Policz silnię dla podanej wartości przy wykorzystaniu *rekurencji*.
 */
public class Zadanie143 {
    public static void main(String[] args) {
        System.out.println(policzSilnie(3));
    }

    private static int policzSilnie(int liczba) {
        System.out.println("liczba = " + liczba);
        if (liczba == 1) {
            return 1;
        } else {
            return liczba * policzSilnie(liczba - 1);
        }
    }


}
