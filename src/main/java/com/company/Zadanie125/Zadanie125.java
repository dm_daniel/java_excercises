package com.company.Zadanie125;
/*
*ZADANIE #125*
Utwórz klasę `Pojazd` (z polami `nazwa`) a następnie dziedziczące klasy klasy
- `Car` (z polami `liczbaKol` i `liczbaDrzwi`)
- `Pociag` (z polami `liczbaWagonow`, `czyMaWagonBarowy`)

Utwórz interfejs `CharakterystykaPojazdu`, zawierający metody `maxPredkosc()` oraz `liczbaPasazerow()`.
 */

import java.util.ArrayList;
import java.util.List;

public class Zadanie125 {
    public static void main(String[] args) {

        Samochod s1 = new Samochod("volvo", 4, 5);
        Pociag p1 = new Pociag("ŁKA", 17, true);
        System.out.println(s1);
        System.out.println(p1);
        System.out.println();
        s1.ustawParametry(190, 5);
        p1.ustawParametry(200, 80);
        System.out.println(s1);
        System.out.println(p1);

        //do listy jestem w stanie dowolny obiekt które implementują ten interfejs
        List<CharakterystykaPojazdu> lista = new ArrayList<>();
        lista.add(s1);
        lista.add(p1);

        for (CharakterystykaPojazdu charakterystykaPojazdu : lista) {
            System.out.println(charakterystykaPojazdu.liczbaPasazerow());
        }
    }
}
