package com.company.Zadanie125;

public class Samochod extends Pojazd {
    private int liczbaKol;
    private int liczbaDrzwi;


    public Samochod(String nazwa, int liczbaKol, int liczbaDrzwi) {
        super(nazwa);
        this.liczbaKol = liczbaKol;
        this.liczbaDrzwi = liczbaDrzwi;
    }

    @Override
    public String toString() {
        return String.format("Samochód %s, ma %s drzwi i %s kół.   %s.   %s.",
                nazwa,
                liczbaDrzwi,
                liczbaKol,
                maxPredkosc > 0 ? "Maksymalna predkosc to: " + maxPredkosc : "",
                liczbaPasazerow > 1 ? "Maksymalna liczba pasażerów to: " + liczbaPasazerow : "");
    }
}
