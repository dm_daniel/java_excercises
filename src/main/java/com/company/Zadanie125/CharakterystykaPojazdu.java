package com.company.Zadanie125;

//    zawierający metody `maxPredkosc()` oraz `liczbaPasazerow()`.
public interface CharakterystykaPojazdu {

    int maxPredkosc();

    int liczbaPasazerow();
}
