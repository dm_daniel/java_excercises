package com.company.Zadanie125;

public class Pociag extends Pojazd {
    private int liczbaWagoow;
    boolean czyMaWagonBarowy;

    public Pociag(String nazwa, int liczbaWagoow, boolean czyMaWagonBarowy) {
        super(nazwa);
        this.liczbaWagoow = liczbaWagoow;
        this.czyMaWagonBarowy = czyMaWagonBarowy;
    }

    @Override
    public String toString() {
        return String.format("Pociąg %s ma %s wagonow %s.  %s.    %s.",
                nazwa,
                liczbaWagoow,
                czyMaWagonBarowy ? "i ma wagon barowy" : "i nie ma wagonu barowego",       //warunek ze znakiem zapyania: jestli czywagonbarowy=true.... =false....
                maxPredkosc > 0 ? "Maksymalna predkosc to: " + maxPredkosc : "",
                liczbaPasazerow > 1 ? "Maksymalna liczba pasażerów to: " + liczbaPasazerow : "");
    }
}

