package com.company.Zadanie125;

abstract class Pojazd implements CharakterystykaPojazdu {
    protected String nazwa;
    protected int maxPredkosc;
    protected int liczbaPasazerow;

    public Pojazd(String nazwa) {
        this.nazwa = nazwa;
    }

    //metody finalne nie mogą być nadpisanie
    @Override
    public final int maxPredkosc() {
        return maxPredkosc;
    }

    @Override
    public final int liczbaPasazerow() {
        return liczbaPasazerow;
    }

    final void ustawParametry(int maxPredkosc, int liczbaPasazerow) {
        this.maxPredkosc = maxPredkosc;
        this.liczbaPasazerow = liczbaPasazerow;

    }
}
