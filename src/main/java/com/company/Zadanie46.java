package com.company;
/*
*ZADANIE 46*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma usunąć z podanej tablicy element o wybranym indeksie i zwrócić nową tablicę.
Zaimplementuj dodatkowo metodę służącą do wyświetlania tablicy.
> Dla `([1, 2, 3, 4, 5],  2)`
> zwróci `[1, 2, 4, 5]`
 */

public class Zadanie46 {
    public static void main(String[] args) {
        int[] tablica = {1, 2, 3, 4, 5};
        wyswietlTablice(tablica);
        wyswietlTablice(usunElement(tablica, 2));

    }

    static int[] usunElement(int[] tablicaWejsciowa, int indeksDoUsuniecia) {
        int[] nowaTablica = new int[tablicaWejsciowa.length - 1];       //nowa musi byc krotsza o 1 bo usuwamy element
        for (int pozycja = 0; pozycja < indeksDoUsuniecia; pozycja++) {
            nowaTablica[pozycja] = tablicaWejsciowa[pozycja];

        }
        for (int pozycja = indeksDoUsuniecia; pozycja < nowaTablica.length; pozycja++) {
            nowaTablica[pozycja] = tablicaWejsciowa[pozycja + 1];
        }
        return nowaTablica;
    }

    static void wyswietlTablice(int[] cokolwiek) {                          //obczaić o co chodzi z tą metodą!!
        for (int pozycja = 0; pozycja < cokolwiek.length; pozycja++) {
            System.out.print(cokolwiek[pozycja] + " ,");
        }
        System.out.println();
    }
}
