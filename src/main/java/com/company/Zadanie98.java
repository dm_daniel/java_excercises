package com.company;

import java.util.ArrayList;
import java.util.List;

public class Zadanie98 {
    /*
    *ZADANIE #98*
Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca ich sumę.
     */
    public static void main(String[] args) {

        List<Integer> myList = new ArrayList<>();
        myList.add(5);
        myList.add(10);
        myList.add(15);

        System.out.println(zwrocSumeZListy1(myList));
        System.out.println(zwrocSumeZListy2(myList));
        System.out.println(zwrocSume200IQ(myList));
    }

    static int zwrocSumeZListy1(List<Integer> lista) {
        int suma = 0;
        for (Integer numer : lista) {
            suma += numer;
        }
        return suma;
    }

    static int zwrocSumeZListy2(List<Integer> lista) {
        int suma = 0;

        for (int pozycja = 0; pozycja < lista.size(); pozycja++) {
            suma += lista.get(pozycja);
        }
        return suma;
    }

    static int zwrocSume200IQ(List<Integer> lista) {
        return lista.get(0) + lista.get(1) + lista.get(2);
    }
}
