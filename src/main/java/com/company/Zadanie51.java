package com.company;

import java.util.Arrays;

/*
*ZADANIE #51*
Utwórz metodę, która przyjmuje dwie tablice. Metoda ma zwrócić tablicę z sumą kolumn podanych tablic.
To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
*Przyjęte założenia:*
- Tablice są tych samych długości.
- Wartości na danej pozycji mogą być większę niż 10             !!!! do domu:to samo,tylko tablice mogą być różnych długości !!!!

[1,  2,  3,  4]
[5,  6,  7,  8]
[6,  8, 10, 11]
 */
public class Zadanie51 {
    public static void main(String[] args) {
        int[] tablica1 = new int[]{1, 2, 3, 4};
        int[] tablica2 = new int[]{7, 1, 2, 11};
        System.out.println(Arrays.toString(zwrocTabliceZSumaKolumn(tablica1, tablica2)));
    }

    static int[] zwrocTabliceZSumaKolumn(int[] tablica1, int[] tablica2) {
        int[] tablicaZSumami = new int[tablica1.length];    //zakladamy ze dlugosci tablicy sa te same wiec trzecia moze byc tak dluga jak tablica1 lub tablica2...

        for (int index = 0; index < tablica1.length; index++) {    ///tez jest obojetnie do rozmiaru ktorej z nich iterujemy
            tablicaZSumami[index] = tablica1[index] + tablica2[index];      //przypisujemy wartosc z 1 tab. pod jakims indeksem + wartosc z 2 tab. pod jakims indeksem, one sa sumowane i przypisywane
        }
        return tablicaZSumami;
    }
}
