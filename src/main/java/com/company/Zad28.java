package com.company;
/*
Utwórz metodę, do której przekazujesz dwa parametry. Metoda ma wyświetlić wszystkie potęgi pierwszej liczby do momentu nie przekroczenia drugiej liczby.
> dla `3, 100` wyświetli:
>
```0 -> 1
1 -> 3
2 -> 9
3 -> 27
4 -> 81 (kolejny krok dałby 243, więc przekroczyłby 100)```
>
> dla `4, 300` wyświetli:
>
```0 -> 1
1 -> 4
2 -> 14
3 -> 64
4 -> 256 (kolejny krok dałby 1024, więc przekroczyłby 300)```
 */

public class Zad28 {
    public static void main(String[] args) {

        System.out.println("for:");
        forWyswietlPotegi(2, 100);
        System.out.println("while:");
        whileWyswietlPotegi(2, 100);
        System.out.println("do while:");
        doWhileWyswietlPotegi(2, 100);


    }

    static void forWyswietlPotegi(int liczbaPotegowana, int liczbaKoncowa) {

        for (int potega = 0; ; potega++) {
            double wynik = Math.pow(liczbaPotegowana, potega);
            if (wynik > liczbaKoncowa) {
                break;
            }
            System.out.printf("%s^%s = %s\n", liczbaPotegowana, potega, wynik);
        }
    }

    static void whileWyswietlPotegi(int liczbaPotegowana, int liczbaKoncowa) {

        int potega = 0;
        double wynik = 1;

        while (wynik < liczbaKoncowa) {
            System.out.printf("%s^%s = %s\n", liczbaPotegowana, potega, wynik);
            potega += 1;
            wynik = Math.pow(liczbaPotegowana, potega);
        }
    }

    static void doWhileWyswietlPotegi(int liczbaPotegowana, int liczbaKoncowa) {

        int potega = 0;
        double wynik = 1;

        do {
            System.out.printf("%s^%s = %s\n", liczbaPotegowana, potega, wynik);
            potega += 1;
            wynik = Math.pow(liczbaPotegowana, potega);
        } while (wynik <= liczbaKoncowa);
    }
}
