package com.company.Zadanie119;

public class Wykladowca extends Osoba {

    private TytulNaukowy tytulNaukowy;

    public Wykladowca(String imie, int wiek, Plec plec, TytulNaukowy tytul) {       //rozne typy nazw tytulu
        super(imie, wiek, plec);
        this.tytulNaukowy = tytul;
    }

    enum TytulNaukowy {
        DR, MGR, INZ
    }
}
