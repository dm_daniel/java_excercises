package com.company.Zadanie119;

import java.util.HashMap;
import java.util.Map;

public class Student extends Osoba {
    private int rokStudiow;
    private Uczelnia uczelnia;
    private Map<PrzedmiotUczelniany, Integer> oceny = new HashMap<>();

    public Student(String imie, int wiek, Plec plec, int rokStudiow) {
        super(imie, wiek, plec);        //wywołuje konsrukot z nadklas
        this.rokStudiow = rokStudiow;
    }

    void dodajOcene(PrzedmiotUczelniany przedmiot, int ocena) {      //wrzucamy wpis do mapy
        oceny.put(przedmiot, ocena);
    }

    @Override
    public String toString() {
        return super.toString() + " na roku " + rokStudiow + " ma oceny: " + oceny;
    }
}
