package com.company.Zadanie119;

import java.util.ArrayList;
import java.util.List;

public class Uczelnia {
    private String nazwa;
    private String adres;
    private List<Student> listaStudentow = new ArrayList<>();
    private List<Wykladowca> listaWykladowcow = new ArrayList<>();

    public Uczelnia(String nazwa, String adres) {
        this.nazwa = nazwa;
        this.adres = adres;
    }

    void dodajStudenta(Student student) {       //typ nazwa
        listaStudentow.add(student);
    }

    void dodajWykladowce(Wykladowca wykladowca) {
        listaWykladowcow.add(wykladowca);
    }

    void wyswietlStudentow() {
        System.out.println("Na uczelni " + nazwa + " studiują:");

        for (Student student : listaStudentow) {
            System.out.println(student);
        }
    }

}
