package com.company.Zadanie119;
/*
*ZADANIE #119*
*Utwórz klasę `GenericClass` z polami:*
- `imie`,
- `wiek`
- `plec` (jako typ wyliczeniowy).


*Utwórz klasę `Student` (dziedziczący po klasie `GenericClass`) z polami*
- `uczelnia` (obiekt klasy `Uczelnia`)
- `rokStudiow` (Integer)
- mapę w postaci `PrzedmiotUczelniany : Integer`

*Utwórz klasę `PrzedmiotUczelniany` z polami*
- `nazwa`
- `wykladowca`

*Utwórz klasę `Wykladowca` z polami:*
- lista zawierając obiekty klasy `Uczelnia`).
- tytuł naukowy (typ wyliczeniowy)


*Utwórz klasę `Uczelnia` z polami*
-`nazwa`
- `adres`
- lista studentów
- lista wykładowców
 */


import static com.company.Zadanie119.Osoba.Plec.*;
import static com.company.Zadanie119.Wykladowca.TytulNaukowy.*;

public class Zadanie119 {
    public static void main(String[] args) {
        Osoba osoba1 = new Osoba("Daniel", 28, MEZCZYZNA);  // zaimportowałem alt+enter klase GenericClass i typ Plec wiec nie musze tu wpisywac
        System.out.println(osoba1);

        Wykladowca wykladowcaMatematyki = new Wykladowca("Ala", 50, MEZCZYZNA, DR);
        PrzedmiotUczelniany matematyka = new PrzedmiotUczelniany("matematyka", wykladowcaMatematyki);
        PrzedmiotUczelniany fizyka = new PrzedmiotUczelniany("fizyka", wykladowcaMatematyki);
        Student student1 = new Student("Krzysiu Chomiczewski", 26, MEZCZYZNA, 5);
        student1.dodajOcene(matematyka, 4);
        student1.dodajOcene(matematyka, 4);
        student1.dodajOcene(fizyka, 6);

        Uczelnia uczelnia1 = new Uczelnia("UŁ", "Pomorska 105");
        uczelnia1.dodajWykladowce(wykladowcaMatematyki);
        uczelnia1.dodajStudenta(student1);
        uczelnia1.wyswietlStudentow();
    }
}
