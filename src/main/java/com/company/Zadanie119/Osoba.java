package com.company.Zadanie119;

public class Osoba {        //informacja na pasku, że jakaś klasa niżej dziedziczy po tej klasie
    private String imie;  //hermetyzacja
    private int wiek;
    private Plec plec;

    enum Plec {                 //typ wyliczeniowy
        KOBIETA, MEZCZYZNA
    }

    public Osoba(String imie, int wiek, Plec plec) {
        this.imie = imie;
        this.wiek = wiek;
        this.plec = plec;
    }

    @Override
    public String toString() {
        return String.format("Imie: %s, wiek: %s, plec: %s",
                imie, wiek, plec);
    }
}
