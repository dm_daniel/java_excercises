package com.company;
/*
*ZADANIE #109*
Porównaj czasy `LinkedList<>` i `ArrayList<>`:
- dodawania na końcu
- dodawania na początku (index `0`)
- wybierania spod indexu (po kolei)
- usuwania spod indexu (po kolei) (edited)
asd
dsa
 */

import java.util.ArrayList;
import java.util.LinkedList;

public class Zadanie109 {
    private static final int LICZBA_OPERACJI = 1_000_000;
    private static final int ELEMENT = 123;

    public static void main(String[] args) {
        long czasStart = System.currentTimeMillis();
        ArrayList<Integer> arrayList = dodawanieDoListy();
        long czasStop = System.currentTimeMillis();
        System.out.println("Wypełnienie ArrayListy na koniec: " + (czasStop - czasStart));

        czasStart = System.currentTimeMillis();
        LinkedList<Integer> linkedList = dodawanieDoLinkedList();
        czasStop = System.currentTimeMillis();
        System.out.println("Wypełnienie LinkedListy na koniec: " + (czasStop - czasStart));

        czasStart = System.currentTimeMillis();
        ArrayList<Integer> arrayListNaPoczatek = dodawanieDoListyNaPoczatek();
        czasStop = System.currentTimeMillis();
        System.out.println("Wypełnienie ArrayListy na poczatek: " + (czasStop - czasStart));

        czasStart = System.currentTimeMillis();
        LinkedList<Integer> linkedListNaPoczatek = dodawanieDoLinkedListNaPoczatek();
        czasStop = System.currentTimeMillis();
        System.out.println("Wypełnienie LinkedListy na poczatek: " + (czasStop - czasStart));

        czasStart = System.currentTimeMillis();
        czasStop = System.currentTimeMillis();
        System.out.println("Dodaje od podania indexu ArrayListy: " + (czasStop - czasStart));
    }

    static ArrayList<Integer> dodawanieDoListy() {

        ArrayList<Integer> nowaLista = new ArrayList<>();       //Integer a = new Integer(5)   ==   int a = 5
        for (int i = 0; i < LICZBA_OPERACJI; i++) {
            nowaLista.add(ELEMENT);
        }
        return nowaLista;
    }

    static LinkedList<Integer> dodawanieDoLinkedList() {
        LinkedList<Integer> nowaLista = new LinkedList<>();
        for (int i = 0; i < LICZBA_OPERACJI; i++) {
            nowaLista.add(ELEMENT);
        }
        return nowaLista;
    }

    static ArrayList<Integer> dodawanieDoListyNaPoczatek() {

        ArrayList<Integer> nowaLista = new ArrayList<>();       //Integer a = new Integer(5)   ==   int a = 5
        for (int i = 0; i < LICZBA_OPERACJI; i++) {
            nowaLista.add(0, ELEMENT);
        }
        return nowaLista;
    }

    static LinkedList<Integer> dodawanieDoLinkedListNaPoczatek() {
        LinkedList<Integer> nowaLista = new LinkedList<>();
        for (int i = 0; i < LICZBA_OPERACJI; i++) {
            nowaLista.add(0, ELEMENT);
        }
        return nowaLista;
    }


}
