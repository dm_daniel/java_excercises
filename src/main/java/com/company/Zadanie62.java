package com.company;
/*
*ZADANIE #62*
Utwórz metodę, która przyjmuje dwa parametry - długość i szerokość tablicy,
a następnie zwraca nowo utworzoną, dwuwymiarową tablicę wypełnioną losowymi wartościami.
Utwórz drugą metodę do wyświetlania zwróconej tablicy.
 */

import java.util.Random;

public class Zadanie62 {
    public static void main(String[] args) {
        int[][] tablicaZLiczbami = zwrocTabliceZpodanymiParametrami(3, 4);
        wyswietlTablice(zwrocTabliceZpodanymiParametrami(3, 4));


    }

    static int[][] zwrocTabliceZpodanymiParametrami(int dlugosc, int szerokosc) {
        int[][] nowaTablica = new int[dlugosc][szerokosc];

        Random random = new Random();

        for (int i = 0; i < dlugosc; i++) {
            for (int j = 0; j < szerokosc; j++) {
                nowaTablica[i][j] = random.nextInt(100)-10;       //jaka maxymalna  liczba ma zostac osiagnieta od 0 do tej liczby czyli tu od 0->99,  -10 bedzie max
            }
        }

        return nowaTablica;
    }

    static void wyswietlTablice(int[][] tablica) {          //metoda sluzaca do wyswietlania

        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[i].length; j++) {       //lub tablica[0] jesli tablice sa rownych dlugości
                System.out.print(tablica[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
