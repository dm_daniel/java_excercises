package com.company.zadanie159WzorceProjektowePanelDomu;

import java.util.Arrays;
import java.util.List;

public class Facade {
    private final int TEMP_OUT = 18;
    private final int TEMP_IN = 24;
    private List<Windows> windowsList = Arrays.asList(
            new Windows(),
            new Windows(),
            new Windows()

    );
    private Alarm alarm = new Alarm();
    private Light light = new Light();
    private Heating heating = new Heating();

    void imOut() {
        windowsList.stream()
                .forEach(Windows::close);     // odwołanie się przez referencje   STARA wersja.  (okno -> okno.close() )
        alarm.enable();
        light.switchOff();
        heating.setTemp(TEMP_OUT);

    }

    void imBack() {
        windowsList.stream()
                .forEach(Windows::open);    //referencja  . STARA wersja (okno -> okno.open() )
        alarm.disable();
        light.switchOn();
        heating.setTemp(TEMP_IN);
    }

}
