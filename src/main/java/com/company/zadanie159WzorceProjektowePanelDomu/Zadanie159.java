package com.company.zadanie159WzorceProjektowePanelDomu;

import java.util.Scanner;

public class Zadanie159 {
    final static int WEJSCIE = 1;
    final static int WYJSCIE = 2;

    public static void main(String[] args) {
        Facade panelSterujacy = new Facade();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Możliwe opcje: ");
        System.out.println(WEJSCIE + " - wchodzisz do domu");
        System.out.println(WYJSCIE + " - wychodzisz z domu");
        int decyzja = scanner.nextInt();


        switch (decyzja) {

            case WEJSCIE:
                panelSterujacy.imBack();
                break;

            case WYJSCIE:
                panelSterujacy.imOut();
                break;

            default:
                System.out.println("Wybierz jeszcze raz");

        }
    }
}
