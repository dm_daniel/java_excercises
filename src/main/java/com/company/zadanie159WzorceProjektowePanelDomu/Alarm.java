package com.company.zadanie159WzorceProjektowePanelDomu;

public class Alarm {

     void enable() {
        System.out.println("Alarm został włączony.");
    }

     void disable() {
        System.out.println("Alarm został wyłączony.");
    }
}
