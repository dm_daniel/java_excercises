package com.company;
/*
*ZADANIE #47*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz wartość logiczną.
Gdy drugi parametr ma wartość `true` metoda ma zwrócić *największa* liczbę z tablicy.
Gdy parametr będzie miał wartość `false`, metoda ma zwrócić *najmniejszą* liczbę z tablicy.
 */

public class Zadanie47 {                                            //Obczaić to zadanie za pomocą 1 pętli ablo skrócić !!!
    public static void main(String[] args) {
        int[] tablica = new int[]{1};
        System.out.println(zwrocMaxLubMin(tablica, true));
        System.out.println(zwrocMaxLubMin(tablica, false));
    }

    static int zwrocMaxLubMin(int[] table, boolean isMax) {         //isMax -> czy najwieksza
        int max = table[0];
        int min = table[0];
        for (int i = 0; i < table.length; i++) {                    //!! obczaić zmianę indeksu
            if (table[i] > max) {
                max = table[i];
            }
            if (table[i] < min) {
                min = table[i];
            }

        }
        return isMax ? max : min;          //operator ternary... zmienna? jesli warunek spelniony : jesli nie spelniony
    }
}