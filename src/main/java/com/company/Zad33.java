package com.company;

/*
Utwórz metodę, która wyświetli prostokąt o podanych
wymiarach (użytkownik podaje jego wymiary jako parametry)
XXXXXXXXXX
X        X
X        X
XXXXXXXXXX```
 */
public class Zad33 {
    static String X = "X ";
    static String Spacja = "  ";

    public static void main(String[] args) {
        wyswietlProstokat(5, 5);
    }

    static void wyswietlProstokat(int wysokosc, int szeroksoc) {
        for (int i = 1; i <= wysokosc; i++) {
            for (int j = 1; j <= szeroksoc; j++) {
                if (i > 1 && i <= wysokosc - 1 && j > 1 && j <= szeroksoc - 1) {
                    System.out.print(Spacja);
                } else {
                    System.out.print(X);
                }
            }

            System.out.println();
        }
    }
}


