package com.company.zadanie94;
/*
*ZADANIE #94*
Utwórz typ wyliczeniowy `Kolor` (zawierająca kilka opcji).
Utwórz klasę `Car`, gdzie jednym z pól będzie ten typ.
 */

public class Zadanie94 {
    public static void main(String[] args) {

        Samochod sam = new Samochod("Mazda", 2009, Kolor.BIALY);
        System.out.println(sam);
    }
}
