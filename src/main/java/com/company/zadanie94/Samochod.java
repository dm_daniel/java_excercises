package com.company.zadanie94;

public class Samochod {
    private String nazwa;
    private int rocznik;
    private Kolor kolor;    //typ Kolor nazwa kolor , własny typ

    Samochod(String nazwa, int rocznik, Kolor kolor) {
        this.nazwa = nazwa; // this.nazwa jest polem , nazwa jest parametrem
        this.rocznik = rocznik;
        this.kolor = kolor;
    }

    //metoda toString
    @Override
    public String toString() {
        return String.format("%s %s %s", nazwa, rocznik, kolor);
    }
}
