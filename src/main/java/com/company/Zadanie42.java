package com.company;

import java.util.Arrays;

/*
*ZADANIE #42*
Utwórz metodę, która jako parametr przyjmuje tablicę i *zwraca nową tablicę* z liczbami w odwrotnej kolejności.
 */
public class Zadanie42 {
    public static void main(String[] args) {
        int[] tab = {1,2,3};
        System.out.println(Arrays.toString(tab));
        System.out.println(Arrays.toString(odwrocTablice(tab)));

    }

    static int[] odwrocTablice(int[] tablica) {
        int n = tablica.length - 1;
        int[] nowatablica = new int[tablica.length];
        for (int i = 0; i < nowatablica.length; i++) {
            nowatablica[i] = tablica[n];        //zerowy element nowej tablicy staje się ostatnim elementem ze starej tablicy
            n--;
        }
        return nowatablica;
    }
}
