package com.company;
/*
*ZADANIE #106*
Utwórz metodę, która wczytuje
liczby od użytkownika do momentu podania duplikatu
a następnie zwraca set (np. `HashSet`) (z podanymi liczbami)
 */

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Zadanie106 {
    public static void main(String[] args) {


        System.out.println(zwrocSet());

    }

    static Set<Integer> zwrocSet() {

        //tworzymy listę zawierającą sety,
        //Sety nie zachowują wartosci w kolejności (worek z danymi)
        Set<Integer> jakisSet = new HashSet<>();
        int zmienna;
        Scanner s = new Scanner(System.in);

        while (true) {
            System.out.print("Podaj liczba: ");
            zmienna = s.nextInt();
            boolean udaloSieDodac = jakisSet.add(zmienna);
            System.out.println(udaloSieDodac);
            if (!udaloSieDodac) {       //set przyjmuje konkretną wartość tylko 1 raz
                break;
            }
        }
        return jakisSet;
    }
}
