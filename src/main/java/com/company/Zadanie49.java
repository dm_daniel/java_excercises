package com.company;

import java.sql.SQLOutput;

/*
*ZADANIE #49*
Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby które są indexami (pozycjami) zakresu.
Metoda ma zwrócić sumę elementów w podanym przedziale.
> Dla `([1, 2, 3, 4, 5], 2, 4)`
> zwróci `12`, bo `3 + 4 + 5`
 */
public class Zadanie49 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 2, 3, 4, 5, 6, 7};
        System.out.println(zwrocSumePrzedzialu(tablica, 1, 4));       //sprawdzic manewrowac przedzialem !!
    }

    static int zwrocSumePrzedzialu(int[] tablica, int start, int stop) {
        int suma = 0;

        if (start > stop || start < 0 || stop > tablica.length - 1) {       //---zabezpieczenia do podawanego zakresu----bo tablice numeruje od 0
            System.out.println("podano zły zakres...");
        } else {

            for (int index = start; index <= stop; index++) {
                suma += tablica[index];

            }
        }
        return suma;
    }
}
