package com.company.zadanie163WzorzecStrategia;

public interface Strategia {
    double policz(double liczbaPierwsza, double liczbaDruga);

    default String nazwaOperacji() {     //od jdk 1.8 jest default. Metoda domyślna, nie jest obowiązkowa dla klas implementujących, ale ona tam jest
        return " jakąś strategię";
    }
}
