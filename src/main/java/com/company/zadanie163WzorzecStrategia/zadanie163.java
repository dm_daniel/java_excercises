package com.company.zadanie163WzorzecStrategia;

public class zadanie163 {
    public static void main(String[] args) {

        Kalkulator kalkulator = new Kalkulator();

        kalkulator.setObecnieWybranaStrategia(new StrategiaDodawania());
        System.out.println(kalkulator.policz(4, 11));

        kalkulator.setObecnieWybranaStrategia(new StrategiaOdejmowania());
        System.out.println(kalkulator.policz(5, 100));

        //strategia mnożenia jest ustawiona jako singleton
        kalkulator.setObecnieWybranaStrategia(StrategiaMnozenia.getInstance());
        System.out.println(kalkulator.policz(3, 3));

        kalkulator.setObecnieWybranaStrategia(new StrategiaDzielenia());
        System.out.println(kalkulator.policz(11, 0));
        System.out.println(kalkulator.policz(11, 11));

        //nie musze tworzyć nowej strategi tylko odwołuje się do singletonu mnożenia
        kalkulator.setObecnieWybranaStrategia(StrategiaMnozenia.getInstance());
        System.out.println(kalkulator.policz(3, 3));

    }
}
