package com.company.zadanie163WzorzecStrategia;

//mnozenie robimy jako singleton
public class StrategiaMnozenia implements Strategia {

    private static StrategiaMnozenia instance = null;

    private StrategiaMnozenia() {
    }

    static StrategiaMnozenia getInstance() {
        if (instance == null) {
            instance = new StrategiaMnozenia();
        }
        return instance;
    }

    @Override
    public double policz(double liczbaPierwsza, double liczbaDruga) {
        return liczbaPierwsza * liczbaDruga;
    }
}
