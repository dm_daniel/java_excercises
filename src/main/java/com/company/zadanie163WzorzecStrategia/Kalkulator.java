package com.company.zadanie163WzorzecStrategia;

public class Kalkulator {
    private Strategia obecnieWybranaStrategia;      //kalkulator przechowuje obecnie wybraną strategie

    double policz(int liczbaPierwsza, int liczbaDruga) {
        if (obecnieWybranaStrategia == null) {
            return 0;
        }
        return obecnieWybranaStrategia.policz(liczbaPierwsza, liczbaDruga);
    }

    public void setObecnieWybranaStrategia(Strategia obecnieWybranaStrategia) {
        this.obecnieWybranaStrategia = obecnieWybranaStrategia;
        System.out.println("wybrano " + obecnieWybranaStrategia.nazwaOperacji());
    }
}
