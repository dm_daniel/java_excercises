package com.company.zadanie163WzorzecStrategia;

public class StrategiaOdejmowania implements Strategia {
    @Override
    public double policz(double liczbaPierwsza, double liczbaDruga) {
        return liczbaPierwsza - liczbaDruga;
    }

}
