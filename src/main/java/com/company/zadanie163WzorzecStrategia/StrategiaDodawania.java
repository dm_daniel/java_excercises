package com.company.zadanie163WzorzecStrategia;

public class StrategiaDodawania implements Strategia {
    @Override
    public double policz(double liczbaPierwsza, double liczbaDruga) {
        if (liczbaDruga == 0) {
            return 0;
        }
        return liczbaPierwsza + liczbaDruga;
    }

    @Override
    public String nazwaOperacji() {
        return "dodawanie";
    }
}
