package com.company;

/*
Utwórz metodę, do której przekazujesz liczbę, która jest numerem miesiąca,
a program powinien *zwrócić nazwę* tego miesiąca.
Wykorzystaj instrukcję warunkową *switch*
> Dla `1` zwróci `styczeń`
>
> Dla `2` zwróci `luty`
>
>`...`
>
> Dla `7` zwróci `lipiec`
 */
public class Zadanie18 {
    public static void main(String[] args) {
        System.out.println(zwrocMiesiac(5));
        System.out.println(zwrocMiesiac(12));
        System.out.println(zwrocMiesiac(0));
        System.out.println();
        System.out.println(miesiacInaczej(11));
    }

    static String zwrocMiesiac(int nrMiesiaca) {     //String jest klasą wiec z wielkiej litery
        switch (nrMiesiaca) {
            case 1:
                return "Styczeń";

            case 2:
                return "Luty";

            case 3:
                return "Marzec";

            case 4:
                return "Kwiecień";

            case 5:
                return "Maj";

            case 6:
                return "Czerwiec";

            case 7:
                return "Lipiec";

            case 8:
                return "Sierpień";

            case 9:
                return "Wrzesień";

            case 10:
                return "Październik";

            case 11:
                return "Listopad";

            case 12:
                return "Grudzień";

            default:
                return "błędny numer miesiaca";


        }
    }

    static String miesiacInaczej(int nrMiesiaca) {
        String komunikat = "błędny numer";
        switch (nrMiesiaca) {
            case 3:
                komunikat = "Marzec";
                break;
            case 4:
                komunikat = "Kwiecień";
                break;
            case 5:
                komunikat = "maj";
                break;
//                default:
//                        return"zły numer miesiaca";       //default nie jest potrzebny bo ustawilismy go na poczatku metody

        }
        return komunikat;
    }

}
