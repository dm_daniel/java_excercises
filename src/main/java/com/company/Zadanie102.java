package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
*ZADANIE #102*
Utwórz metodę, która przyjmuje trzy parametry - listę liczb (np. `ArrayList`), początek zakresu (`int`) oraz koniec zakresu (`int`) a zwrócić liczbę.
Metoda ma policzyć różnicę pomiędzy największym i najmniejszym elementem w tym zakresie.
Policzona różnica jest indeksem elementu z tablicy wejściowej który należy zwrócić (jeśli wartość będzie poza zakresem indeksów metoda ma zwrócić `-1`) (edited)
 */
public class Zadanie102 {
    public static void main(String[] args) {             // 0  1  2  3  4  5  6
        List<Integer> lista = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

//        System.out.println(zwrociRozniceZZakresu(lista, 2, 4));

        System.out.println(pokazIndeksZRoznicy(lista, 3, 6));
    }

    static Integer pokazIndeksZRoznicy(List<Integer> lista, int start, int end) {
        Integer indeks = zwrociRozniceZZakresu(lista, start, end);

        if (indeks < 0 || indeks > lista.size() - 1) {
            return -1;
        }
        return lista.get(indeks);
    }

    static Integer zwrociRozniceZZakresu(List<Integer> lista, int start, int end) {
        Integer max = lista.get(start);
        Integer min = lista.get(start);

        for (int i = start; i <= end; i++) {
            if (lista.get(i) > max) {
                max = lista.get(i);
            }
            if (lista.get(i) < min) {
                min = lista.get(i);
            }
        }
        return max - min;
    }
}
