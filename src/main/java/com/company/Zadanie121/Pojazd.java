package com.company.Zadanie121;

abstract class Pojazd {     //abstrakcyjna klasa ( nie można utworzyć jej obiektu)

    private String nazwa;
    private int maxPredkosc;
    private int liczbaPasazerow;

    Pojazd(String nazwa, int maxPredkosc, int liczbaPasazerow) {
        this.nazwa = nazwa;
        this.maxPredkosc = maxPredkosc;
        this.liczbaPasazerow = liczbaPasazerow;
    }
   abstract void przedstawSie();
}
