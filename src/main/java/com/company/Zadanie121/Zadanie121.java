package com.company.Zadanie121;

/*
*ZADANIE #121*
Utwórz klasę `Pojazd` (z polami `nazwa`, `maxPredkosc` i `liczbaPasazerow`) a następnie klasy dziedziczące:
- `Car` (z polami `liczbaKol` i `liczbaDrzwi`)
- `Pociag` (z polami `liczbaWagonow`, `czyMaWagonBarowy`)
 */
public class Zadanie121 {
    public static void main(String[] args) {

        Samochod samochod1 = new Samochod("toyota", 120, 5, 4, 5);
        Pociag pociag1 = new Pociag("PKP", 65, 100);
        //        Pojazd pojazd1 = new Pojazd("rower",50,1); nie jestesmy w stanie utworzyc obiektów klasy abstract
    }
}
