package com.company.Zadanie121;

public class Samochod extends Pojazd {

    private int liczbaKol = 4;
    private int liczbaDrzwi;

    public Samochod(String nazwa, int maxPredkosc, int liczbaPasazerow, int liczbaKol, int liczbaDrzwi) {
        super(nazwa, maxPredkosc, liczbaPasazerow);
        this.liczbaKol = liczbaKol;
        this.liczbaDrzwi = liczbaDrzwi;
    }

    public Samochod(String nazwa, int maxPredkosc, int liczbaPasazerow) {
        super(nazwa, maxPredkosc, liczbaPasazerow);

    }

    // nadpisuje abstrakcyjną metode z klasy Pojazd, ona już nie jest abstrakcyjna
    @Override
    void przedstawSie() {
    }
}
