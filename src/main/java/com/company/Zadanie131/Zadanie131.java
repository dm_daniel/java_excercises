package com.company.Zadanie131;
/*
*ZADANIE #131*
Utwórz metodę, która przyjmuje dwa parametry - napis (`String`)
oraz liczbę (`int`) wcześniej odczytaną `Scanner`-em od użytkownika.

Metoda ma wyświetlić podany napis, przekazaną liczbę razy.

W przypadku gdy liczba wystąpień będzie mniejsza bądź równa zero
powinien zostać rzucony własny wyjątek (dziedziczący po `RuntimeException`).
 */


import java.util.Scanner;

public class Zadanie131 {       //sygnatura klasy
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Ile razy chcesz wyświetlić napis ?");
        int liczbaWystapien = scanner.nextInt();
        scanner.nextLine();         //zrzuca podane elementy ze schowka( usuwa nasz enter zatwierdzający podanie pierwszej danej liczby wystąpień)
        System.out.print("Podaj napis");
        String napis = scanner.nextLine();

        try {
            wyswietlNapis(napis, liczbaWystapien);
        } catch (Exception exc) {       //exc to zmienna d której jest przypisany jest obiekt throw
            System.out.println(exc.getMessage());
        }
    }

    static void wyswietlNapis(String napis, int ileRazy) {

        if (ileRazy <= 0) {
            throw new ZbytMalaLiczbaWykonan(ileRazy + " nie jest poprawną liczbą wystąpień");
        }

        for (int i = 0; i < ileRazy; i++) {
            System.out.println(napis);
        }
    }
}
