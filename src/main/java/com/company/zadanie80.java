package com.company;
/*
*ZADANIE #80*
Utwórz metodę, która przyjmuje liczbę w formie napisu, a następnie ma zwrócić (jako `String`) sumę wszystkich cyfr
> Dla `"536"`, zwróci `"14"` (bo 5 + 3 + 6 = 14)
> Dla `"12345"`, zwróci `"15"`
 */

public class zadanie80 {
    public static void main(String[] args) {

        System.out.println(zwrocSume("563"));
        System.out.println(zwrocSume("100"));

    }

    private static String zwrocSume(String liczba) {

        String[] tablica = liczba.split(""); //rozdzielamy napis 536 za kazda litera( co znak)np a|l|a
        int suma = 0;
        for (String cyfra : tablica) {
            suma += Integer.parseInt(cyfra);       //statyczna metoda parseInt(metode statyczną wywołuje na nazwie klasy Integer....nie trzeba tworzyć nowego obiekut),
            // przetwórz na int,  Double.parseDouble(typ docelowy)
        }
        return String.valueOf(suma);            //przyjmuje int zwraca string, również wywołuje statyczną metodę na klasie String
    }
}
