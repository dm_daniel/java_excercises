package com.company;

public class Zadanie6 {
    public static void main(String[] args) {
           // methodOne(10, 20);
        methodTwo(10, 20);
    }

    static void methodOne(int a, int b) {      //shift F6 zmiana nazwy dla wszystkich elementów np.a
        System.out.println(a + "+" + b + "=" + (a + b));
        System.out.println(a + "-" + b + "=" + (a - b));
        System.out.println(a + "*" + b + "=" + (a * b));
        System.out.println(a + "/" + b + "=" + (a / b));
    }

    static void methodTwo(int numberOne, int numberTwo) {
        System.out.printf("%s + %s = %s \n", numberOne, numberTwo, numberOne + numberTwo);        //printf wyswietli zgodnie z formatem
        System.out.printf("%s - %s = %s \n", numberOne, numberTwo, numberOne - numberTwo);         //%s wyswietla jako string po kolei zmienne przypisuje do %s
        System.out.printf("%s * %s = %s \n", numberOne, numberTwo, numberOne * numberTwo);         //%_ znak specjalny
        System.out.printf("%s / %s = %s \n", numberOne, numberTwo, numberOne / numberTwo);
    }
}
