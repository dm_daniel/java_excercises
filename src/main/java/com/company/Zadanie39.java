package com.company;
/*
*ZADANIE #39*
Utwórz metodę, która przyjmuje dwa parametry - tablicę (wartości `short`) oraz liczbę.
Metoda ma zwrócić informację (jako wartość logiczna) czy dana liczba znajduje się w tablicy.
 */

public class Zadanie39 {
    public static void main(String[] args) {
        short[] tablica = new short[]{1, 5, 10};
        //short zmienna =5; alternatywa
        boolean wynik = czyZnajdujeSie(tablica, (short) 5);
        boolean wynik2 = czyZnajdujeSie(tablica, (short) 12);            //musi byc inna zmienna zeby wywołac 2 raz metodę
        System.out.println(wynik ? "Liczba znajduje się w tablicy"
                : "Liczba nie znajduje się w tablicy");
        System.out.println(wynik2 ? "Liczba znajduje się w tablicy"
                : "Liczba nie znajduje się w tablicy");

    }

    static boolean czyZnajdujeSie(short[] tablica, short liczbaSzukana) {
        for (short liczba : tablica) {
            if (liczba == liczbaSzukana) {
                return true;
            }
        }
        return false;

    }
}
