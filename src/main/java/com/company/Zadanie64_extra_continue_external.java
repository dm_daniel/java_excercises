package com.company;
/*
*ZADANIE #64 - EXTRA*
Utwórz metodę, która przyjmuje tablicę liczb i zwraca rozmiar tablicy po usunięciu duplikatów
> Dla `[-7, 8, 8, 0, 2, 7, 2]` (ma 7 elementów)
> zwróci `5` (bo  tablica bez duplikatów to `[-7, 8, 0, 2, 7]`)
 */

import java.util.Arrays;

public class Zadanie64_extra_continue_external {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 1, 2, 2};
        System.out.println(zwrocRozmiarTablicy(tab));
    }


    static int zwrocRozmiarTablicy(int[] tablica) {
        int licznik = 0;

        external:
        for (int i = 0; i < tablica.length; i++) {
            for (int j = i + 1; j < tablica.length; j++) {
                if (tablica[i] == tablica[j]) {
                    continue external;
                }
            }
            licznik++;
        }

        return licznik;
    }
}




