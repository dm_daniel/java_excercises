package com.company.zadanie154KlasyAnonimowe;

class Komputer {

    void start() {
        prepare();
        System.out.println("komputer działa");
    }

    protected void prepare() {
        System.out.println("uruchamianie systemu");
    }

}
