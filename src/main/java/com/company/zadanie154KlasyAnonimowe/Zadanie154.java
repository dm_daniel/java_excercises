package com.company.zadanie154KlasyAnonimowe;

public class Zadanie154 {
    public static void main(String[] args) {

//       metoda1();
//        metoda2();
//        metoda3();
        metoda4();
    }

    private static void metoda1() {
        Komputer komputer = new Komputer();
        komputer.start();
    }

    private static void metoda2() {
        NowyKomputer nowyKomputer = new NowyKomputer();
        nowyKomputer.start();
    }

    private static void metoda3() {
        //Stworzenie klasy anonimowej,nadpisanie metody
        new Komputer() {
            @Override
            protected void prepare() {
                super.prepare();
                System.out.println("anonimowe przygotowanie systemu");

            }
        }.start();                  //wywołanie
    }

    private static void metoda4() { //przypisanie do obiektu,dziedziczymy po klasie komputer i zmieniamy działanie metod

        Komputer k = new Komputer() {
            @Override
            protected void prepare() {
                super.prepare();
                System.out.println("anonimowe przygotowanie");
            }

            @Override
            void start() {
                super.start();
                System.out.println("Anonimowy start");
            }
        };
        k.start();

    }
}
