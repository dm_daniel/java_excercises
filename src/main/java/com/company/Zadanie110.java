package com.company;
/*
*ZADANIE #110*
Utwórz metodę, które zwróci mapę gdzie kluczem jest skrót kraju (np. “PL”, “DE”, “ES”),
a wartościami pełna nazwa (np. `Polska`, `Niemcy`, `Hiszpania`).
 */

import java.util.HashMap;
import java.util.Map;

public class Zadanie110 {
    public static void main(String[] args) {

        Map<String, String> map = ZwrocMapeZKluczemKraju();
        System.out.println(map);

        Map<Integer, String> kierunkowy = zwrocMapeZKierunkowym();
        System.out.println(kierunkowy);

        System.out.println(zwrocKraj(zwrocMapeZKierunkowym(),12));
        System.out.println(zwrocKraj(zwrocMapeZKierunkowym(),48));
    }

    static Map<String, String> ZwrocMapeZKluczemKraju() {
        Map<String, String> mapa = new HashMap<>();      //najpierw piszemy typ bardziej ogólny z przodu (dobra praktyka)
        mapa.put("PL", "Polska");          //wstawianie za pomocą put
        mapa.put("DE", "Niemcy");           // klucze w mappie musza byc unikalne
        mapa.put("ESP", "Hiszpania");
        return mapa;
    }

    static Map<Integer, String> zwrocMapeZKierunkowym() {
        Map<Integer, String> mapa = new HashMap<>();
        mapa.put(48, "Polska");
        mapa.put(12, "Niemcy");
        mapa.put(17, "Hiszpania");
        return mapa;
    }

    static String zwrocKraj(Map<Integer, String> mapa, int kierunkowy) {
        return mapa.get(kierunkowy);
    }
}
