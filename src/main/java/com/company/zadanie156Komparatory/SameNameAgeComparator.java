package com.company.zadanie156Komparatory;

import java.util.Comparator;

public class SameNameAgeComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {  //metoda compare jest wymuszona poprzez implementację interfejsu Comperator( przyjmuje 2 obiekty)
        Pracownik p1 = (Pracownik) o1;  //rzutujemy obiekty przekazane jako parametr na naszych pracownikow(chcemy pracować na pracownikach :)  )
        Pracownik p2 = (Pracownik) o2;

        int nameCompareResult = p1.getImie().compareTo(p2.getImie());     //wartość zwracana przez metodę compare to(porównuje imiona)

        if (nameCompareResult == 0) {   //jeśli te imiona są takie same, porównaj mi wiek, jesli nie return nameCompareResult(jak te imiona stoją względem siebe -1 lub 1)
            int ageCompareResult = Integer.compare(p1.getWiek(), p2.getWiek());

            if (ageCompareResult == 0) {    //jeśli również wiek jest równy, porównanie po pensji tych osób
                return Integer.compare(p1.getPensja(), p2.getPensja());
            }

            return ageCompareResult;
        }
        return nameCompareResult;

    }
}
