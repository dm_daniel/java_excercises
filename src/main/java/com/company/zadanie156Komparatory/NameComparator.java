package com.company.zadanie156Komparatory;

import java.util.Comparator;

public class NameComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Pracownik p1 = (Pracownik) o1;   //ja twierdze że ten obiekt przekazany jest moim pracownikiem
        Pracownik p2 = (Pracownik) o2;   // rzutowanie tak jak np (int)7.5

        return p1.getImie().compareTo(p2.getImie());    //p1.get Imie porownaj z p2.get Imie
    }
}
