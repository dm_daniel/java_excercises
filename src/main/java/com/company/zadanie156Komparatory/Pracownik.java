package com.company.zadanie156Komparatory;

public class Pracownik implements Comparable<Pracownik> {       //jestesmy w stanie pracownika z innym <pracownikiem> (wymaga importowania metody compare to)
    private String imie;
    private int wiek;
    private int pensja;

    public Pracownik(String imie, int wiek, int pensja) {
        this.imie = imie;
        this.wiek = wiek;
        this.pensja = pensja;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getPensja() {
        return pensja;
    }

    public void setPensja(int pensja) {
        this.pensja = pensja;
    }

    @Override
    public String toString() {
        return String.format("%-10s| %-4s| %-10s",     // - ustawia do lewej strony,  %10s rezerwuje sobie 10 znaków
                imie, wiek, pensja);
    }

    @Override
    public int compareTo(Pracownik p) {     //metoda musi zwrócic >0 , <0 , lub 0. Jeśli zwróci 0 obiekty są takie same/Jeśli zwróci wartość ujemną obecny obiekt musi stać przed/Jeśli zwracam wartość dodatnią, obecny jest po przekazywanym
//        if (this.wiek == p.wiek) {
//            return 0;   //wiek obiektów jest taki sam
//        } else if (this.wiek < p.wiek) {
//            return -1; //obecny(this) obiekt jest pierwszy
//        } else {
//            return 1;   //przekazywany jako parametr obiekt jest pierwszy
//        }//przy zamianie -1 z 1 i odwrotnie posortujemy od najwiekszego do najmniejszego wiek
//
//        inny sposob
//        return Integer.compare(this.wiek, p.wiek);
//        inny sposob
        return this.wiek - p.wiek;
    }
}
