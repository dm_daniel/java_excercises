package com.company.zadanie156Komparatory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
 *ZADANIE #156*- interfejs `Comparable`
 */
public class Zadanie156 {
    private static List<Pracownik> listaPracowikow = Arrays.asList(
            new Pracownik("Karol", 50, 6000),
            new Pracownik("Ada", 30, 2500),
            new Pracownik("Pawel", 35, 3500),
            new Pracownik("Karolina", 20, 3000),
            new Pracownik("Karolina", 24, 2000),
            new Pracownik("Karol", 50, 4000),
            new Pracownik("Karol", 33, 6000)
    );

    public static void main(String[] args) {
//        metoda1();
//        metoda2();
        metoda3();
        metoda4();
    }

    static void metoda1() {
        wyswietlListePracownikow(listaPracowikow);

    }

    static void metoda2() {
        Collections.sort(listaPracowikow);
        wyswietlListePracownikow(listaPracowikow);  //posortowana tabela po wieku
    }

    static void metoda3() {
        listaPracowikow.sort(new NameComparator());         //używamy naszego utworzonego komparatora (musi być to obiekt i dlatego new NameComparator()  )
        wyswietlListePracownikow(listaPracowikow);
    }

    static void metoda4() {
        listaPracowikow.sort(new NameComparator());
        listaPracowikow.sort(new SameNameAgeComparator());  //nasz komparator, jeśli imiona są takie same, najpierw wyświetli obiekt z mniejszym wiekiem
        wyswietlListePracownikow(listaPracowikow);
    }

    static void wyswietlListePracownikow(List<Pracownik> lista) {
        System.out.println("  IMIE    | WIEK|PENSJA");
        System.out.println("==========|=====|======");
        for (Pracownik pracownik : lista) {
            System.out.println(pracownik);
        }
    }


}
