package com.company;
/*
*ZADANIE #23*
Utwórz metodę, w której *wyświetlisz co drugą* liczbę z przedziału `0` do `14` (krańcowy zakres przedziału ma być drugim parametrem metody)
> Dla `0, 4` wyświetli `0, 2, 4`
>
> Dla `6, 14` wyświetli `6, 8, 10, 12, 14`
 */


public class Zad23 {
    public static void main(String[] args) {
        wyswietlCoDrugo(0, 4);
        wyswietlCoDrugo(6, 14);
        System.out.println();
        whileWyswietlCoDrugo(0, 4);
        whileWyswietlCoDrugo(6, 14);
        System.out.println();
        doWhileWyswietlCoDrugo(0, 4);
        doWhileWyswietlCoDrugo(6, 14);
    }

    static void wyswietlCoDrugo(int poczatek, int granica) {

        for (int i = poczatek; i <= granica; i += 2) {
            System.out.print(i + " ");
        }
        System.out.println();

    }

    static void whileWyswietlCoDrugo(int poczatek, int granica) {
        while (poczatek <= granica) {
            System.out.print(poczatek + " ");
            poczatek += 2;
        }
        System.out.println();
    }

    static void doWhileWyswietlCoDrugo(int poczatek, int granica) {
        do {
            System.out.print(poczatek + " ");
            poczatek += 2;
        } while (poczatek <= granica);

        System.out.println();
    }
    }

