package com.company.Zadanie147;

/*
*ZADANIE #147*
Utwórz klasę `Kalkulator` z metodami umożliwiającymi:
- sumowanie dwóch liczb (`(Integer, Integer)`)
- sumowanie dowolnej liczby liczb (`varargs`)
- odejmowanie dwóch liczb (`(Integer, Integer)`)
 */
public class Kalkulator {
    public static void main(String[] args) {

    }

    public Integer dodawanieDwochLiczb(Integer liczba1, Integer liczba2) {
        if (liczba1 == null) {
            return liczba2;
        }
        if (liczba2 == null) {
            return liczba1;
        }
        return liczba1 + liczba2;
    }

    public Integer dodawanieWieluLiczb(int... liczby) {
        if (liczby == null) {
            return null;
        }

        int suma = 0;
        for (int i : liczby) {
            suma += i;
        }
        return suma;
    }

    public Integer odejmowanieDwochLiczb(Integer liczba1, Integer liczba2) {

        if (liczba1 == null && liczba2 == null) {
            return null;
        }
        if (liczba1 == null) {
            return -liczba2;
        }
        if (liczba2 == null) {
            return liczba1;
        }

        return liczba1 - liczba2;

    }
}
