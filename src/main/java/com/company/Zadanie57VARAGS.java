package com.company;
/*
*ZADANIE #57*
Utwórz metodę, która wykorzystuje mechanizm *varargs* by przekazać do metody dowolną, większą od zera, liczbę elementów typu `int` i zwrócić ich sumę.
 */

public class Zadanie57VARAGS {
    public static void main(String[] args) {
        System.out.println(zwrocSume(1,4,3,2));            //podobny mechanizm do printf, po przecinku podajemy liczby
    }

    static int zwrocSume(int... liczby) {             //chce do metody przekazać nieokreslona liczbe parametrow (liczby=nazwa parametru)
        //mamy tablice nieokreslonych elementow
        int suma = 0;

//        for (int i = 0; i < liczby.length; i++) {
//            suma += liczby[i];

        for (int liczba : liczby) {            //pętla foreach od poczatku do konca, liczba nalezy do liczby
            suma += liczba;
        }

        return suma;
    }
}
