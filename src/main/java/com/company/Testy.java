package com.company;

public class Testy {
    public static void main(String[] args) {
        String imie = "";

        System.out.println(takCzyNie("Daniel Mikolajczyk"));
        System.out.println(takCzyNie("Daniel Daniel Mikolajczyk"));
        System.out.println(takCzyNie("Daniel Daniel Daniel Mikolajczyk"));
        System.out.println(takCzyNie("Daniel"));
        System.out.println();
        System.out.println(czyEmail("danielMikolajczyk@gmail.com"));
        System.out.println();
        System.out.println(czyNumerTel("+48333555000"));
        System.out.println(czyNumerTel("0048333555000"));
        System.out.println(czyNumerTel("333555000"));
        System.out.println(czyNumerTel("333-555-000"));
        System.out.println(czyNumerTel("333-555000"));


    }

    static boolean takCzyNie(String imie) {
        return imie.toLowerCase()
                .matches("[a-z]{3,}( [a-z]{3,})? [a-z]{3,}");
    }

    static boolean czyEmail(String email) {
        return email
                .toLowerCase()
                .matches("[a-z1-9]+@+[a-z1-9]+\\.[a-z]{2,}");
    }

    static boolean czyNumerTel(String numer) {
        return numer.matches("((\\+|00)48)?[0-9]{9}|([0-9]{3}-){2}[0-9]{3}");
    }

}
