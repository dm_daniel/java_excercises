package com.company.zadanie160WzorzecFasadaBankomat;


public class CashMashine {

    private PinVerificator pinVerificator = new PinVerificator();
    private Account account = new Account();

    void withdrawCash(int cash, String pin) {
        if (pinVerificator.isPinOk(pin)) {
            if (account.getBalance() >= cash) {
                System.out.println("Balance before: " + account.getBalance());
                account.withdrawMoney(cash);
                System.out.println("Balance after: " + account.getBalance());
            } else {
                System.out.println("Not enough money on account");
            }

        } else {
            System.out.println("Wrong PIN number");
        }
    }

    void inputCash(int cash, String pin) {
        if (pinVerificator.isPinOk(pin)) {
            System.out.println("Balance before: " + account.getBalance());
            account.addMoney(cash);
            System.out.println("Balance after: " + account.getBalance());
        } else {
            System.out.println("Wrong PIN number");
        }

    }

}
