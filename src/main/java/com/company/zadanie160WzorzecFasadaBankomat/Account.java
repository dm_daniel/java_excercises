package com.company.zadanie160WzorzecFasadaBankomat;

public class Account {

    private double accountBalance = 0;

    double getBalance() {       //taki getter
        return accountBalance;
    }

    void addMoney(double money) {
        accountBalance += money;
    }

    void withdrawMoney(double money) {
        accountBalance -= money;
    }

}
