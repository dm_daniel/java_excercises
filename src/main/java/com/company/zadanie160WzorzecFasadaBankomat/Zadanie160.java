package com.company.zadanie160WzorzecFasadaBankomat;

import java.util.Scanner;


public class Zadanie160 {
    final private static int WITHDRAW = 1;
    final private static int DEPOSIT = 2;

    public static void main(String[] args) {
        CashMashine cashMashine = new CashMashine();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter Pin number");
        String pin = scanner.nextLine();

        System.out.print("Enter cash amount: ");
        int cash = scanner.nextInt();

        System.out.println("Options:");
        System.out.println("1: Withdraw");
        System.out.println("2: Deposit");

        int option = scanner.nextInt();

        switch (option) {

            case WITHDRAW:
                cashMashine.withdrawCash(cash, pin);
                break;
            case DEPOSIT:
                cashMashine.inputCash(cash, pin);
                break;

            default:
                System.out.println("Invalid PIN number");

        }
    }
}
