package com.company.zadanie160WzorzecFasadaBankomat;

public class PinVerificator {
    boolean isPinOk(String pinNum) {
        return pinNum.matches("^[0-9]{4}$");
    }
}
