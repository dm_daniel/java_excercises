package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;

/*
*ZADANIE #129*
Utwórz metodę która wczytuje liczbę (czyli typ `int`) od użytkownika a następnie ją wyświetla.
W przypadku podania innej wartości (np. litery), wyświetl użytkownikowi komunikat i pobierz informację ponownie.
Zaimplementuj to na dwa sposoby (pierwszy - obsługa wyjątku przez metodę pobierającą, drugi - metoda rzucająca wyjątek)
 */
public class Zadanie129 {
    public static void main(String[] args) {
        //oblsuga wyjątku wewnątrz metody
        System.out.println(wyswietlLiczbe());

        //metoda rzucająca wyjątek
        try {
            System.out.println(wyswietlLiczbe2());
        } catch (Exception e) {
            e.printStackTrace(); // wyświetla ścieżkę błedu
        }
    }

    static int wyswietlLiczbe() {

        Scanner s = new Scanner(System.in);
        int liczba;
        System.out.print("Podaj liczbe: ");
        try {
            liczba = s.nextInt();
        } catch (InputMismatchException e) {
            liczba = 0;
        }
        return liczba;
    }

    static int wyswietlLiczbe2() throws Exception {        //uzywajac tą metodę użytkownik musi być świadom, że taki wyjątek może wylecieć

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbe");
        return s.nextInt();
    }

}

