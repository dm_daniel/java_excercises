package com.company;
/*
Utwórz metodę, do której przekazujesz jeden parametr a następnie wyświetlasz tyle wielokrotności liczby `10`
> Dla `4` wyświetli `10, 20, 30, 40`
>
> Dla `7` wyświetli `10, 20, 30, 40, 50, 60, 70`
 */

public class Zadanie24 {
    public static void main(String[] args) {
        wielokrotnoscLiczby(4);

    }

    static void wielokrotnoscLiczby(int wielokrotnosc) {
        for (int i = 1; i <= wielokrotnosc; i++) {
            System.out.print(i * 10 + " ");
        }
    }

//    static void whileWielokrotnosc(int wielokrotnosc) {   DOKONCZYC W DOMU!
//        int i = 1;
//        do (i <= wielokrotnosc) {
//            System.out.println(i * 10 + " ");
//
//        }while (i<=stop)
//
//            }
//        }
//    }
}
