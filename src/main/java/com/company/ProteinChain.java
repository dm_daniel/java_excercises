package com.company;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ProteinChain {
    public static void main(String[] args) {
        System.out.println(czyMoznaZamienic("ABCDEF", "ACBDMF"));
        System.out.println(changePosible("ABCDEF","ABCDEF"));
    }

    static boolean czyMoznaZamienic(String chain1, String chain2) {

        if (chain1.length() != chain2.length()) {   //jesli łańcuchy są różnej długości
            return false;
        }

        char[] chainArr1 = chain1.toCharArray();
        char[] chainArr2 = chain2.toCharArray();

        List<Integer> listaPozycji = new LinkedList<>();

        for (int i = 0; i < chain1.length(); i++) {

            if (chainArr1[i] != chainArr2[i]) {     //porównoje znaki na konkretnych pozycjach
                listaPozycji.add(i);        //Dodajemy pozycje na której nie zgodziły się znaki w dwóch tablicach
                if (listaPozycji.size() > 2) {
                    return false;
                }
            }

        }
        if (listaPozycji.size() == 1) {
            return false;
        }
        //zamiana dwóch zmiennych miejscami
        char bufor = chainArr2[listaPozycji.get(0)];        //bufor = znak z tablicy 2 z pozycji 0 z listy
        chainArr2[listaPozycji.get(0)] = chainArr2[listaPozycji.get(1)];
        chainArr2[listaPozycji.get(1)] = bufor;

        return Arrays.equals(chainArr1, chainArr2);
    }

    static boolean changePosible(String chainA, String chainB) {
        //rozbijamy na tablicę znaków, bo talicę znaków możemy posortkować
        char[] a = chainA.toCharArray();
        char[] b = chainA.toCharArray();
        //sortowanie alfabetycznie
        Arrays.sort(a);
        Arrays.sort(b);

        return Arrays.equals(a, b);
    }

}
