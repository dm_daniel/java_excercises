package com.company.zadanie164WzorzecObserwator;

import java.util.Scanner;

public class Zadanie164 {
    public static void main(String[] args) {
        Weather weatherSystem = new Weather();  //tworzymy nową instancję klasy Weather

        Observer observer1 = new Observer("Ola", 20, -10);
        Observer observer2 = new Observer("Kasia", 10, 0);
        Observer observer3 = new Observer("Agnieszka", 30, -15);

        weatherSystem.addObserver(observer1, observer2, observer3);

        Scanner scanner = new Scanner(System.in);

        //pętla nieskończona
        for (; ; ) {
            System.out.print("ustaw temperature: ");
            int tempValue = scanner.nextInt();
            weatherSystem.updateTemp(tempValue);
        }
    }
}
