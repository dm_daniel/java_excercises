package com.company.zadanie164WzorzecObserwator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Weather {
    private List<Observer> list = new ArrayList<>();       //można było przypisać liste albo jako pole w klasie(ten przypadek), albo w konstruktorze
    private int currentTemp;

    //dwie metody o takiej samej nazwie ale innych parametrach (overload)
    void addObserver(Observer user) {
        list.add(user);
    }

    void addObserver(Observer... users) {       //dodaje hurtem userów, w mechanizmie varargs dostajemy tablicę
        list.addAll(Arrays.asList(users));  //add all wymaga kolekcji(nie można dodać tablicy do kolekcji) wiec arrays.aslist(zamieniam tablicy na listę i liste dodaje do listy obserwatorów)

    }

    //aktualizacja temperatury
    void updateTemp(int newTemp) {
        currentTemp = newTemp;
        notifyObservers();  //wywołuje metode
    }

    //powiadom obserwatorów
    private void notifyObservers() {

//        for (Observer observer : list) {      alternatywa
//            observer.react(currentTemp);
//        }

        //użycie streama, którego nie musze pisać bo zostało to domyślnie, -> wyrażenie lambda
        list.forEach(observer -> observer.react(currentTemp));      //dla kazdego elementu tej listy zrob to co jest za strzałką(wywołaj metodę i przekaz current temp)
    }
}
