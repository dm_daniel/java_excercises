package com.company.zadanie164WzorzecObserwator;

public class Observer {
    private String imie;
    private int maxTemp;
    private int minTemp;

    public Observer(String imie, int maxTemp, int minTemp) {
        this.imie = imie;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
    }

    //gettery
    public String getImie() {
        return imie;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public int getMinTemp() {
        return minTemp;
    }

    void react(int currentTemp) {
// %%s -  w kolejnym wywołaniu string formatu (każy string format zjada jeden %) widzi %s czyli maxTemp albo minTemp (%%s przetrwa pierwszy string format i wyjdzie w kolejnym(printf,jest w sumie string formatem)
        String message = String.format("%s twierdzi, że %s stopni to %%s.\n Jego/Jej %%s to %%s \n",
                imie,
                currentTemp);


        if (currentTemp > maxTemp) {
            System.out.printf(message, "za ciepło", " temp. maksymalna ", maxTemp);

        } else if (currentTemp < minTemp) {
            System.out.printf(message, "za zimno", " temp. minimalna ", minTemp);

        }
    }
}
