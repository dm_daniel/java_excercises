package com.company;
/*
*ZADANIE #93*
Utwórz metodę, która przyjmuje ciąg znaków i zwraca informacje jak długie są ciągi znaków które stoją obok siebie
> DLa `"aaabccccbb"` zwróci `[3, 1, 4, 2]`
> DLa `"telefon"` zwróci `[1, 1, 1, 1, 1, 1, 1]`
> DLa `"anna"` zwróci `[1, 2, 1]`
 */

import java.util.Arrays;

public class Zadanie93 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(zwrocDlugoscZnakowObokSiebie("aaabccccbb")));

    }

    static int[] zwrocDlugoscZnakowObokSiebie(String ciagZnakow) {

        String[] tablica = ciagZnakow.split("");
        String tenSamElement = tablica[0];
        int[] tablicaWynikow = new int[tablica.length];
        int licznik = 0;
        int indexTablicyWynikowej = 0;

        for (int i = 0; i < ciagZnakow.length(); i++) {
            if (tablica[i].equals(tenSamElement)) {
                licznik++;
            } else {
                tenSamElement = tablica[i];
                tablicaWynikow[indexTablicyWynikowej] = licznik;
                indexTablicyWynikowej++;
                licznik = 1;
            }
        }
        tablicaWynikow[indexTablicyWynikowej] = licznik;

        return przytnijTablice(tablicaWynikow);
    }

    static int[] przytnijTablice(int[] tablica) {
        int i = 0;      //i powie ile razy
        for (; i < tablica.length; i++) {
            if (tablica[i] == 0) {
                break;
            }
        }
        int[] krotszaTablica = new int[i];  //dlugosc skróconej tablicy
        for (int j = 0; j < krotszaTablica.length; j++) {
            krotszaTablica[j] = tablica[j];
        }
        return krotszaTablica;
    }
}
