package com.company;

public class Zadanie5 {
    public static void main(String[] args) {
        int value1 = 25;
        float value2 = 2.5f;    //domyslne jest double wiec trzeba dodac f(float
        double value3 = 1.3;
        System.out.println(value1 + value2);
        System.out.println(value3 * value2);
        System.out.println(value1 / value3);

        // kazda operacja matematyczna jest podnoszona do typu szerszego
        System.out.println(10 / 3);     //int
        System.out.println(10f / 3);    //float
        System.out.println(10 / 3.0);   //double
    }
}
