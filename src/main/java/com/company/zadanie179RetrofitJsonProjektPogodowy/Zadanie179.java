package com.company.zadanie179RetrofitJsonProjektPogodowy;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;
import java.util.Scanner;

public class Zadanie179 {

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.metaweather.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static WeatherApi api = retrofit.create(WeatherApi.class);


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Wpisz miasto: ");
        String nameOfCity = scanner.nextLine();

        Call<List<SearchCity>> call = api.searchCities(nameOfCity);

        call.enqueue(getSearchCallback()); //wywołanie w nowym wątku
    }

    private static Callback<List<SearchCity>> getSearchCallback() {
        return new Callback<List<SearchCity>>() {
            @Override
            public void onResponse(Call<List<SearchCity>> call, Response<List<SearchCity>> response) {
                if (response.isSuccessful()) {
                    parseSearchData(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<SearchCity>> call, Throwable throwable) {
                throwable.printStackTrace();
            }
        };
    }

    private static void parseSearchData(List<SearchCity> body) {
//sposob 1.  jedno elementowa tablica
//        final int[] counter = {0};  //myk na obejście wielowątkowości
//        body.forEach(city -> System.out.println(counter[0]++ + city.title));

        int counter = 1;
        for (SearchCity city : body) {
            System.out.println(counter + ". " + city.title);
            counter++;
        }

        //wybieram z listy 1 miasto
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wybrana pozycja z listy miast: ");
        int selectedPosition = scanner.nextInt();
        String cityId = body.get(selectedPosition - 1).id;

        getCity(cityId);
    }

    private static void getCity(String cityId) {
        Call<City> call = api.getCity(cityId);

        call.enqueue(getCityCallback());
    }

    private static Callback<City> getCityCallback() {
        return new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                if (response.isSuccessful() && response.body() != null) {
                    for (Day day : response.body().days) {
                        System.out.printf("%s - %s \n", day.date, day.temperature);
                    }
                }

            }

            @Override
            public void onFailure(Call<City> call, Throwable throwable) {

            }
        };
    }
}
