package com.company.zadanie179RetrofitJsonProjektPogodowy;

import com.google.gson.annotations.SerializedName;

public class SearchCity {

    // "title":"Warsaw",
    // "location_type":"City",
    // "woeid":523920,
    // "latt_long":"52.235352,21.009390"

    //jakie pole ma byc zamienione na jakie (dodaje adnotacje) - przemapowania nazw pól
    @SerializedName("title")
    String title;

    @SerializedName("location_type")
    String locationType;

    @SerializedName("woeid")
    String id;

    @SerializedName("latt_long")
    String latLong;

    @Override
    public String toString() {
        return "SearchCity{" +
                "title='" + title + '\'' +
                ", locationType='" + locationType + '\'' +
                ", id='" + id + '\'' +
                ", latLong='" + latLong + '\'' +
                '}';
    }
}
