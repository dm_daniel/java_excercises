package com.company.zadanie179RetrofitJsonProjektPogodowy;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface WeatherApi {

    //zapytanie get , widoczne zapytanie w URL przegladarki
    @GET("location/search")
//co ma nam dokleić do ścieżki głównej
    Call<List<SearchCity>> searchCities(@Query("query") String cityName);      //w moim zapytaniu ma mi dodać Query

    @GET("location/{city_id}")
    Call<City> getCity(@Path("city_id") String id);             //@Path - posklejane ze sobą wartości po sleszach
}
