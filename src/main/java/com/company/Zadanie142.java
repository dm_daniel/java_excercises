package com.company;

/*
*ZADANIE #142*
Policz sumę cyfr przekazanej liczby przy wykorzystaniu *rekurencji*.
 */
public class Zadanie142 {
    public static void main(String[] args) {
        System.out.println(sumaCyfr(321));
    }

    private static int sumaCyfr(int number) {
        System.out.println("number = [" + number + "]");
        if (number == 0) {
            return 0;
        } else {
            return (number % 10) + sumaCyfr(number / 10);
        }
    }
}
