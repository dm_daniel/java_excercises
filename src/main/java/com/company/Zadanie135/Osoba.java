package com.company.Zadanie135;

class Osoba {
    private String name;
    private String surname;
    private int age;
    private boolean isWoman;

     Osoba(String name, String surname, int age, boolean isWoman) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isWoman = isWoman;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s (%s years old)",
                isWoman ? " Woman " : " Man ",
                name,
                surname,
                age);
    }
}
