package com.company.Zadanie135;
/*
*ZADANIE #135*
Przygotuj plik .csv  albo .txt(comma separated value) (gdzie wartości oddzielone są przecinkami - `,`),
który będzie zawierał reprezentację obiektów klasy `GenericClass`.
Metoda ma odczytać plik i zwrócić listę obiektów klasy `GenericClass`.

Jan,Nowak,12,false
Anna,Kowalska,25,true
Kuma,Janowski,66,false
Karolina,Nowa,88,true
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Zadanie135 {
    private static final String SEPARATOR = ",";

    public static void main(String[] args) {

        try {
            List<Osoba> cokolwiek = zwrocListeObiektowOsoba("pliki/zadanie135.txt");
            for (Osoba osoba : cokolwiek) {
                System.out.println(osoba);
            }
        } catch (IOException e) {
            System.out.println("błąd odczytu pliku");
            e.printStackTrace();
        }
    }

    private static List<Osoba> zwrocListeObiektowOsoba(String sciezkaDoPliku) throws IOException {

        FileReader fileReader = new FileReader(sciezkaDoPliku);
        BufferedReader buffer = new BufferedReader(fileReader); //w konstruktorze przekazujemy plik z którym bedziemy "gadać"
        String line = buffer.readLine();

        //tworzymy liste, w pętli ją wypełniamy i za nią zwracamy
        List<Osoba> listaLudzi = new LinkedList<>();

        while (line != null) {      //przerywamy w momencie gdy linia jest nullem
            if (!line.isEmpty()) {  //jeżeli lista jest nie pusta to ją przetwórz
                Osoba osoba = getPersonFromLine(line);  //wywołana kolejna metoda, wygenerowana pod spodem
                listaLudzi.add(osoba);
            }
            line = buffer.readLine();
        }
        return listaLudzi;
    }

    private static Osoba getPersonFromLine(String line) {
        String[] lineArr = line.split(SEPARATOR);

        return new Osoba(       //do konstruktora klasy GenericClass przekazujemy te parametry:
                lineArr[0],
                lineArr[1],
                Integer.valueOf(lineArr[2]),        //ze stringa zamieniamy na Integer
                Boolean.valueOf(lineArr[3]));         //ze stringa zamieniamy na wartość logiczną

    }
}


