package com.company;
/*
*ZADANIE #65 - EXTRA*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma “przesunąć” elementy w środku tablicy o tyle ile wynosi wartość drugiego elementu i zwrócić nową tablicę.
Dla wartości dodatniej przesuwa w prawo, a dla ujemnej w lewo. Elementy z końca tablicy lądują na początku (i na odwrót)

> dla `([1,2,3,4,5,6],  2)`
> należy zwrócić `[5,6,1,2,3,4]` (przesunięcie o dwa w prawo)
>
> dla `([1,2,3,4,5,6],  -3)`
> należy zwrócić `[4,5,6,1,2,3]` (przesunięcie o trzy w lewo)
> dla `([1,2,3,4,5,6],  -2)`
> należy zwrócić `[3,4,5,6,0,0]` (przesunięcie o trzy w lewo)

 */

import java.util.Arrays;

public class Zadanie65_extra {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, 5, 6};
        System.out.println(Arrays.toString(zwrocTablice(tab, 4)));
    }

    static int[] zwrocTablice(int[] tablica, int liczba) {
        int element1 = liczba;
        int element2 = liczba;
        int[] tablicaKoncowa = new int[tablica.length];

        if (liczba == 0) {
            tablicaKoncowa = tablica;
        }

        if (liczba > 0) {

            for (int i = tablica.length - liczba; i < tablica.length; i++) {
                tablicaKoncowa[liczba - element1] = tablica[i];
                element1--;
            }
            for (int i = 0; i < tablica.length - element2; i++) {
                tablicaKoncowa[liczba] = tablica[i];
                liczba++;
            }
        }

        if (liczba < 0) {
            liczba = Math.abs(liczba);
            element1 = Math.abs(element1);
            element2 = Math.abs(element2);

            for (int i = tablica[liczba]; i <= tablica.length; i++) {
                tablicaKoncowa[liczba - element1] = i;
                liczba++;
            }

            liczba = element1;

            for (int i = 0; i < element1; i++) {
                tablicaKoncowa[tablica.length - liczba] = tablica[i];
                if (liczba > 1) {
                    liczba--;
                }
            }

        }
        return tablicaKoncowa;
    }
}