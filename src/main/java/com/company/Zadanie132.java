package com.company;
/*
*ZADANIE #132*
Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku,
a następnie wyświetla zawartość tego pliku na konsolę.
Zrealizuj zadanie odczytując dane “znak po znaku”.
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;       //ten wyjątek dodalismy najpierw, mówił ze nie co gdy nie znajdzie pliku do wczytania
import java.io.IOException; //sprawdza, czy mamy dostęp do pliku + obsługuje wyjątek FileNotFound

public class Zadanie132 {
    public static void main(String[] args) {

        try {
            oczytajPlikZnakPoZnaku("pliki/zadanie132.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void oczytajPlikZnakPoZnaku(String sciezkaDoPliku) throws IOException {

        FileInputStream plik = new FileInputStream(sciezkaDoPliku);        //tworzymy nowy obiekt klasy FileInput... do konstruktora przekasujemy parametr
        int znak = plik.read(); //plik.read odczytuje 1 znak

        while (znak != -1) {  //kręc się tak dlugo aż znak różny od -1 (żaden znak nie posiada takiej wartości liczbowej) -1 = EndOfFile
            System.out.print((char)znak);     //wyswietlam znak(liczby) ale traktuje je jako wartości char
            znak = plik.read();
        }
    }
}
