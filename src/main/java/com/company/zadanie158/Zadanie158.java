package com.company.zadanie158;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Zadanie158 {

    static List<Osoba> list = Arrays.asList(
            new Osoba("Janusz", 55),
            new Osoba("Grażyna", 53),
            new Osoba("Julia", 20),
            new Osoba("Lukasz", 33),
            new Osoba("Edyta", 33),
            new Osoba("Karol", 55)
    );

    public static void main(String[] args) {
//        metoda1();
//        metoda2();
//        metoda3();
//        metoda4();
//        metoda5();
//        metoda6();
//        metoda7();
//        metoda8();
//        metoda9();
//        metoda10();
//        metoda11();
//        metoda12();
//        metoda13();
//        metoda14();
        metoda15();
//        metoda16();
//        metoda17();
//        metoda18();
//        metoda19();
//        metoda20();
//        metoda21();
    }

    private static void metoda21() {
        String zlepioneImiona = list.stream()
                .map(osoba -> osoba.getImie() + osoba.getWiek())
                .collect(Collectors.joining(","));

        System.out.println(zlepioneImiona);
    }

    private static void metoda20() {
        Map<Integer, List<Osoba>> map = list.stream()
                .collect(Collectors.groupingBy(osoba -> osoba.getWiek()));

        System.out.println(map);
    }

    private static void metoda19() {
        List<Integer> listaWiek = list.stream()
                .map(osoba -> osoba.getWiek())
                .sorted()
                .collect(Collectors.toList());

        System.out.println(listaWiek);
    }

    private static void metoda18() {
        List<String> lista = Arrays.asList("PL", "GER", "Eng");
        boolean wynik = lista
                .stream()
                .anyMatch(slowo -> slowo.length() % 2 == 0);    //gdy którakolwiek pasuje

        System.out.println(wynik);

        List<Integer> listaLiczb = Arrays.asList(4, 8, 16, 1, 20);
        boolean czyParzyste = listaLiczb.stream()
                .allMatch(liczba -> liczba % 2 == 0);

        System.out.print(czyParzyste);
    }

    private static void metoda17() {
        Stream.of("A1", "A2", "A20", "C4", "J8")            //Stream zbiór wyrazów
                .filter(slowo -> {      //filter musi cos zwracać
                    System.out.println("teraz filtruje " + slowo);
                    return true;
                })
                .map(slowo -> {     //nie modyfikuje tylko wyświetlam...
                    System.out.println("teraz Mapuje " + slowo);
                    return slowo;
                })
                .forEach(slowo -> System.out.println("ForEach " + slowo));
    }

    private static void metoda16() {
//                .mapToInt(slowo->Integer.parseInt(slowo))
        Stream.of("A1", "A2", "A20", "C4", "J8")
                .map(slowo -> slowo.substring(1))
                .mapToInt(Integer::parseInt)
                .max()
                .ifPresent(System.out::println);
    }

    private static void metoda15() {
        List<String> listaNapisow = Arrays.asList("A1", "A2", "B1", "B2", "G5", "J8", "A1A", "AA");
        List<Integer> listaRzednych = listaNapisow
                .stream()
                .filter(slowo -> slowo.length() == 2)    //bierzemy wyrazy tylko ==2
                .map(slowo -> slowo.substring(1))   //wyswietlamy od 1 pozycji każdego wyrazu
                .map(slowo -> {         // dodaje {} jeśli mam wiecej niż 1 polecenie
                    try {
                        return Integer.parseInt(slowo);
                    } catch (Exception e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());


        System.out.println(listaRzednych);
    }

    private static void metoda14() {
        List<String> listaNapisow = Arrays.asList("Kot", "Pies", "", "Ptak", "", "Ryba");
        List<Integer> dlugosciSlow = listaNapisow
                .stream()
                .filter(slowo -> !slowo.isEmpty())
                .map(String::length)
                .collect(Collectors.toList());

        System.out.println(dlugosciSlow);


    }

    private static void metoda13() {
        List<String> lista = Arrays.asList("Kot", "Pies", "", "Ptak", "", "Ryba");
        List<String> kolekcjaSlow = lista
                .stream()
                .filter(slowo -> !slowo.isEmpty())      //filtrujemy slowa niepuste
                .filter(slowo -> slowo.startsWith("R") || slowo.startsWith("K"))        //czy slowo zaczyna się na K lub R
//                .map(slowo -> slowo.toUpperCase())
                .map(String::toUpperCase)       //z klasy String chce wziac metode uppercase
                .collect(Collectors.toList());

        System.out.println(kolekcjaSlow);
    }

    private static void metoda12() {
        List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        IntSummaryStatistics statistics = lista.stream().mapToInt(liczba -> liczba).summaryStatistics();
        //info o zbiorze
        System.out.println("max to: " + statistics.getMax());
        System.out.println("średnia to: " + statistics.getAverage());
        System.out.println("ilośc elementów: " + statistics.getCount());
        System.out.println("min to: " + statistics.getMin());
        System.out.println("suma elementów : " + statistics.getSum());
    }

    private static void metoda11() {
        List<Integer> lista = IntStream
                .rangeClosed(1, 100)
                .filter(liczba -> Math.sqrt(liczba) % 1 == 0)
                .boxed()
                .collect(Collectors.toList());

        System.out.println(lista);
    }

    private static void metoda10() {
        IntStream
                .iterate(1, liczba -> liczba + 2)       //zaczynamy od 1 i biore co 2 wartość
                .limit(5)   //chce 5 liczb
                .forEach(liczba -> System.out.println(liczba)); //wyświetlam wartość
    }

    private static void metoda9() {
        IntStream.rangeClosed(10, 20)
                .filter(liczba -> liczba % 2 == 0)
                .average()
                .ifPresent(liczba -> System.out.print(liczba));       // jeśli uda się policzyć średnią...
    }

    private static void metoda8() {
        int[] tablica = {3, 5, 7};
        double srednia = Arrays.stream(tablica)
                .filter(liczba -> {
                    if (liczba % 3 == 0) {
                        return true;
                    }
                    return false;
                })
                .average()
                .orElse(0);      //jaka wartość ma zostać zwrócona jesli nie uda sie policzyc sredniej

        System.out.println(srednia);
    }

    private static void metoda7() {
        IntStream
                .rangeClosed(-20, 20)
                .filter(liczba -> liczba % 3 == 0)
                .forEach(liczba -> System.out.print(liczba + ","));
    }

    private static void metoda6() {
        int[] tablica = {5, 2, 8, -5};
        Arrays.stream(tablica)
                .map(liczba -> liczba * 2)      //kazdy z elementów został przemapowany(zmodyfikowany) - nie pracujemy już na tablicy
                .forEach(liczba -> System.out.print(liczba + ","));
    }

    private static void metoda5() {
        int[] tablica = {3, 5, 7};
        Arrays.stream(tablica)
                .forEach(liczba -> System.out.print(liczba + ", "));
    }

    private static void metoda4() {
        List<Integer> lista = new Random().ints(-20, 20)     //random losuje int a nie Integery, wiec używamy boxed() żeby włożyć do listy która przyjmuje typ klasowy
                .limit(10)
                .boxed()
                .collect(Collectors.toList());      //zbierz wylosowane elementy

        System.out.println(lista);

    }

    private static void metoda3() {
        Random random = new Random();
        random.ints(-10, 20)           //losujemy 5 sztuk
                .limit(5)
                .forEach(System.out::println);

    }

    private static void metoda2() {
        IntStream
                .rangeClosed(1, 10)         //range closed 2 parametr jest uwzgledniony ( od 1 do 10 )
                .forEach(System.out::print);      //odwołanie przez referencję
    }

    private static void metoda1() {
        IntStream
                .range(1, 10)       //od 1 do 9
                .forEach(liczba -> {                //parametr przekazujemy do metody sout
                    System.out.print(liczba);
                });
    }
}
