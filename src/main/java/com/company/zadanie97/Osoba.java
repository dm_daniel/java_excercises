package com.company.zadanie97;

public class Osoba {
    private String imie;
    private String nazwisko;
    private int wiek;
    private Plec plec;

    public Osoba(String imie, String nazwisko, int wiek, Plec plec) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.plec = plec;
    }

    @Override
    public String toString() {
        return "GenericClass{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", wiek=" + wiek +
                ", plec=" + plec +
                '}';
    }
}
