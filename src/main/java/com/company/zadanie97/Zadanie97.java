package com.company.zadanie97;
/*
*ZADANIE #97*
Utwórz typ wyliczeniowy `Plec` (zawierająca dwie opcje).
Utwórz klasę `GenericClass`, gdzie jednym z pól będzie ten typ.
Utwórz tablicę obiektów klasy `GenericClass` oraz metodę która zwróci nam liczbę pracowników wybranej (przez parametr) płci.
 */

public class Zadanie97 {
    public static void main(String[] args) {

        Osoba[] osoby = new Osoba[]{
                new Osoba("Jan", "Nowak", 30, Plec.MEZCZYZNA),
                new Osoba("Mariusz", "Kowalski", 55, Plec.MEZCZYZNA),
                new Osoba("Jolanta", "Kot", 44, Plec.KOBIETA)};

        for (Osoba osoba : osoby) {
            System.out.println(osoba);
        }

    }
}
