package com.company;

/*

*ZADANIE #15*
Utwórz metodę, do której przekazujesz dwa parametry a ona zwróci informację
 (w formie wartości logicznej) czy *obydwie* są parzyste.
Oraz metodę, która zwróci informację czy *chociaż jeden z nich* jest większy od zera
 */
public class Zadanie15 {
    public static void main(String[] args) {

        System.out.println(czyObieParzyste(1, 1));
        System.out.println(czyObieParzyste(2, 2));
        System.out.println(czyObieParzyste(1, 2));
        System.out.println(czyObieParzyste(2, 1));
        System.out.println();
        System.out.println(czyChocJedenWiekszy(1,1));
        System.out.println(czyChocJedenWiekszy(1,-1));
        System.out.println(czyChocJedenWiekszy(-1,1));
        System.out.println(czyChocJedenWiekszy(-1,-1));

    }

    static boolean czyObieParzyste(int pierwsza, int druga) {
        return pierwsza % 2 == 0 && druga % 2 == 0;

    }

    static boolean czyChocJedenWiekszy(int pierwsza, int druga) {  // || lub (alternatywa)
        return pierwsza > 0 || druga > 0;
    }
}

