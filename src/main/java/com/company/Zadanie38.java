package com.company;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca *NAJWIĘKSZĄ* liczbę
 */

public class Zadanie38 {
    public static void main(String[] args) {
        double[] tab = new double[]{-11, 22.2, 7.5};
        System.out.println(zwrocMax(tab));
        System.out.println(forZwrocMax(tab));

    }

    static double zwrocMax(double[] tab) {
        double max = tab[0];        //int max(wzielismy element na pozycji 0)...przeszukujac zbior (min czy max czy cokolwiek) zaczynajmy od elementu ktory sie znajduje w tablicy
        for (double elementTablicy : tab) { //elementTablicy to pojedynczy element z tab
            if (elementTablicy > max) {
                max = elementTablicy;
            }
        }
        return max;
    }

    static double forZwrocMax(double[] tab) {
        double max = tab[0];
        for (int i = 1; i < tab.length; i++) {  //w tym przypadku nie musimy sprawdzać od 0, bo element maksymalny jest tab[0]
            if (tab[i] > max) {
                max = tab[i];
            }
        }
        return max;
    }
}
