package com.company.zadanie178BibliotekaLombok;

//dzieki lombokowi wygenerowaliśmy gettery w ten sposób

import lombok.*;

//dodaje rzeczy z lomboka
@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@Setter
class Car {
    private String name;
    @Setter(AccessLevel.NONE)
    private String color;
    private int numberOfDoors;
    @Getter(AccessLevel.NONE)       //zablokowałem getter dla tego pola
    private int yearOfProduction;
}
