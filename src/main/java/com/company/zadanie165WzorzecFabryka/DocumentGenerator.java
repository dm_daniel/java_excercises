package com.company.zadanie165WzorzecFabryka;

class DocumentGenerator {

    Document createDocument(String textToSave, DocumentType type) {      //metoda zwracająca Dokument
        Document document = null;

        switch (type) {
            case TXT:
                document = new TxtDocument(type, textToSave);
                break;

            case HTML:
                document = new HtmlDocument(type, textToSave);
                break;


        }
        return document;

    }

    enum DocumentType {
        TXT("txt"),
        HTML("html");


        private String extension;

        DocumentType(String extension) {
            this.extension = extension;
        }

        public String getExtension() {
            return extension;
        }

    }

}
