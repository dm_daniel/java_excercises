package com.company.zadanie165WzorzecFabryka;

class Zadanie165 {
    public static void main(String[] args) {

        DocumentGenerator generator = new DocumentGenerator();

        String text1 = "Jest niedziela\nale nie pada śnieg.";
        String text2 = "linia1\nlinia2\nlinia3";

        Document txt = generator.createDocument(text1, DocumentGenerator.DocumentType.TXT);
        txt.saveFile("zadanie_165");

        txt = generator.createDocument(text2, DocumentGenerator.DocumentType.TXT);
        txt.saveFile("zadanie_165text2");

        Document html = generator.createDocument(text1, DocumentGenerator.DocumentType.HTML);
        html.saveFile("zadanie_165");


    }
}
