package com.company.zadanie165WzorzecFabryka;

import com.company.zadanie165WzorzecFabryka.DocumentGenerator.DocumentType;

public class HtmlDocument extends Document {

    public HtmlDocument(DocumentType type, String content) {
        super(type, formatText(content));       //rozbudowanie
    }


    private static String formatText(String text) {
        String extraH1 = String.format("<h1>%s</h1>", text);
        String extraBR = extraH1.replaceAll("\n", "</br>");     //bez tego w przeglądarce nie złamie lini
        extraBR = "<meta charset=\"utf-8\"/>" + extraBR;        //bez tego nie wyświetli polskich znaków w przeglądarce
        return extraBR;
    }

}
