package com.company.zadanie165WzorzecFabryka;

import com.company.zadanie165WzorzecFabryka.DocumentGenerator.DocumentType;

class TxtDocument extends Document {

    public TxtDocument(DocumentType type, String content) {
        super(type, content);
    }
}
