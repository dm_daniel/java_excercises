package com.company.zadanie165WzorzecFabryka;

import com.company.zadanie165WzorzecFabryka.DocumentGenerator.DocumentType;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

class Document {
    private DocumentType type;
    private String content;

    Document(DocumentType type, String content) {
        this.type = type;
        this.content = content;
    }

    void saveFile(String fileName) {
        String outputPath = String.format(      //montujemy scieżkę do pliku
                "pliki/%s.%s",
                fileName,
                type.getExtension()
        );

        File file = new File(outputPath);       //tworze zmienna typu File

        try (FileWriter writer = new FileWriter(file)) {        //FileWriter implementuje interfejs closable(metoda close wywoła sie automatycznie)
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getContent() {
        return content;
    }
}
