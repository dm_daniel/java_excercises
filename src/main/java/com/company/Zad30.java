package com.company;
/*
Utwórz metodę, która przyjmuje *jeden* parametr (który jest liczbą wierszy i kolumn)
 oraz wyświetla tabliczkę mnożenia (znaki możesz oddzielać znakiem tabulacji `\t`).
> Dla `3` wyświetli:
>
1   2   3
2   4   6
3   6   9


> Dla `6` wyświetli:
>
1   2   3   4   5   6
2   4   6   8  10  12
3   6   9  12  15  18
4   8  12  16  20  24
5  10  15  20  25  30
6  12  18  24  30  36
 */

public class Zad30 {
    public static void main(String[] args) {
        forTabMnozenia(5);
        System.out.println();
        whileTabMnozenia(5);

    }

    static void forTabMnozenia(int wymiar) {
        for (int i = 1; i <= wymiar; i++) {
            for (int j = 1; j <= wymiar; j++) {
                System.out.print(i * j + "\t");
            }
            System.out.println();
        }
    }

    static void whileTabMnozenia(int wymiar) {
        int i = 1;
        int j = 1;
        while (i <= wymiar) {
            while (j <= wymiar) {
                System.out.print(i * j + "\t");
                j++;
            }
            j = 1;
            i++;
            System.out.println();
        }

    }
}
