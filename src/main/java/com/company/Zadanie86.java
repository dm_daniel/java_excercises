package com.company;
/*
*ZADANIE #86*
Utwórz metodę, która przyjmuje dwa parametry - pierwszy datą (jako ciąg znaków),
a drugi jest separatorem znaków. Metoda ma zwrócić rok z przekazanej daty.
> Dla `("12/05/2018", "/")`, zwróci `"2018"`
> Dla `("22.11.2007", ".")`, zwróci `"2007"`
 */

public class Zadanie86 {
    public static void main(String[] args) {
        System.out.println(zwrocRok("12/05/2009", "/"));
        System.out.println(zwrocRok("12/05/2018"));

        System.out.println(zwrocRok("12.05.2009", "\\."));      // \\traktuje kropke jako zwykly znak(bo domyslnie jest jako znak specjalny)
    }

    static String zwrocRok(String data, String separator) {
        String[] tablica = data.split(separator);
        return tablica[tablica.length - 1];
    }

    static String zwrocRok(String data) {           // metoda zama dokleja separator, korzysta z pierwszej metody
        return zwrocRok(data, "/");
    }
}
