package com.company;
/*
*ZADANIE #126*
Utwórz metodę która przyjmuje tablicę, a następnie przy wykorzystaniu `Scannera` użytkownik może podać indeks parametru.
W przypadku indeksu poza zakresem (tzn. gdy wystąpi wyjątek) zapytaj użytkownika o indeks ponownie.
 */

import java.util.Scanner;

public class Zadanie126 {
    public static void main(String[] args) {
        int[] array = new int[]{12, 5, 60, 100};

        Scanner s = new Scanner(System.in);
        System.out.print("Podaj indeks: ");
        int index = s.nextInt();
        try {
            System.out.println(array[index]);
        } catch (Exception e) {   //czego się spodziewam
            System.out.println("Podano zły indeks");
        }
    }
}
