package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie99 {
    /*
    *ZADANIE #99*
Utwórz metodę, która przyjmuje listę liczb, a następnie największą z nich
     */
    public static void main(String[] args) {

        List<Integer> mojaLista = new ArrayList<>(Arrays.asList(1, -5, 50, 15, 270,-5, 7, 22));         //Arrays.asList zamień tablicę na listę z elementów które podam w nawiasie

        System.out.println(zwrocNajwieksza(mojaLista));
        System.out.println(zwrocNajwieksza2(mojaLista));
    }

    static int zwrocNajwieksza(List<Integer> lista) {
        int najwieksza = lista.get(0);

        for (int element = 0; element < lista.size(); element++) {
            if (lista.get(element) > najwieksza) {
                najwieksza = lista.get(element);
            }
        }
        return najwieksza;
    }

    static int zwrocNajwieksza2(List<Integer> lista) {
        int najwieksza = lista.get(0);

        for (Integer i : lista) {
            if (i > najwieksza) {
                najwieksza = i;
            }
        }
        return najwieksza;
    }

}
