package com.company;
/*
*ZADANIE #108*
Porównaj czasy łączenia (np. 10 000) napisów przy wykorzystaniu
- `+=`  (zwykłej konkatenacji)
- `StringBuilder`-a
- `StringBuffer`-a (edited)
 */

public class Zadanie108 {
    private final static int LICZBA_WYKONAN = 80_000;       //mozna stosowac podkreslenia dowolnie dla czytelnosci (nie moze stac przy kropce)
    private final static String NAPIS_TESTOWY = "pies";

    public static void main(String[] args) {


        long czasStart = System.currentTimeMillis();       //czas od 1 stycznia 1977roku ( ile ms uplynelo od tego czasu)
        laczenie();
        long czasStop = System.currentTimeMillis();
        System.out.println("Czas konkatenacji w pętli (=+): " + (czasStop - czasStart));

        czasStart = System.currentTimeMillis();
        laczenieStringBuilderem();
        czasStop = System.currentTimeMillis();
        System.out.println("Czas stringBuilderem (=+): " + (czasStop - czasStart));

        czasStart = System.currentTimeMillis();
        laczenieStringBuffer();
        czasStop = System.currentTimeMillis();
        System.out.println("Czas stringBufferem (=+): " + (czasStop - czasStart));


    }

    static String laczenie() {
        String napis = "";

        for (int i = 0; i < LICZBA_WYKONAN; i++) {
            napis += NAPIS_TESTOWY;
        }
        return napis;
    }

    static String laczenieStringBuilderem() {
        StringBuilder napis = new StringBuilder();  //String builder tworzy tablice

        for (int i = 0; i < LICZBA_WYKONAN; i++) {
            napis.append(NAPIS_TESTOWY);            //append to jest += (nie łączy napisów co obrót tylko je magazynuje) ( operacje łączenia robi jednorazowo appendem z tąd różnica czasu)
        }
        return napis.toString();     //konwertuje tablice na napis
    }

    static String laczenieStringBuffer() {
        StringBuffer napis = new StringBuffer();

        for (int i = 0; i < LICZBA_WYKONAN; i++) {
            napis.append(NAPIS_TESTOWY);
        }
        return napis.toString();
    }

}
