package com.company;
/*
*ZADANIE #138*
Utwórz metodę która przyjmuje dwa parametry, które są ścieżkami do plików. Metoda ma scalić pliki “zazębiając” linijki
 */

import java.io.*;

public class Zadanie138 {
    public static void main(String[] args) {
        try {
            scalPliki("pliki/Zadanie138.Test1.txt", "pliki/Zadanie138.Test2.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void scalPliki(String sciezka1, String sciezka2) throws IOException {
        BufferedReader odczytPierwszegoPliku = new BufferedReader(new FileReader(sciezka1));
        BufferedReader odczytDrugiegoPliku = new BufferedReader(new FileReader(sciezka2));
        BufferedWriter zapisDoNowegoPliku = new BufferedWriter(new FileWriter("pliki/Zadanie138.txt"));

        while (true) {
            String liniaZPierwszegoPliku = odczytPierwszegoPliku.readLine();
            String liniaZDrugiegoPliku = odczytDrugiegoPliku.readLine();
            if ((liniaZPierwszegoPliku == null) && (liniaZDrugiegoPliku == null)) {
                break;
            }
            if (liniaZPierwszegoPliku != null) {
                zapisDoNowegoPliku.write(liniaZPierwszegoPliku);
                zapisDoNowegoPliku.newLine();
            }
            if (liniaZDrugiegoPliku != null) {
                zapisDoNowegoPliku.write(liniaZDrugiegoPliku);
                zapisDoNowegoPliku.newLine();
            }
        }
        zapisDoNowegoPliku.close();
    }
}
