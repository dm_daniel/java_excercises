package com.company.zadanie176WątkiExecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Zadanie176 {
    public static void main(String[] args) {
        metho1();
    }

    private static void metho1() {
        //iloma wątkami ma iść zadanie
        ExecutorService es1 = Executors.newFixedThreadPool(4);
        System.out.println("Wątek: " + Thread.currentThread().getId());

        for (int i = 0; i < 10; i++) {
            Runnable r = getRunnable();
            //dodaj element, obiekt klasy runnable
            es1.submit(r);
        }
        //zamyka przyjmowanie zgłoszeń i zaczyna wykonywać zadania
        es1.shutdown();
    }

    private static Runnable getRunnable() {
        return new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("zadanie podjął wątek nr: " + Thread.currentThread().getId());
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
    }
}
