package com.company;
/*
*ZADANIE #59 - EXTRA*
Utwórz metodę, która przyjmuje tablicę liczb i zwraca nową tablicę w której wszystkie ujemne liczby będą w lewej części,
a wszystkie dodanie będą w prawej części. Liczby ujemne i dodatnie mogą być w dowolnej kolejności (tzn. `1,2,3` czy `3,2,1` są jak najbardziej poprawne)
> Dla `[1, 2, -6, -9, 11, 0, -2]`
> zwróci `[-6, -9, -2, 0, 11, 2, 1]`
 */

import java.util.Arrays;

public class Zadanie59_extra {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(posortujTablice(5, -1,0, -2, 5, -3, 10, -4, -5, 0)));
    }

    static int[] posortujTablice(int... liczby) {
        int[] tablicaKoncowa = new int[liczby.length];
        int znacznikKonca = liczby.length - 1;
        int znacznikPoczatku = 0;
        for (int i = 0; i < liczby.length; i++) {

            if (liczby[i] >= 0) {
                tablicaKoncowa[znacznikKonca] = liczby[i];
                znacznikKonca--;
            }
        }

        for (int i = 0; i < liczby.length; i++) {

            if (liczby[i] < 0) {
                tablicaKoncowa[znacznikPoczatku] = liczby[i];
                znacznikPoczatku++;
            }
        }

        return tablicaKoncowa;
    }
}
