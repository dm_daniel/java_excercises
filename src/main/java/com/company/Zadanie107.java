package com.company;
/*
*ZADANIE #107*
Utwórz metodę, która przyjmuje dwa parametry - set (np. `HashSet`) oraz poszukiwaną liczbę (jako `double`.)

Metoda ma zwrócić set elementów większych od podanego (drugiego) parametru.
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Zadanie107 {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));
        System.out.println((zwrocWieksza(set1, 2.0)));

    }


    static Set<Integer> zwrocWieksza(Set<Integer> mojSet, double liczba) {
        Set<Integer> setKoncowy = new HashSet<>();

        for (Integer element : mojSet) {
            if (element > liczba) {
                setKoncowy.add(element);
            }
        }
        return setKoncowy;
    }
}
