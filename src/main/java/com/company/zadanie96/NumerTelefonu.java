package com.company.zadanie96;

class NumerTelefonu {
    private String numer;
    private Kierunkowy kierunkowy;

    NumerTelefonu(String numer, Kierunkowy kierunkowy) {
        this.numer = numer;
        this.kierunkowy = kierunkowy;
    }

    void wyswietlNr() {
        System.out.println("(" + kierunkowy.jakoTekst()
                + ")" + numer);
        System.out.print("(" + kierunkowy.jakoNumer()
                + ")" + numer);
    }
}
