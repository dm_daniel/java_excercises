package com.company.zadanie96;

enum Kierunkowy {
    //trzy możliwości z 1 wartością
    POLSKA(48), NIEMCY(49), ROSJA(7);
    private int kierunkowy;

    Kierunkowy(int kierunkowy) {
        this.kierunkowy = kierunkowy;
    }


    String jakoTekst() {
        return "+" + kierunkowy;
    }

    int jakoNumer() {
        return kierunkowy;
    }

}
