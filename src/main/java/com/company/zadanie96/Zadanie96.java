package com.company.zadanie96;
/*
*ZADANIE #96*
Utwórz klasę `NumerTelefonu` z polami `numer` oraz `kierunkowy`.
Numer kierunkowy jest typem wyliczeniowym (np. z opcjami `POLSKA`, `NIEMCY`, `ROSJA`)
posiadającym dwie metody (`jakoTekst()` zwracająca np. `+48` oraz `jakoNumer()` zwracająca np. `48`).
 */

public class Zadanie96 {
    public static void main(String[] args) {
        NumerTelefonu nr = new NumerTelefonu("3551029", Kierunkowy.POLSKA);
        nr.wyswietlNr();
    }
}
