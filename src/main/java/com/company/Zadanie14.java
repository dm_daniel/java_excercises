package com.company;

/*Utwórz metodę, która zwróci informację (w formie wartości logicznej) czy przekazana liczba jest parzysta czy nie.
        >Dla `12` zwroci `true`
        >
        >Dla `9` zwróci `false`
        >
        >Dla `1` zwróci `false`
        >
        >Dla `40` zwróci `true`
        >
        >Dla `123` zwróci `false`
        */
public class Zadanie14 {
    public static void main(String[] args) {
        System.out.println(czyParzysta(10));
        System.out.println(czyParzyste2(52));

    }

    static boolean czyParzysta(int liczba) {  //7%2=1 modulo(reszta z dzielenia)
        if (liczba % 2 == 0) {
            return true;
        }               //nie musimy uzywać else
        return false;
    }

    static boolean czyParzyste2(int liczba) {   //krótsza metoda
        return (liczba % 2 == 0);
    }
}
