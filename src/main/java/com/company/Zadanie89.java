package com.company;
/*
Utwórz metodę, która przyjmuje ciąg w formie `"<nazwa operacji> <operacja>"`.
Na bazie nazwy operacji należy rozpoznać separator operacji.
> DLa `"add 5+4+99"` zwróci `108`
> DLa `"subtract 100-20"` zwróci `80`
> DLa `"multiply 8*17"` zwróci `136`
> DLa `"divide 200/25"` zwróci `8`
 */

public class Zadanie89 {
    // nie będzie możliwości zmiany wartości zmiennej finalnej
    private static final String OPERATION_ADDING = "add";
    private static final String OPERATION_ADDING_SYMBOL = "\\+";
    private static final String OPERATION_SUBSTRACTING = "substract";
    private static final String OPERATION_SUBSTRACTING_SYMBOL = "-";
    private static final String OPERATION_MULTIPLING = "multiply";
    private static final String OPERATION_MULTIPLING_SYMBOL = "\\*";
    private static final String OPERATION_DIVIDING = "divide";
    private static final String OPERATION_DIVIDING_SYMBOL = "/";


    public static void main(String[] args) {

        System.out.println(zwrocWynik("add 5+10+15"));
        System.out.println(zwrocWynik("substract 10-5-5"));
        System.out.println(zwrocWynik("multiply 8*17"));
        System.out.println(zwrocWynik("divide 10/2"));
    }

    static Integer zwrocWynik(String napis) {

        String[] tablica = napis.split(" ");
        Integer wynik = 0;

        switch (tablica[0]) {
            case OPERATION_ADDING:
                String[] elementy1 = tablica[1].split(OPERATION_ADDING_SYMBOL);        //podzielilismy po plusach
                for (String element : elementy1) {
                    wynik += Integer.parseInt(element);
                }
                break;

            case OPERATION_SUBSTRACTING:
                String[] liczbyDoOdjecia = tablica[1].split(OPERATION_SUBSTRACTING_SYMBOL);
                wynik = Integer.parseInt(liczbyDoOdjecia[0]);
                for (int i = 1; i < liczbyDoOdjecia.length; i++) {
                    wynik -= Integer.parseInt(liczbyDoOdjecia[i]);
                }
                break;

            case OPERATION_MULTIPLING:
                String[] liczbyDoMnozenia = tablica[1].split(OPERATION_MULTIPLING_SYMBOL);
                wynik = Integer.parseInt(liczbyDoMnozenia[0]);
                for (int i = 1; i < liczbyDoMnozenia.length; i++) {
                    wynik *= Integer.parseInt(liczbyDoMnozenia[i]); //napis jako String jest przetwarzany na wartość liczbową
                }
                break;

            case OPERATION_DIVIDING:
                String[] liczbyDoDzielenia = tablica[1].split(OPERATION_DIVIDING_SYMBOL);
                wynik = Integer.parseInt(liczbyDoDzielenia[0]);
                for (int i = 1; i < liczbyDoDzielenia.length; i++) {
                    wynik /= Integer.parseInt(liczbyDoDzielenia[i]);
                }

                break;
            default:
                wynik = null;
        }
        return wynik;  //return null          metoda zwraca Integer, wiec można zrobic return null
    }
}
