package com.company;
/*
Utwórz metodę, w której pętlą *wyświetlisz* wszystkie liczby od liczby przekazanej jako parametr do `0` (tj. w kolejności malejącej).
> Dla `9` wyświetli `9, 8, 7, 6, 5, 4, 3, 2, 1, 0`
>
> Dla `4` wyświetli `4, 3, 2, 1, 0`
 */

public class Zadanie21 {
    public static void main(String[] args) {
        wyswietlWDol(9);

    }

    static void wyswietlWDol(int granica) {

        for (int i = granica; i >= 0; i--) { //i=i-1 lub i-=1 lub i--
            System.out.print(i + " ");
        }
    }
}
