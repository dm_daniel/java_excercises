package com.company;
/*
*ZADANIE #114*
Utwórz metodę, która przyjmuje jeden parametr - napis (typu `String`).
Metoda ma zwrócić mapę w postaci `String : List<Integer>`,
w której jako klucze mają być litery (z napisu wejściowego) a jako wartości - *pozycje* na jakich występują one w napisie wejściowym.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zadanie114 {
    public static void main(String[] args) {
        System.out.println(mapaPozycjiLiter("Anna"));

    }


    static Map<String, List<Integer>> mapaPozycjiLiter(String slowo) {

        Map<String, List<Integer>> mapa = new HashMap<>();
        String[] tabZnakow = slowo.split("");

        for (int i = 0; i < tabZnakow.length; i++) {
            List<Integer> list;                 //deklaracja zmiennej
            if (mapa.containsKey(tabZnakow[i])) {   //czy mapa zawiera klucz , a pozniej co jest na konkretnej pozycji w tablicy
                list = mapa.get(tabZnakow[i]);      //mapa zwróci listę pozycji, która znajduje się pod konkretnym kluczem
            } else {
                list = new ArrayList<>();       //tworzymy nową Listę
            }
            list.add(i);    //dodajemy pozycję do listy
            mapa.put(tabZnakow[i], list);   //wstawiamy znak z tablicy
        }
        return mapa;
    }


}
