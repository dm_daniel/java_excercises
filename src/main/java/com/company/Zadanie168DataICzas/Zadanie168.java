package com.company.Zadanie168DataICzas;

import com.sun.org.apache.xerces.internal.impl.dv.DTDDVFactory;

import java.sql.SQLOutput;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;

/*
*ZADANIE #168*
Data & Czas
 */
public class Zadanie168 {
    public static void main(String[] args) {
//Local time:
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
//Local Date:
//        method6();
//        method7();
//        method8();
//        method9();
//        method10();
//Local Date Time (obsługa pełnej daty)
//        method11();
//        method12();
//Strefy czasowe Zoned Date time
//        method13();
//        method14();
//        method15();
//Period & Duration
//        method16();
//        method17();
//        method18();

    }

    private static void method18() {
        String firstDateTime = "08.09.2018 13:20";
//        String secondDateTime = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

        LocalDateTime firstLocalDateTime = LocalDateTime.parse(firstDateTime, formatter);
        LocalDateTime secondLocalDateTime = LocalDateTime.now();

        long years = ChronoUnit.YEARS.between(firstLocalDateTime, secondLocalDateTime);
        long months = ChronoUnit.MONTHS.between(firstLocalDateTime, secondLocalDateTime);
        long weeks = ChronoUnit.WEEKS.between(firstLocalDateTime, secondLocalDateTime);
        long days = ChronoUnit.DAYS.between(firstLocalDateTime, secondLocalDateTime);
        long hours = ChronoUnit.HOURS.between(firstLocalDateTime, secondLocalDateTime);
        long minutes = ChronoUnit.MINUTES.between(firstLocalDateTime, secondLocalDateTime);
        long seconds = ChronoUnit.SECONDS.between(firstLocalDateTime, secondLocalDateTime);
        long nanos = ChronoUnit.NANOS.between(firstLocalDateTime, secondLocalDateTime);

        System.out.printf("Pomiędzy %s a %s różnica wynosi: \n", firstDateTime, secondLocalDateTime);
        System.out.println(years + " lat");
        System.out.println(months + " miesięcy");
        System.out.println(weeks + " tygodni");
        System.out.println(days + " dni");
        System.out.println(hours + " godzin");
        System.out.println(minutes + " minut");
        System.out.println(seconds + " second");
        System.out.println(nanos + " nanosekund");
    }

    private static void method17() {
        String firsDate = "10.05.2018";
        String secondDate = "23.12.2018";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate firstLocalDate = LocalDate.parse(firsDate, formatter);
        LocalDate secondLocalDate = LocalDate.parse(secondDate, formatter);

//troche lipa bo porównuje dni z dniami, miesiące z miesiącami xD
        Period period = Period.between(firstLocalDate, secondLocalDate);

        System.out.printf("Pomiędzy %s a %s różnica wynosi: \n", firsDate, secondDate);
        System.out.println(period.getDays() + " dni");
        System.out.println(period.getMonths() + " miesięcy");
        System.out.println(period.getYears() + " lat");

    }

    private static void method16() {
        String firstTime = "11:20";
        String secondTIme = "16:20";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        LocalTime firstLocalTime = LocalTime.parse(firstTime, formatter);
        LocalTime secondLocalTime = LocalTime.parse(secondTIme, formatter);

        Duration duration = Duration.between(firstLocalTime, secondLocalTime);

        System.out.printf("Pomiędzy %s a %s róznica wynosi: \n", firstTime, secondTIme);
        System.out.println(duration.toHours() + " godzin");
        System.out.println(duration.toMinutes() + " minut");
        System.out.println(duration.getSeconds() + " sekund");
        System.out.println(duration.getNano() + " nano sekund");


    }

    private static void method15() {
        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();

        availableZoneIds
                .stream()
                .sorted()
                .forEach(System.out::println);
    }

    private static void method14() {
        LocalDateTime inputLocalDateTime = LocalDateTime.now();
        ZoneId inputZone = ZoneId.of("Europe/Warsaw");

        ZoneId outputZone = ZoneId.of("US/Hawaii");
        LocalDateTime outputLocalDateTime = inputLocalDateTime  //data wejściowa
                .atZone(inputZone)  //mówie w jakiej jestem strefie
                .withZoneSameInstant(outputZone)    //na co ma być zamieniona
                .toLocalDateTime(); //pzerabiam na czas lokalny

        System.out.println(inputLocalDateTime);
        System.out.println(outputLocalDateTime);
    }

    private static void method13() {
        LocalDateTime inputDate = LocalDateTime.now();

        ZonedDateTime zonedDateTime = ZonedDateTime.of(inputDate, ZoneOffset.UTC);

        ZoneId zoneId = ZoneId.of("Europe/Moscow");

        ZonedDateTime outputZonedDateTime = zonedDateTime.withZoneSameInstant(zoneId);

        LocalDateTime dateTime = outputZonedDateTime.toLocalDateTime();

        System.out.println(inputDate);
        System.out.println(dateTime);
    }

    private static void method12() {
        String inputDate = "12.07.2018 11:22";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");        //zgodznie z jakim schematem mamy dane wejściowe

        LocalDateTime dateTime = LocalDateTime.parse(inputDate, inputFormat);  //jakie dane z jakim schematem bede przetwarzać

        System.out.println(dateTime);

        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("dd MMMM yyy (D 'dzień roku'), HH:mm");

        System.out.println(dateTime.format(outputFormat));
    }

    private static void method11() {
        LocalDateTime dateTime = LocalDateTime.of(2018, Month.FEBRUARY, 1, 14, 39, 05);

        System.out.println(dateTime.toString());

        DayOfWeek dayOfWeek = dateTime.getDayOfWeek();
        System.out.println(dayOfWeek);
        System.out.println(dayOfWeek.getValue());

        //przesuniecie dnia o 1 do przodu
        DayOfWeek plus = dayOfWeek.plus(1);
        System.out.println(plus);

        Month month = dateTime.getMonth();
        System.out.println(month);
        System.out.println(month.getValue());
        System.out.println(month.minLength());
        System.out.println(month.maxLength());

        //pobranie minuty dnia, sekundy dnia,nano sekundy dnia
        int minuteOfDay = dateTime.get(ChronoField.MINUTE_OF_DAY);
        System.out.println(minuteOfDay);
        int secondOfDay = dateTime.get(ChronoField.SECOND_OF_DAY);
        System.out.println(secondOfDay);
        long nanoOfDay = dateTime.getLong(ChronoField.NANO_OF_DAY);
        System.out.println(nanoOfDay);

    }

    private static void method10() {
        String inputDate = "4/8/2018";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("d/M/yyyy");

        LocalDate date = LocalDate.parse(inputDate, inputFormat);   //co , w jakim formacie
//gotowe standardy
        System.out.println("ISO_DATE : " + date.format(DateTimeFormatter.ISO_DATE));
        System.out.println("BASIC_ISO_DATE: " + date.format(DateTimeFormatter.BASIC_ISO_DATE));
        System.out.println("ISO_ODRINAL_DATE: " + date.format(DateTimeFormatter.ISO_ORDINAL_DATE));
        System.out.println("ISO_WEEK_DATE: " + date.format(DateTimeFormatter.ISO_WEEK_DATE));
        System.out.println("ISO_LOCAL_DATE: " + date.format(DateTimeFormatter.ISO_LOCAL_DATE));
    }

    private static void method9() {
        String inputDate = "31/12/2018";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("d/MM/yyyy");

        LocalDate date = LocalDate.parse(inputDate, inputFormat);
//w 3 rozne sposoby uzyskuje to samo
        int dayOfYear = date.get(ChronoField.DAY_OF_YEAR);
        System.out.println(dayOfYear);

        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("D");
        System.out.println(date.format(outputFormat));

        System.out.println(date.getDayOfYear());
    }

    private static void method8() {
        Scanner s = new Scanner(System.in);
        System.out.print("Podaj format: ");
        String pattern = s.nextLine();

        try {
            LocalDate date = LocalDate.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            System.out.println("Jest dziś: " + date.format(formatter));
        } catch (UnsupportedTemporalTypeException e) {
            System.out.println("PODANY ZŁY WZORZEC");
        } catch (IllegalArgumentException e) {
            System.out.println("COŚ POSZŁO NIE TAK");
        }
    }

    private static void method7() {
        LocalDate date = LocalDate.of(2018, 12, 1);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd//MM//yyyy");         //of pattern zgodznie z wzorcem
        System.out.println(date.format(formatter)); //odrazu formatuje i wyswietlam

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yy");
        //yy=18 , yyy lub yyyy = 2018
        System.out.println(date.format(formatter2)); //

        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("MMMM");
        //M lub MM = miesiąc cyfrą, MMM = miesiąc slownie w skróconej formie, MMMM- slownie w pelnej postaci
        System.out.println(date.format(formatter3));

        DateTimeFormatter formatter4 = DateTimeFormatter.ofPattern("LLLL");
        //LL,LLL jak wyżej. przy M
        //LLLL - grudzień, miesiąc słownie nie odmieniony
        System.out.println(date.format(formatter4));

        DateTimeFormatter formatter5 = DateTimeFormatter.ofPattern("EEEE");
        //EEEE = sobota (dzień w pełnej formie)
        System.out.println(date.format(formatter5));

        DateTimeFormatter formatter6 = DateTimeFormatter.ofPattern("d MMMM, yyyy::EEEE::");
        System.out.println(date.format(formatter6));
    }

    private static void method6() {
        LocalDate date = LocalDate.of(2018, Month.DECEMBER, 1);

        System.out.println(date);
    }

    private static void method5() {
        String inputTime = "13:05:33";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("HH:mm:ss"); //tworzymy format związany z danymi wejsciowymi

        LocalTime time = LocalTime.parse(inputTime, inputFormat);

        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("HH_mm_ss");

        String outputTime = time.format(outputFormat);
        System.out.println(outputTime);
    }

    private static void method4() {
        String currentTime = "12+51--28";       //godzina+minuty--sekundy dla draki (dane w dziwnym formacie)

        DateTimeFormatter format = DateTimeFormatter.ofPattern("HH+mm--s");     //ja podaję do jakiego wzorca ma pasować tekst z datą

        LocalTime time = LocalTime.parse(currentTime, format);  //parse przetwarza wartosic (nasz current time z naszym formatem)

        System.out.println(time);       //wyświetla czas w normalnym formacie

        int hour = time.get(ChronoField.HOUR_OF_DAY);        //chronofield - pole związane z czasem
        int minute = time.get(ChronoField.MINUTE_OF_HOUR);

        System.out.printf("Mamy %s min. po godzinie %s-tej\n", minute, hour);
    }

    private static void method3() {
        LocalTime t1 = LocalTime.now();

        System.out.println(t1);
    }

    private static void method2() {
        LocalTime t1 = LocalTime.parse("12:43:25");

        System.out.println(t1.getHour()); //12
        System.out.println(t1.getMinute());//43
        System.out.println(t1.getSecond());//25
        System.out.println(t1.getNano());//0
    }

    private static void method1() {
        LocalTime t1 = LocalTime.of(12, 3, 20);         //nie tworzymy nowych obiektów tylko bazujemy na parametrach
        System.out.println(t1.getHour());

        System.out.printf("Godzina: %s, minuta: %s\n",
                t1.getHour(),
                t1.getMinute(),
                t1.getSecond());


        System.out.println(t1);
    }


}
