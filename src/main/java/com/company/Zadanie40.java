package com.company;
/*
*ZADANIE #40*
Utwórz metodę, która zwraca tablicę o podanym rozmierza wypełnioną losowymi liczbami (użyj klasy `Random()`).
Rozmiar tablicy ma być parametrem metody.
 */

import java.util.Arrays;
import java.util.Random;

public class Zadanie40 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(tablica2(10)));        //wyswietlenie soutem tablicy jest nie mozliwe wiec uzywamy klasy arrays
    }

    static int[] tablica1(int rozmiarTablicy) {
        int[] tablica = new int[rozmiarTablicy];
        Random r = new Random();
        for (int pozycja : tablica) {       //tą pętlą nie wpływamy na zbiór(nie modyfikuje danych)
            pozycja = r.nextInt();           //przypisz do pozycji kolejny losowy int (r. wyswietli dostepne metody w tej klasie)
        }
        return tablica;
    }

    static int[] tablica2(int rozmiarTablicy) {
        int[] tablica = new int[rozmiarTablicy];
        Random r = new Random();

        for (int pozycja = 0; pozycja < rozmiarTablicy; pozycja++) {          //lub zamiast i<rozmiarTablicy -> tablica.length
            tablica[pozycja] = r.nextInt(11) - 4;       //r.NextInt  ...losuje wartosci int z przedziału 10 do -10
        }

        return tablica;
    }
}
