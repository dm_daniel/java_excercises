package com.company.zadanie174WątkiThreadAndRunnable;

//dziedziczymy po klasie Thread(wątek)
public class WlasnyWątek extends Thread {
    @Override
    public void run() {
        System.out.println("Wątek startuje, ID wątku: " + Thread.currentThread().getId());
        for (int i = 0; i < 20; i++) {
            System.out.println(i + " identyfikator wątku: " + Thread.currentThread().getId());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Wątek zakończył pracę, ID wątku: " + Thread.currentThread().getId());
    }
}
