package com.company.zadanie174WątkiThreadAndRunnable;

public class Zadanie174 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
        method5();
    }

    private static void method5() {
        System.out.println("ID wątku main: " + Thread.currentThread().getId());
        WlasnyWątek watek = new WlasnyWątek();
        WlasnyWątek watek2 = new WlasnyWątek();
        watek.start();
        watek2.start();

        //obecny wątek musi poczekać na inny wątek
        try {
            watek.join();
            watek2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("zakończenie pracy wątku main, ID wątku: " + Thread.currentThread().getId());
    }

    private static void method4() {
        //runnable jest interfejsem
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 20; i++) {
                    System.out.printf("%s ", i);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        //przekazuje w konstruktorze obiekt klasy Runnable
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private static void method3() {
        Thread watek1 = new Thread() {
            @Override
            public void run() {
                long id = Thread.currentThread().getId();

                for (int i = 0; i < 20; i++) {
                    System.out.printf("%s.          (id=%s)\n", i, id);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        watek1.start();

        Thread watek2 = new Thread() {
            @Override
            public void run() {
                long id = Thread.currentThread().getId();

                for (int i = 0; i < 20; i++) {
                    System.out.printf("%s. (id=%s)\n", i, id);
                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        watek2.start();
    }

    private static void method2() {
        //klasa anonimowa:
        Thread watek = new Thread() {

            //nadpisujemy metodę run
            @Override
            public void run() {
                System.out.println("Identyfikator nowego wątku: " + Thread.currentThread().getId());
            }
        };
        watek.start();
    }

    private static void method1() {
        System.out.println("Identyfikator wątku: " + Thread.currentThread().getId());
    }

}
