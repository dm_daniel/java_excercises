package com.company;
/*
*ZADANIE #90*
Utwórz metodę, która przyjmuje tablicę ciągów znaków.
Metoda ma zwrócić napis w formie zdania (z dużą literom na początku i kropką na końcu).
> DLa `["ala", "ma", "kota"]` zwróci `"Ala ma kota."`
> DLa `["lubię", "kolor", "żółty"]` zwróci `"Lubię kolor żółty."`
 */

public class Zadanie90 {
    public static void main(String[] args) {
        String[] tablica = {"ala", "ma", "kota"};
        System.out.println(zwrocZdanie(tablica));
    }

    static String zwrocZdanie(String[] znaki) {
        char c = '.';
        String zdanie = "";

        for (String wyraz : znaki) {
            zdanie += wyraz + " ";
        }
//TODO
        c = zdanie.replace(" ", ".").charAt(zdanie.length() - 1);
        c = zdanie.toUpperCase().charAt(0);

        System.out.println(c);
        return zdanie;

    }
}
