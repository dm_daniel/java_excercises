package com.company;
/*
*ZADANIE #69*
Utwórz metodę, która przyjmuje wysokość trójkąta Pascala a następnie tworzy odpowiednią strukturę danych i ją wypełnia
> dla `6`  zwróci
>`[1]`
>`[1, 1]`
>`[1, 2, 1]`
>`[1, 3, 3, 1]`
>`[1, 4  6,  4,  1]`
>`[1, 5, 10, 10, 5, 1]`
>`[1, 6, 15, 20, 15, 6, 1]`
 */

import java.util.Arrays;

public class Zadanie69 {
    public static void main(String[] args) {

        wyswietlTablice(zwrocStrukture(10));
    }

    static int[][] zwrocStrukture(int wysokoscTrojkata) {

        int[][] trojkatPascala = new int[wysokoscTrojkata][];        // nie trzeba podawac drógiej długości, nie znamy szerokosci wiersza, bo kazdy z nich bedzie miał inną długość, wiec jej nie podajemy

        for (int wiersz = 0; wiersz < wysokoscTrojkata; wiersz++) {
            trojkatPascala[wiersz] = new int[wiersz + 1];        //na tej konkretnej pozycji przypisz mi nową tablice
            int number = 1; //ustawiamy przed pętla bo trojkat pascala zawsze bierze cos od 1

            for (int kolumna = 0; kolumna <= wiersz; kolumna++) {
                trojkatPascala[wiersz][kolumna] = number;           //musimy zapamietywac tą wartość bo ona bierze udział w kolejnym liczeniu
                number = number * (wiersz - kolumna) / (kolumna + 1);
            }
        }

        return trojkatPascala;
    }

    static void wyswietlTablice(int[][] trojkatPascala) {        //nazwy jest obojetna
        for (int[] wiersz : trojkatPascala) {
            System.out.println(Arrays.toString(wiersz));
        }

    }

}
