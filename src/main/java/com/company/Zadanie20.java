package com.company;
/*
Utwórz metodę, w której pętlą wyświetlisz wszystkie liczby od `0` do wartości podanej przez użytkownika (przekazywanej jako parametr do tej metody)
> gdyż użytkownik poda `3` wyświetl `0, 1, 2, 3`
>
> gdyż użytkownik poda `9` wyświetl `0, 1, 2, 3, 4, 5, 6, 7, 8, 9`
>
> gdyż użytkownik poda `18` wyświetl `0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18`
 */

public class Zadanie20 {
    public static void main(String[] args) {
        wyswietlLiczbe(3);
        wyswietlLiczbe(9);
        wyswietlLiczbe(18);

    }

    static void wyswietlLiczbe(int granica) {
        for (int pozycja = 0; pozycja <= granica; pozycja++) { //skrót fori
            System.out.print(pozycja + ",");

        }
    }
}
