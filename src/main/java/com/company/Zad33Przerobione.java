package com.company;

public class Zad33Przerobione {
    static String x = "X\t";
    static String o = "\t";

    public static void main(String[] args) {

        method(7, 7);
    }

    static void method(int szerokosc, int wysokosc) {
        for (int i = 1; i <= wysokosc; i++) {
            for (int j = 1; j <= szerokosc; j++) {
                if (i > 1 && i <= wysokosc - 1 && j <= szerokosc - 1 && j != 1) {
                    System.out.print(o);
                } else
                    System.out.print(x);
            }
            System.out.println();
        }
    }
}
