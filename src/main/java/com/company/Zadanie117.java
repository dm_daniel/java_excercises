package com.company;
/*
*ZADANIE #117*
Utwórz metodę, która przyjmuje dwa parametry - mapę (w postaci `String : Integer`) oraz poszukiwaną liczbę.
Metoda ma zwrócić informację ile razy w mapie występuje (jako wartość) podana liczba. (edited)
 */

import java.util.HashMap;
import java.util.Map;

public class Zadanie117 {
    public static void main(String[] args) {
        Map<String, Integer> mapa = new HashMap<>();
        mapa.put("Ala", 4);
        mapa.put("Ala2", 5);
        mapa.put("Karol", 6);
        mapa.put("Kasia", 5);
//        mapa.put("Kasia",11); nie przyjmie, bo duplikujemy klucz
        mapa.put("Karolina", 5);
        mapa.put("Danielek", 5);
        System.out.println(ileRazyWMapieWystepujeLiczba(mapa, 5));
    }

    static int ileRazyWMapieWystepujeLiczba(Map<String, Integer> mapa, int szukanaLiczba) {
        int licznik = 0;

        for (Integer wartosc : mapa.values()) {
            if (wartosc == szukanaLiczba) {
                licznik += 1;
            }
        }
        return licznik;
    }
}
