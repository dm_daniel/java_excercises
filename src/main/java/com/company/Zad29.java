package com.company;

/*
Utwórz metodę, do której przekazujesz dwa parametry następnie wyswietlasz wszystkie liczby z podanego przedziału podzielne przez 3 albo 5
Dane wyświetl w formie:
> dla `0, 13` wyświetli:
>
 ```0  <-- 3, 5
 1
 2
 3  <-- 3
 4
 5  <-- 5
 6
 7
 9  <-- 3
10  <-- 5
11
12  <-- 3
13
14
15  <-- 3, 5```
 */
public class Zad29 {
    public static void main(String[] args) {
        zaznaczPodzielne(0, 15);
    }

    static void zaznaczPodzielne(int przedzialStart, int przedzialStop) {
        for (int i = przedzialStart; i <= przedzialStop; i++) {
            System.out.print(i);
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print("<-- 3, 5");
            } else if (i % 3 == 0) {
                System.out.print(" <-- 3");
            } else if (i % 5 == 0) {
                System.out.print(" <--5");
            }
            System.out.println();
        }
    }
}
