package com.company;
/*
*ZADANIE #48*
Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz początek i koniec przedziału.
Metoda ma zwrócić nową tablicę, która będzie zawierać elementy z wybranego przedziału.
> Dla `([1, 2, 3, 4, 5],  2,  4)`
> zwróci `[3, 4, 5]`
 */

import java.util.Arrays;

public class Zadanie48 {
    public static void main(String[] args) {

        System.out.println("tablica po przycieciu: " +
                Arrays.toString(
                        zwrocTabZPrzedzialu(new int[]{2, 4, 6, 7, 8, 9}, 1, 4)));            //deklaracja tablicy w wywołaniu metody + arrays to string
                                                                                                        // zeby wyswietlic tablice
    }

    static int[] zwrocTabZPrzedzialu(int[] tablicaStara, int start, int stop) {
        int[] tablicaZmniejszona = new int[stop - start + 1];              //albo podajemy rozmiar albo czym ma byc wypelniona
        for (int index = start; index <= stop; index++) {
            tablicaZmniejszona[index - start] = tablicaStara[index];
        }
        return tablicaZmniejszona;
    }
}
