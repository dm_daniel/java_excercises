package com.company;
/*
*ZADANIE #114*
Utwórz metodę, która przyjmuje jeden parametr - napis (typu `String`).
Metoda ma zwrócić mapę w postaci `String : List<Integer>`, w której jako klucze mają być litery (z napisu wejściowego)
a jako wartości - *pozycje* na jakich występują one w napisie wejściowym.

*ZADANIE #115  -  (PRACA DOMOWA ODWROCONE ZADANIE 114)
Utwórz metodę, która przyjmuje jeden parametr - mapę w postaci `String : List<Integer>`.
Metoda ma zwrócić napis. W mapie przekazanej jako parametr, kluczami są litery a jako wartościami - *pozycje*
na jakich mają wystąpić w zwracanym napisie. (edited)
 */


import java.util.*;

public class Zadanie115Domowe {
    public static void main(String[] args) {

        Map<String, List<Integer>> mapa2 = new HashMap<>();

        mapa2.put("B", Arrays.asList(0));
        mapa2.put("a", Arrays.asList(1, 3, 5));
        mapa2.put("n", Arrays.asList(2, 4));


        System.out.println(zwrocNapis(mapa2));


    }

    static String zwrocNapis(Map<String, List<Integer>> mapa) {
        String wyraz = "";
        int dlugosc = 0;

        List<Integer> lista = new ArrayList<>();

        //zliczam dlugosci wszystkich list w mapie
        for (List<Integer> wartosci : mapa.values()) {
            dlugosc += wartosci.size();
        }

        //tworze tablice o poprawnej dlugosci
        String[] tablica = new String[dlugosc];

        //lista pozycji
        for (List<Integer> wartosci : mapa.values()) {
            lista.addAll(wartosci);
        }

        for (Map.Entry<String, List<Integer>> entry : mapa.entrySet()) {        //mapa.entrySet (wartości z mapy),  entry(struktura przyjmująca 2 pola)
//            System.out.println("entry");

            //wypelniam tablice literami
            for (Integer pozycja : entry.getValue()) {      //przechodzimy po wartościach z mapy
                System.out.println("klucz=" + entry.getKey() + " pozycja=" + pozycja);
                tablica[pozycja] = entry.getKey();
            }
        }

//        return wyraz.toString();
        //ewentualie elementy zostaną scalon znakiem ""     (join wstawia ten separator(delimiter) między elementami a na końcu już nie)
        return String.join("", tablica);
    }
}
