package com.company;

/*
*ZADANIE #56*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma zwrócić *ile elementów* (idąc po kolei od *PRAWEJ*) należy zsumować by przekroczyć podany (jako drugi) parametr
dla `([1,2,3,4,5,6],  9)`
należy zwrócić `2`
 */
public class Zadanie56 {
    public static void main(String[] args) {
        int[] tablica = {1, 2, 3, 4, 1, 1};
        System.out.println(ileElementow(tablica, 0));
    }

    static int ileElementow(int[] tablica, int miniSuma) {
        int suma = 0;
        for (int index = tablica.length - 1; index >= 0; index--) {      //numerujemy od 0 dlatego trzeba odjac 1
            suma += tablica[index];
            if (suma >= miniSuma) {
                return tablica.length - index;
            }
        }

        return -1;      //przyjelo sie ze jak jest cos nie tak to podajemy wartosc ujemna
    }
}
