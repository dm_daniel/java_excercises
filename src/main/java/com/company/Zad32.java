package com.company;
/*
Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy ) oraz wyświetla “choinkę” nie przerywając numerowania
> Dla `4` wyświetli:
>
1
2 3
4 5 6
7 8 9 10```
>
> Dla `6` wyświetli:
>
1
2 3
4 5 6
7 8 9 10
11 12 13 14 15
16 17 18 19 20 21```
 */

public class Zad32 {
    public static void main(String[] args) {
        // forChoinkaBezPrzerywania(5);
        whileChoinkaBezPrzerywania(5);
    }

    static void forChoinkaBezPrzerywania(int iloscWierszy) {
        int k = 1;
        for (int i = 1; i <= iloscWierszy; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(k++ + "\t");
            }
            System.out.println();
        }
    }

    static void whileChoinkaBezPrzerywania(int iloscWierszy) {
        int i = 1;
        int j = 1;
        int x = 1;
        while (i <= iloscWierszy) {
            while (j <= i) {
                System.out.print(x + "\t");
                j++;
                x++;
            }
            System.out.println();
            j = 1;
            i++;
        }
    }
}



