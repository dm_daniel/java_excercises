package com.company.zadanie77;

public class Osoba {
    //pola w klasie ( dobrym standardem jest definiowanie ich na górze)
    String imie;
    String nazwisko;
    int wiek;
    Boolean czyMezczyzna;

    //konstruktor
    Osoba(String imie, String nazwisko) {        //konstruktor 2 parametrowy(2 pola obowiązkowe)
        this.imie = imie;                       //szuka nazwiska w polach w klasie, jeśli nie znajdzie jej w konstruktorze
        this.nazwisko = nazwisko;               //jeśli nie ma wartości to dostaje null,,, .this -> chodzi o pole w klasie

    }

    //konstruktor
    public Osoba(String imie, String nazwisko, int wiek, Boolean czyMezczyzna) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyMezczyzna = czyMezczyzna;
    }

    @Override
    public String toString() {
        if (wiek == 0 || czyMezczyzna == null) {
            return String.format("%s %s",
                    imie,
                    nazwisko);

        } else {
            return String.format("%s %s (%s 1.) - %s",
                    imie,
                    nazwisko,
                    wiek,
                    czyMezczyzna ? "mężczyzna" : "kobieta");
        }
    }

    //metoda
    void przedstawSie() {
        System.out.printf("Cześć jestem %s %s", imie, nazwisko);
    }

    boolean czyPelnoletni() {
        return wiek < 18 ? false : true;            //zapis wydłużony
        //return wiek>18;  samo to wyrazenie zwroci jakas wartość
    }

}


