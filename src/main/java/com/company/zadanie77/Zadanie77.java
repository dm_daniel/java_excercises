package com.company.zadanie77;

/*
*ZADANIE #77*
Utwórz klasę `Person` (`GenericClass`) posiadającą
*pola reprezentujące:*
>* imie
>* nazwisko
>* wiek
>* czyMezczyzna (lub czyKobieta)

*konstruktory oraz metody służące do:*
>* przedstawienia się (`Witaj, jestem Adam Nowak, mam 20 lat i jestem mężczyzną`)
>* sprawdzenia czy osoba jest pełnoletnia
 */
public class Zadanie77 {
    public static void main(String[] args) {

        Osoba o1 = new Osoba("Janusz", "Cebula");
        System.out.println(o1);
        Osoba k = new Osoba("kasia", "kowalska", 28, false);
        System.out.println(k);
        Osoba o2 = new Osoba("Daniel", "Mikołajczyk", 28, true);
        System.out.println(o2);
        o2.przedstawSie();
        System.out.println();
        if (o2.czyPelnoletni()) {
            System.out.println("GenericClass jest pełnoletnia");
        } else {
            System.out.println("GenericClass niepełnoletnia");
        }
    }
}
