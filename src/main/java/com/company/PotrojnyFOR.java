package com.company;

/*
Potrójny FOR (triFORce)
 */
public class PotrojnyFOR {
    public static void main(String[] args) {

        wyswietlTabliczkeMnozenia(5);
    }

    static void wyswietlTabliczkeMnozenia(int wymiar) {
        for (int i = 1; i <= wymiar; i++) {
            System.out.print(i + "A.");
            for (int j = 1; j <= wymiar; j++) {
                System.out.print(j + "B.");
                for (int k = 1; k <= wymiar; k++) {
                    System.out.print(k + "C.");
                }
                System.out.println("Linia1");
            }
            System.out.println("Linia2");
        }
        System.out.println("Linia3");
    }
}
