package com.company.zadanie78;
/*
*ZADANIE #78*
Utwórz klasę `Rectangle` (`Prostokąt`) posiadającą
>*pola reprezentujące:*
>* dłuższy bok
>* krótszy bok
>
>*konstruktory oraz metody służące do:*
>* policzenia obwodu
>* policzenia pola powierzchni
>* porównania czy pola powierzchni dwóch przekazanych prostokątów są takie same (zwracająca wartość `boolean`)
>
>*Utwórz tablicę zawierającą 5 obiektów tej klasy i w pętli wyświetl zdania w formie*
>`10 x 20 = 200`
 */

public class Zadanie78 {
    public static void main(String[] args) {
        Rectangle prostokat1 = new Rectangle(5, 4);     //stworzylismy nowy typ w javie - Rectangle o nazwie prostokat1 = new Rectangle (konstruktor)
        System.out.println(prostokat1.obliczObwod());
        System.out.println(prostokat1.getDluzszyBok());     //nie mam mozliwości modyfikacji dlugości, (bo są private) lecz moge pobrać te wartości
        System.out.println(prostokat1.getKrotszyBok());
        System.out.println(prostokat1.obliczPole());
        System.out.println();

        Rectangle prostokat2 = new Rectangle(18, 9);
        System.out.println(prostokat2.getKrotszyBok());
        System.out.println(prostokat2.getDluzszyBok());
        System.out.println(Rectangle.czyPolaSaTakieSame(prostokat1, prostokat2));    //wyswietlenie metody statycznie
    }
}
