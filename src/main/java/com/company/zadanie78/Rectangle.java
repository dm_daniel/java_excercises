package com.company.zadanie78;

public class Rectangle {
    //to są 2 pola w tej klasie, są "private", więc można się do nich odwołać tylko w tej klasie
    private int dluzszyBok;
    private int krotszyBok;

    // konstruktor 2 parametrowy, można go użyć w całym projekcie
    // dzięki modyfikatorowi dostępu "public"
    public Rectangle(int dluzszyBok, int krotszyBok) {
        //this. = odwołujemy się do obecnego obiektu
        // szukamy "krotszyBok" odrazu w polach klasy ( pomijamy parametry podane w metodzie)
        this.dluzszyBok = dluzszyBok;
        this.krotszyBok = krotszyBok;
    }

    //metoda typu domyslnego
    int obliczObwod() {   //metoda nie jest static wiec mozemy korzystac z pól
        return 2 * dluzszyBok + 2 * krotszyBok;
    }

    int obliczPole() {
        return getDluzszyBok() * getKrotszyBok(); //uzylem metody getterów zamiast dluzszyBok*krotszyBok
    }

    //metoda statyczna
    static boolean czyPolaSaTakieSame(Rectangle r1, Rectangle r2) {
        return r1.obliczPole() == r2.obliczPole();          //czy wartosci dla tych obiektow są takie same
    }

    //dwa gettery: ( do zwracania informacji )  //Alt+insert (Getter,setter,toString, etc...)
    public int getDluzszyBok() {
        return dluzszyBok;
    }


    public int getKrotszyBok() {
        return krotszyBok;
    }

}
