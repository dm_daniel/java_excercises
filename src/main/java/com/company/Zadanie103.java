package com.company;
/*
*ZADANIE #103*
Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. `ArrayList`) oraz poszukiwana liczba.

Metoda ma za zadanie zwrócić *listę pozycji* na których znajduje się liczba (przekazana jako drugi parametr).
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie103 {
    public static void main(String[] args) {

        Integer[] tablica = new Integer[]{9, 1, 2, 3, 9, 4, 9, 5, 6, 7, 8, 9, 9};
        List<Integer> lista1 = new ArrayList<>(Arrays.asList(tablica));

        System.out.println(zwrocListePozycji(lista1, 9));
    }

    static List<Integer> zwrocListePozycji(List<Integer> lista, int liczba) {
        List<Integer> listaPozycji = new ArrayList<>();

        for (int index = 0; index < lista.size(); index++) {
            if (lista.get(index) == liczba) {
                listaPozycji.add(index);
            }
        }
        return listaPozycji;
    }
}
