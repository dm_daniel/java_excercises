package com.company;


import java.util.Arrays;

/*
*ZADANIE #43*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca drugą największą liczbę w tablicy.
> dla `[1, 2, 3, 4, 5]` zwróci `4`
 */
public class Zadanie43 {
    public static void main(String[] args) {
        int[] tablica = {10, 11, 9, 5, 6};
        System.out.println(drugaNajwieksza(tablica));
    }

    static int drugaNajwieksza(int[] tablica) {
        int max = Integer.MIN_VALUE;        //ustalamy najmniejsza wartosc jaka moze byc z zakresu integer
        int drugiMax = Integer.MIN_VALUE;
        for (int pozycja = 0; pozycja < tablica.length; pozycja++) {
            if (tablica[pozycja] > max) {
                drugiMax = max;
                max = tablica[pozycja];
            } else if (tablica[pozycja] > drugiMax && tablica[pozycja] != max) {
                drugiMax = tablica[pozycja];
            }
        }

        return drugiMax;
    }
}
