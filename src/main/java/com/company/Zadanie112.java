package com.company;
/*
*ZADANIE #112*
Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania wartości `-1`
a następnie zwraca mapę wartości, gdzie kluczem jest liczba a wartością jest liczba jej wystąpień.
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zadanie112 {
    public static void main(String[] args) {
        System.out.println(zwrocMapeWartosci());
    }

    static Map<Integer, Integer> zwrocMapeWartosci() {
        Map<Integer, Integer> mapa = new HashMap<>();
        Scanner s = new Scanner(System.in);

        while (true) {
            System.out.print("Podaj Liczbe: ");
            int wprowadzonaLiczba = s.nextInt();
            if (wprowadzonaLiczba == -1) {
                break;
            }
            //sprawdza czy mapa zawiera taki klucz
            if (mapa.containsKey(wprowadzonaLiczba)) {
                //pobieramy z mapy wartość dla danego klucza
                int staraLiczbaWystapien = mapa.get(wprowadzonaLiczba);
                //na ten sam klucz wprowadzamy inną wartość(nadpisujemy wiersz)
                mapa.put(wprowadzonaLiczba, staraLiczbaWystapien + 1);
            } else {
                //jeśli mapa nie zawiera wartości, pod podanym kluczem wstawiamy wartość = 1
                mapa.put(wprowadzonaLiczba, 1);
            }
        }
        return mapa;
    }
}
