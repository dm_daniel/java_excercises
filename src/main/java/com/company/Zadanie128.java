package com.company;

/*
*ZADANIE #128*
Utwórz metodę która wykonuję operację dzielenia dwóch liczb przekazywanych jako parametry.
W przypadku podzielenia przez `0` metoda powinna zwrócić `null`.
 */
public class Zadanie128 {
    public static void main(String[] args) {

        System.out.println(dzielenie(6, 2));
        System.out.println(dzielenie(6, 0));
        System.out.println(dzielenie(-11, 0));
        System.out.println(dzielenie(6, 0) == null);
        System.out.println(dzielenie(0, 0));
        System.out.println(dzielenie(0, 6));

    }

    static private Integer dzielenie(int a, int b) {

        try {
            return a / b;           //formatuje z int do double bo zwracam Double w metodzie( wykonuje operacje dzielenia miedzy double a intem, wynik tego dzielenia bedzie typu double)
        } catch (ArithmeticException e) {
            return null;
        }
    }

}
