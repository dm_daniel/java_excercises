package com.company;
/*
*ZADANIE #67*
Utwórz metodę, która wylosuję planszę do gry Saper z zaznaczonymi bombami (wartość `true` oznacza bombe, wartość `false` oznacza puste pole).

Na potrzeby wizualizacji utwórz dodatkową metodę która będzie wyświetlać planszę gdzie bomby będą zaznaczone jako `*`, a puste pola np. `_`

Kolejna metoda ma zwrócić wypełnioną planszę z numerami (gdzie `-1` oznacza bombę).

>*Pierwsza metoda zwróci:*
>
 true false true
false false true
false false false```

>*Druga metoda wyświetli:*
>
 * _ *
 _ _ *
 _ _ _```

>*Trzecia metoda zwróci:*
>
 * 3 *
 1 3 *
 0 1 1```
 */

import java.util.Arrays;
import java.util.Random;

public class Zadanie66 {
    public static void main(String[] args) {
        boolean[][] planszaBazowa = wylosujPlansze(5);
        // plansza bazowa z  wartościami logicznymi jest przekazywana jako parametr do metody wypelnijLiczbami
        int[][] cokolwiek = wypelnijLiczbami(planszaBazowa);
        wyswietlPlansze(cokolwiek);
    }

    static boolean[][] wylosujPlansze(int wymiar) {
        boolean[][] plansza = new boolean[wymiar][wymiar];      //plansza jest struktura ktora wypelniamy
//tablica boolenow jest defaultowo wypelniona falsami, wystarczy wypelnic je minami
        Random random = new Random(); //nowy obiekt klasy random
        for (int proba = 0; proba < wymiar * wymiar * 0.2; proba++) {           //20% planszy zapelnimy minami
            int wiersz = random.nextInt(wymiar);        //random zaczyna od 0 do dlugosci wymiaru-1 bo 0 jest pierwsza liczba
            int kolumna = random.nextInt(wymiar);

            if (plansza[wiersz][kolumna]) {          // na dziendobry jest to wartosc logiczna czy  plansza[wiersz][kolumna] = true, sprawdzam czy w tym polu mam wartosc true
                proba--;                             //zmniejszamy probe o 1 zeby nie stracic proby losowania
//                System.out.println("bledne pole  " + wiersz + "," + kolumna);
            } else {
                plansza[wiersz][kolumna] = true;    //w polu o tych współrzednych wstawi true czyli mine
//                System.out.println("wylosowane pole  " + wiersz + "," + kolumna);
            }
        }
        return plansza;
    }

    static void wyswietlPlansze(boolean[][] plansza) {      //nie używamy tej metody w programie.
        for (boolean[] wiersz : plansza) {
            for (boolean elementWiersza : wiersz) {        //elementem wiersza(konkretne pole)
                System.out.print(elementWiersza == true ? "X" : "-");    //wyswietlamy X w miejscu gdzie jest mina i _ gdy jej nie ma
                System.out.print("\t");
            }
            System.out.println();
        }
    }

    static int[][] wypelnijLiczbami(boolean[][] tab) {
        int[][] tablicaWartosci = new int[tab.length][tab.length];

        for (int wiersz = 0; wiersz < tab.length; wiersz++) {
            for (int kolumna = 0; kolumna < tab[0].length; kolumna++) {
                //sprawdzamy czy pod danymi wspolrzednymi jest bomba
                if (tab[wiersz][kolumna] == true) {
                    //jeśli tak wstaw -1 dla bomby
                    tablicaWartosci[wiersz][kolumna] = -1;
                } else {
                    //dla danego pola o wspolrzednych [wiersz][kolumna] sprawdzamy ilosc bomb wokoło
                    tablicaWartosci[wiersz][kolumna] = zwrocLiczbaBomb(tab, wiersz, kolumna);

                }
            }

        }
        return tablicaWartosci;
    }

    static int zwrocLiczbaBomb(boolean[][] tab, int wiersz, int kolumna) {
        int liczbaBomb = 0;

        //podajemy zakres sprawdzenia pól wokół sprawdzanego pola(-1 bo zaczynamie wiersz i kolumne wczesniej)
        for (int i = wiersz - 1; i <= wiersz + 1; i++) {
            //+1 bo konczymy wiersz i kolumnę dalej, żeby sprawdzić wszystkie pola wokół badanego pola
//            którego współrzędne przekazujemy jako parametry do tej metody
            for (int j = kolumna - 1; j <= kolumna + 1; j++) {
                //sprawdzamy czy indeksy, które chcemy sprawdzić nie wykraczają poza planszę
                //(sprawdzamy z dwóch stron, wartości poniżej 0 , jak i wartości powyżej rozmiaru planszy
                if (j >= 0 && i >= 0 && j < tab.length && i < tab.length) {
                    //sprawdzamy, czy na danej pozycji jest bomba, jeśli tak zwiekszamy liczbe bomb +1
                    if (tab[i][j] == true) {
                        liczbaBomb++;
                    }

                }
            }

        }
        return liczbaBomb;
    }

    static void wyswietlPlansze(int[][] tablica) {

        for (int[] wiersz : tablica) {
            for (int pole : wiersz) {
                if (pole == -1) {
                    System.out.print("*\t");
                } else {
                    System.out.print(pole + "\t");
                }
            }
            System.out.println();
        }


    }
}
