package com.company;
/*
Utwórz metodę, do której przekazujesz dwa parametry następnie wyświetlasz wszystkie liczby z podanego
przedziału *I ICH DZIELNIKI*

Dane wyświetl w formie:
> dla `0, 6` wyświetli:
>
 1  <--  1
 2  <--  1, 2,
 3  <--  1, 3,
 4  <--  1, 2, 4,
 5  <--  1, 5,
 6  <--  1, 2, 4, 6```


 */

public class Zadanie36 {
    public static void main(String[] args) {
        wyswietlLiczby(5, 1);

    }

    static void wyswietlLiczby(int start, int stop) {
        for (int obecnaLiczba = start; obecnaLiczba <= stop; obecnaLiczba++) {


            System.out.print(obecnaLiczba + " <-- ");
            for (int dzielnik = 1; dzielnik <= obecnaLiczba; dzielnik++) {
                if (obecnaLiczba % dzielnik == 0) {
                    System.out.print(dzielnik + " ");
                }

            }
            System.out.println();
        }
    }
}
