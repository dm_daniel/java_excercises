package com.company;
/*
*ZADANIE #81*
Utwórz metodę, która przyjmuje zdanie (np. `"Ala ma kota."`, `"Dziś jest sobota"`),
a następnie ma zwrócić sumę długości *wszystkich* wyrazów.
 */

public class zadanie81 {
    public static void main(String[] args) {
        String k = ("Ala ma kota");
        System.out.println(sumaWyrazow(k));
        System.out.println(sumaWyrazow2(k));
    }

    static int sumaWyrazow(String zdanie) {
        int suma = 0;
        String[] tablica = zdanie.split(" ");
        for (String wyraz : tablica) {
            suma += wyraz.length();
        }
        return suma;
    }

    static int sumaWyrazow2(String zdanie) {

        return zdanie.replace(" ", "").length();    //zamieniamy spacje na nic i wyswietlamy dlugosc
    }

}
