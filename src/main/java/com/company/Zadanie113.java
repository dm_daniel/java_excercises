package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/*
*ZADANIE #113*
Utwórz metodę która przyjmuje liczbę, a następnie tyle razy losuje liczbę (przy wykorzystaniu klasy `Random`).
Metoda ma zwrócić mapę, która będzie przechowywać informację ile razy wystąpiła każda z liczb. (edited)
 */
public class Zadanie113 {
    public static void main(String[] args) {
        System.out.println(sprawdzIleRazy(22_000));
    }

    static Map<Integer, Integer> sprawdzIleRazy(int liczbaLosowan) {
        Random random = new Random();
        Map<Integer, Integer> mapa = new HashMap<>();

        for (int i = 0; i < liczbaLosowan; i++) {
            int wylosowanaLiczba = random.nextInt(11);  //losuje od 0 do 10
//            if (mapa.containsKey(wylosowanaLiczba)) {
//                mapa.put(wylosowanaLiczba, mapa.get(wylosowanaLiczba) + 1); //zwieszkam +1 po pobraniu
//                inny sposob
//                int staraLiczbaWystapien=mapa.get(wylosowanaLiczba);
//                mapa.put(wylosowanaLiczba,staraLiczbaWystapien+1);
//            } else {
//                mapa.put(wylosowanaLiczba, 1);
//            }
            int liczbaWystapien = mapa.getOrDefault(wylosowanaLiczba, 0);   //jesli znajdzie wartość w mapie to ją zwraca, jeśli nie zwróci wartość domyślną 0
            mapa.put(wylosowanaLiczba, liczbaWystapien + 1);
        }
        return mapa;
    }

}
