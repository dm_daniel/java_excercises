package com.company;
/*
*ZADANIE #67 - EXTRA*
Utwórz metodę, która przyjmuje dwuwymiarową tablicę (o rozmiarze `9 x 9`).
Metoda ma zwrócić informację logiczną czy jest to poprawnie wypełniona plansza do gry w Sudoku.
 */

import java.util.Arrays;

public class Zadanie67_extra_sudoku {
    public static void main(String[] args) {
        int[][] tablica = new int[][]{{1, 2, 3, 4, 5, 6, 7, 8, 9}, {1, 10, 3, 4, 5, 6, 7, 8, 9}};
        wyswietlTablice(tablica);

        System.out.println(czyWypelnionoWlasciwie(tablica));
    }

    static boolean czyWypelnionoWlasciwie(int[][] tablica) {

        for (int wiersz = 0; wiersz < tablica.length; wiersz++) {
            int licznik = 0;

            for (int kolumna = 0; kolumna <= 9; kolumna++) {
//sprawdzam czy w wierszu nie powtarza się cyfra
                for (int i = 1; i <= 8; i++) {
                    if (tablica[wiersz][kolumna] == tablica[wiersz][i]) {
                        licznik++;
                    } else if (licznik > 1) {
                        System.out.println("źle wypełniona");
                        break;
                    } else {
                        System.out.println("dobrze wypełniona");
                    }
                }
            }

        }
        return true;
    }

    static void wyswietlTablice(int[][] tablica) {

        for (int[] wiersz : tablica) {
            System.out.println(Arrays.toString(wiersz));
        }

    }
}
