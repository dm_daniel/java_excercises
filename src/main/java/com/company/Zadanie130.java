package com.company;
/*
*ZADANIE #130*
Utwórz metodę, która przyjmuje parametry typu `int` korzystając z mechanizmu `varargs`.
W przypadku nie przekazania żadnego parametru, metoda powinna rzucić własny wyjątek (z komunikatem o powodzie).
W przypadku podania parametrów, metoda powinna zwrócić ich sumę.
 */

public class Zadanie130 {
    public static void main(String[] args) {

        try {
            System.out.println(zwrocSume(1, 2, 6, 4, 8));
            System.out.println(zwrocSume());
        } catch (MojWlasnyWyjatek excpetion) {
            System.out.println(excpetion.getMessage());     //normalnie get.Message bylo by puste, ale w klasie MojWlasnyWyjatek przekazaliśmy tą wiadomość
        }
    }

    static int zwrocSume(int... liczba) throws MojWlasnyWyjatek {        //liczba bedzie tablicą (varargs), metoda może rzucić typ wyjątku którym jest klasa utworzoną niżej
        int suma = 0;
        if (liczba.length == 0) {   //jesli tablica jest pusta
            throw new MojWlasnyWyjatek("Brak parametrów do zsumowania");
        } else {
            for (int i = 0; i < liczba.length; i++) {   //jesli tablica jest nie pusta sumujemy elementy tablicy
                suma += liczba[i];
            }
            return suma;
        }
    }
}

class MojWlasnyWyjatek extends Exception {  //klasa mój wyjątek dzidziczy po klasie Exception
                        //excpetion ma 2 konstruktory ( jeden jest bezparametrowy a drugi jest nasz (z 1 parametrem) i przekazujemy go od rodzica) dzięki te
    MojWlasnyWyjatek(String wiadomosc) { //konstruktor 1 parametrowy
        super(wiadomosc); //przekazujemy do konstruktora rodzica
    }
}
