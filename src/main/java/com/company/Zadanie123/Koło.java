package com.company.Zadanie123;

public class Koło extends Ksztalt {
    private double promien;

    public Koło(int promien) {
        this.promien = promien;
    }

//dziedziczona metoda:
    @Override
    String wyswietlInformacjeOSObie() {
        return String.format("Jestem kołem i mój promień to %s", promien);
    }
//implementowane metody z interfejsu:
    @Override
    public double policzPole() {
        return Math.PI * (promien * promien);
    }

    @Override
    public double policzObjetosc(int wysokosc) {
        //TODO poprawić wzór na objętość kuli
        return policzPole() * wysokosc;
    }

    @Override
    public double policzObwod() {
        return 2 * Math.PI * promien;
    }
}
