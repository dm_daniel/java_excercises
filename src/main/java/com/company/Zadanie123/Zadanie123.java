package com.company.Zadanie123;
/*
*ZADANIE #123*
Utwórz klasę `Ksztalt` oraz klasy dziedziczące po niej:
- klasę `Prostokat` (z polami `dlugosc`, `szerokosc`),
- klasę `Kwadrat` (z polem `dlugosc`),
- klasę `Koło` (z polem `promien`).
Zablokuj możliwość tworzenia obiektów klasy `Ksztalt` oraz wymuś,
by klasy dziedziczące musiały zaimplementować metodę `wyswietlInformacjeOSobie()`. (edited)

Utwórz interfejs `Obliczenia` w którym będą metody `policzPole()`, `policzObwod()` oraz `policzObjetosc(int wysokosc)`.
Interfejs ten zaimplementuj we wcześniej utworzonych klasach. (edited)
 */

import java.util.ArrayList;
import java.util.List;

public class Zadanie123 {
    public static void main(String[] args) {

        Prostokat p = new Prostokat(5, 10);
        System.out.println(p.wyswietlInformacjeOSObie());
        Kwadrat k = new Kwadrat(5);
        Koło kolo = new Koło(12);

        List<Ksztalt> listaKsztalow = new ArrayList<>();
        listaKsztalow.add(p);
        listaKsztalow.add(k);
        listaKsztalow.add(kolo);

        //polimofrizm wywołuje metodę wyświetlInformacje bezpośrednio z konkretnej klasy
        for (Ksztalt ksztalt : listaKsztalow) {
            System.out.println(ksztalt.wyswietlInformacjeOSObie());
            System.out.printf("Pole wynosi: %.2f \n", ksztalt.policzPole());   //2f tylko 2 miejsca po przecinku
        }

    }
}
