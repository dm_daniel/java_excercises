package com.company.Zadanie123;

public class Kwadrat extends Prostokat {
    //wywołuje konstruktor 1 parametrowy(od rodzica dostałem 2 parametry, ale używam tylko 1)
    Kwadrat(int dlugosc) {      //tworząc obiekt kwadrat dziedzicze z kontstruktora
        // prostokąta i musze miec 2 parametry (ale dla kwadratu nie potrzebuje szrokości, wystaryczy dlugość
        super(dlugosc, dlugosc);        //wywoływany jest konstruktor nadklasy
    }

    @Override
    String wyswietlInformacjeOSObie() {
        return String.format("Jestem kwadratem o boku %s", dlugosc);
    }
}
