package com.company.Zadanie123;

interface Obliczenia { //lista metod,którą klasa musi posiadać

     double policzPole();

    double policzObjetosc(int wysokosc);

    double policzObwod();
}
