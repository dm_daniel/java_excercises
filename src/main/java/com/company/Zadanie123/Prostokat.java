package com.company.Zadanie123;

public class Prostokat extends Ksztalt {
//pola w klasie
    protected int dlugosc;  //protected otwiera dostęp zmiennej dla klas dziedziczących, otwiera również dostęp pakietowy !!!( 2 cechy protected)
    private int szerokosc;

    public Prostokat(int dlugosc, int szerokosc) {
        this.dlugosc = dlugosc;
        this.szerokosc = szerokosc;
    }
//metody implementowane
    //alt insert implement (implementuje konkretne metody z interfejsu Obliczenia)
    @Override
    public double policzPole() {
        return szerokosc * dlugosc;
    }

    @Override
    public double policzObjetosc(int wysokosc) {
        return policzPole() * wysokosc;
    }

    @Override
    public double policzObwod() {
        return 2 * dlugosc + 2 * szerokosc;
    }

    @Override
    String wyswietlInformacjeOSObie() {     //metoda dziedziczona
        return String.format(
                "jestem protokątem a moje boki to %s i %s",
                dlugosc,
                szerokosc);

    }
}
