package com.company;
/*
Utwórz metodę, do której przekazujesz trzy parametry (np `start`, `amount`, `jump`).
Następujące parametry odpowiadają za:
pierwszy - początkowa (pierwsza) wyświetlona wartość
drugi  - liczba wyświetlonych elementów
trzeci (`double`) - różnica pomiędzy kolejnymi elementami (“skok”)
> Dla `0, 5, 5` wyświetli `0, 5, 10, 15, 20` (zaczynamy od `0`, wyświetlamy `5` elementów a elementy różnią się od siebie o `5`)
> Dla `0, 6, 2.5` wyświetli `0.0, 2.5, 5.0, 7.5, 10.0, 12.5` (zaczynamy od `0`, wyświetlamy `6` elementów a elementy różnią się od siebie o `2.5`)
 */

public class zad25 {
    public static void main(String[] args) {
        forWyswietlSkok(0, 5, 5);
        forWyswietlSkok(0, 6, 2.5);
        System.out.println("____");
        whileWyswietlSkok(0, 5, 5);
        whileWyswietlSkok(0, 6, 2.5);
        System.out.println("____");
        doWhileWyswietlSkok(0, 5, 5);
        doWhileWyswietlSkok(0, 6, 2.5);
    }

    static void forWyswietlSkok(int start, int stop, double jump) {
        for (int i = start; i < stop; i++) {
            System.out.print(i * jump + " ,");
        }
        System.out.println();
    }

    static void whileWyswietlSkok(int start, int stop, double jump) {
        int i = start;
        while (i < stop) {
            System.out.print(i * jump + " ");
            i++;
        }
        System.out.println();
    }

    static void doWhileWyswietlSkok(int start, int stop, double jump) {
        do {
            System.out.print(start * jump + " ");
            start++;
        } while (start < stop);
        System.out.println();
    }
}
