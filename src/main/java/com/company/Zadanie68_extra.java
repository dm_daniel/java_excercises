package com.company;
/*
*ZADANIE #68 - EXTRA*
        Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca nową tablicę z liczbami w odwrotnej kolejności.
        Nie wolno utworzyć nowej tablicy - należy pracować na tej wejściowej.
        */

import java.util.Arrays;

public class Zadanie68_extra {
    public static void main(String[] args) {

        int[] tab = new int[]{1, 2, 3, 4, 5, -1, 6};
        System.out.println(Arrays.toString(odwrocTablice(tab)));
    }

    static int[] odwrocTablice(int[] tablica) {

        int zmienna = tablica.length - 1;
        int x = 0;

        for (int i = 0; i < tablica.length / 2; i++) {
            x = tablica[i];
            tablica[i] = tablica[zmienna];
            tablica[zmienna] = x;
            zmienna--;
        }
        return tablica;
    }
}