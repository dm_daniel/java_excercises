package com.company.zadanie170TypyGeneryczne.shape;

//Klasa opakowująca akceptuje tylko te klasy,
//które mają relacje z klasą Shape
//tzn. po niej dziedziczą lub implementują

public class ShapeBox<TYPE extends Shape> {

    public ShapeBox(TYPE shape) {
        this.shape = shape;
    }

    private TYPE shape;

    public TYPE getShape() {
        return shape;
    }

    //metoda klasy opakowującej
    public String getNameOfShape() {
        return shape.getName();     //mam dostęp do metod z klasy shape ! (getName)
    }
}
