package com.company.zadanie170TypyGeneryczne.shape;

public interface Shape {

    public String getName();

//implementacja domyślna dla namej metody,
// nie musimy jej implementować w innych klasach implementujących ten interface będzie ona jako domyślna ( od java 8)
    default void hello() {
        System.out.println("Hello,im shape");
    }
}
