package com.company.zadanie170TypyGeneryczne;

import com.company.zadanie170TypyGeneryczne.bezGenerycznych.Apple;
import com.company.zadanie170TypyGeneryczne.bezGenerycznych.AppleBox;
import com.company.zadanie170TypyGeneryczne.bezGenerycznych.Orange;
import com.company.zadanie170TypyGeneryczne.bezGenerycznych.OrangeBox;
import com.company.zadanie170TypyGeneryczne.interfface.FruitInterface;
import com.company.zadanie170TypyGeneryczne.interfface.FruitInterfaceBox;
import com.company.zadanie170TypyGeneryczne.methods.ReturnMethods;
import com.company.zadanie170TypyGeneryczne.methods.VoidMethods;
import com.company.zadanie170TypyGeneryczne.object.ObjectFruitBox;
import com.company.zadanie170TypyGeneryczne.shape.*;
import com.company.zadanie170TypyGeneryczne.simplegeneric.BiggerFruitBox;
import com.company.zadanie170TypyGeneryczne.simplegeneric.FruitBox;
import com.sun.org.apache.xpath.internal.operations.Or;

public class Zadanie170 {
    public static void main(String[] args) {
        /**Dedykowana klasa opakowująca(OrangeBox,AppleBox
         dla każdej z klas "w środku" (Orange,Apple)
         co powoduje duplikację rozwiązań w metodzie main*/
//        method1();
        /**
         *Klasa opakowująca akceptuje obiekty klasy Object,
         *co powoduje "zaakceptowanie także obiektów klas
         *
         *  Orange i Apple ( które po niej dzedziczą).
         *Akceptowane są także WSZYSTKIE obiekty klas,
         * które dziedziczą po klasie Object.
         *
         * Czyli wewnątrz klasy opakowującej jesteśmy w stanie umieścić
         * ABSOLUTNIE dowolny typ (np String lub Byte)
         */
//        method2();
        /**
         *Klasa opakowująca akceptuje tylko i wyłącznie obiekty tych klas
         * które implementują interfejs "FruitInterface"
         */
//        method3();
        /**
         *Klasa opakowująca jest klasą generyczną, czyli przy deklaracji
         * musimy jawnie podać, jaki typ elementów będzie znajdować się w środku.
         */
//        method4();
        /**
         *Klasa opakowująca jest klasą generyczną.
         *
         * Przyjmuje dwa obiekty klas, które musimy podać przy tworzeniu obiektu,
         * czyli
         *  BiggerFruitBox<String,Integer >
         *
         */
//        method5();
/**
 *Sprawdzamy, że generyczne klasy są w stanie przyjąć
 * tylko te obiekty, których typ został podany przy deklaracji obiektu
 * czyli dla:
 * np List<Integer> a = new ArrayList<>();
 * mogę dodać obiekty klasy Integer oraz obiekty wszystkich klas dziedziczących po klasie Integer
 * Nie jesteśmy w stanie dodać obiektów żadnej "nadklasy"
 */
//        method6();
        /**
         *Metody, które przyjmują typy generyczne
         */
//        method7();
        /**
         * Metody które w sygnaturze mają typ generyczny
         */
        method8();
    }

    private static void method8() {
        String imie = "Natalia";
        ReturnMethods.method5(imie);
        ReturnMethods.method5(4);
        ReturnMethods.method5(true);

        String napis = ReturnMethods.method6("Pies");
        int liczba = ReturnMethods.method6(100);

        ReturnMethods.method7(new Circle());
        ReturnMethods.method7(new Square());

        ReturnMethods.method8("Niedziela", false);

        boolean result = ReturnMethods.method9(new Rectangle(), new Orange());       //do metody jestem w stanie przekazać 2 abstrakcyjne, rozłączne od siebie typy

    }

    private static void method7() {
        ShapeBox<Circle> circleBox = new ShapeBox<>(new Circle());
        ShapeBox<Rectangle> rectangleBox = new ShapeBox<>(new Rectangle());
        ShapeBox<Square> squareBox = new ShapeBox<>(new Square());

        //akceptowana klasa jest podana w sygnaturze metody
//       VoidMethods.method1(rectangleBox);//nie zadziała
//       VoidMethods.method1(squareBox);  //nie zadziała
        VoidMethods.method1(circleBox);

        //akceptowany jest dowolny ShapeBox
        VoidMethods.method2(rectangleBox);
        VoidMethods.method2(squareBox);
        VoidMethods.method2(circleBox);

        //Akceptowane są klasy które dziedziczą po rectangle
        VoidMethods.method3(rectangleBox);
        VoidMethods.method3(squareBox);
//        VoidMethods.method3(circleBox);    nie zadziała

        //Akceptowane są NADKLASY klasy Rectangle
        VoidMethods.method4(rectangleBox);
//        VoidMethods.method4(squareBox);   nie zadziała
//        VoidMethods.method4(circleBox);   nie zadziała


    }

    private static void method6() {
        //Tworzymy nowy obiekt
        Rectangle rectangle = new Rectangle();

//klasa opakowująca przy tworzeniu której musimy określić typ elementów w środku
        ShapeBox<Rectangle> box1 = new ShapeBox<>(rectangle);

        //wywołujemy metodę na elemencie który jest  w srodku
        System.out.println(box1.getNameOfShape());

        Circle circle = new Circle();
        ShapeBox<Circle> box2 = new ShapeBox<>(circle);

        //Operacja prawidłowa
        //Dodajemy element bardziej szczegółowy,
        //czyli POSIADA cechy rodzica (bardziej ogólne)
        ShapeBox<Rectangle> box3 = new ShapeBox<>(new Rectangle());
        ShapeBox<Rectangle> box4 = new ShapeBox<>(new Square());

        //Opreracja nieprawidłowa
        //Próbujemy dodać element bardziej ogólny
        //gdy wymagany jest szczegółowy
        //ShapeBox<Square> box5 = new ShapeBox<>(new Rectangle());
    }

    private static void method5() {
        String imie = "Kuba";
        Integer wiek = 38;

        //Utworzenie obiektu generycznej klasy opakowującej
        //Elementami w "środku mogą być TYLKO instancje klas (czyli nie akceptuje int, void, ale Integer,Void)
        BiggerFruitBox<String, Integer> box = new BiggerFruitBox<>(imie, wiek);

        //Wyświetlenie elementów "ze środka"
        System.out.println(box.getFirst());
        System.out.println(box.getSecond());

//typy mogą być takie same ( nie zaweżaliśmy nigdzie typów)
        BiggerFruitBox<String, String> box2 = new BiggerFruitBox<>("ALA", "MA kota");

        System.out.println(box2.getFirst());
        System.out.println(box2.getSecond());
    }

    private static void method4() {

        //Tworzymy obiekt
        Apple apple = new Apple();

        //Utworzenie (generycznej) klasy opakowującej
        FruitBox<Apple> box = new FruitBox<Apple>(apple);

        //Pobranie elementu "ze środka" obiektu opakowującego
        Apple fruit = box.getFruit();

//        Orange orange = new Orange();
//        box.setFruit(orange); BŁEDNA OPERACJA !!! wcześniej podałem że przyjmę <APPLE>

        Orange orange = new Orange();
        FruitBox<Orange> box2 = new FruitBox<>(orange);

        Boolean czyZima = true;
        FruitBox<Boolean> box3 = new FruitBox<>(czyZima);
    }

    private static void method3() {
        //Tworzymy nowy obiekt
        Apple apple = new Apple();

        //Tworzymy nowy obiekt klasy opakowującej
        FruitInterfaceBox box = new FruitInterfaceBox(apple);

        //Wyświetlenie nazwy klasy elementu, który znajduje się w środku.
        System.out.println(box.getFruit().getClass().getSimpleName());

        //pobranie elementu ze środka
        FruitInterface fruit = box.getFruit();
        //Rzutujemy zwrócony obiekt na klasę"Apple"
        Apple appleFruit = (Apple) fruit;

//        String info = "Ala ma kota";
//        box.setFruit(info);           OPERACJA BŁĘDNA!!!, dość mocno ograniczyliśmy zbiór elementów w środku

        //Podmiana elementów w środku na obiekt klasy Orange
        Orange orange = new Orange();
        box.setFruit(orange);
        //wyświetlenie nazwy klasy elementu, który jest w środku
        System.out.println(box.getFruit().getClass().getSimpleName());

        //obiekt klasy Object nie jest akceptowany
        //ponieważ nie implementuje wybranego interfejsu FruitInterfaceBox
        //  box.setFruit(new Object());

    }

    private static void method2() {

        Apple apple = new Apple();      //Tworzymy nowy obiekt
        ObjectFruitBox box = new ObjectFruitBox(apple); //klasa opakowująca
        System.out.println(box.getFruit() instanceof Apple);//sprawdzam czy obiekt w środku jest instancją klasy Apple ->true

        //Tworzymy nowy obietk
        Orange orange = new Orange();
        //zmieniamy element w środku
        box.setFruit(orange);   //używam settera
        //sprawdzamy czy obiekt w środku jest instancją (obiektem) klasy Orange
        System.out.println(box.getFruit() instanceof Apple);    //false
        System.out.println(box.getFruit() instanceof Orange);       //true

        //Getterem pobieram element "ze środka"
        Object fruit = box.getFruit();
        //pobieram nazwę klasy elementu ze środka (get simple name zwracaj nazwę klasy)
        String className = fruit.getClass().getSimpleName(); //naowocu chcę pograć nazwę klasy
        System.out.println(className);

        //rzutowanie by zwracany obiekt był traktowany jak obiekt klasy Orange
        Orange orangeFruit = (Orange) box.getFruit(); // spróbuj traktować to jako obiekt tej klasy

        //Tworzymy nową zmienną, którą umieścimy w pudełku
        String info = "Ala ma kota";
        //zmiana elementu w środku
        box.setFruit(info); //wymagane są cechy conajmniej klasy Object (String dziedziczy po object więc przyjmuje)
        //pobieranie elementu ze środka
        Object fruit2 = box.getFruit();
        //pobieram nazwę klasy elementu ze środka
        String className2 = fruit2.getClass().getSimpleName();
        System.out.println(className2);

        //rzutujemy element ze środka na klasę String
        String info2 = (String) box.getFruit();
        System.out.println(info2);

    }

    private static void method1() {

        Apple apple = new Apple();
        AppleBox box = new AppleBox(apple);     //musimy wstawic taki obiekt jak powiedziano w konstruktorze applebox
        box.getApple();

        Orange orange = new Orange();
        OrangeBox box2 = new OrangeBox(orange);     //dla każdego typu musze stworzyć ponownie, duplikacja rozwiązań
        box2.getOrange();
    }
}
