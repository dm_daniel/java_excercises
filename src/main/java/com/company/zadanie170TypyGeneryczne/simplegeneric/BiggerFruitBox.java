package com.company.zadanie170TypyGeneryczne.simplegeneric;

public class BiggerFruitBox<A, B> {
    private A first;
    private B second;

    public BiggerFruitBox(A pierwszy, B drugi) {
        this.first = pierwszy;
        this.second = drugi;
    }

    public A getFirst() {
        return first;
    }

    public B getSecond() {
        return second;
    }
}
