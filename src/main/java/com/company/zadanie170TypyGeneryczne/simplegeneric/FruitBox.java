package com.company.zadanie170TypyGeneryczne.simplegeneric;

/**
 * przyjelo się, że
 * nie znamy na poczatku jaki typ będzie w <> więc wpisujemy cos tymczasowo:
 * T-typ
 * K-Klucz
 * V - wartość
 * E - element(kolekcji)
 * N - liczba
 */
public class FruitBox<TYP> {

    private TYP fruit;

    public FruitBox(TYP fruit) {
        this.fruit = fruit;
    }

    public TYP getFruit() {
        return fruit;
    }

    public void setFruit(TYP fruit) {
        this.fruit = fruit;
    }
}
