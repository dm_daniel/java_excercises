package com.company.zadanie170TypyGeneryczne.object;

//obejście żeby zaakceptować różne type (Apple Orange i inne, które dziedziczą po Object)
public class ObjectFruitBox {

    private Object fruit;

    public ObjectFruitBox(Object fruit) {
        this.fruit = fruit;
    }

    public Object getFruit() {
        return fruit;
    }

    public void setFruit(Object fruit) {
        this.fruit = fruit;
    }
}
