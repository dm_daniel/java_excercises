package com.company.zadanie170TypyGeneryczne.methods;

import com.company.zadanie170TypyGeneryczne.bezGenerycznych.Orange;
import com.company.zadanie170TypyGeneryczne.shape.Shape;


public class ReturnMethods {

    //metoda jest w stanie przyjąć dowolny typ
    public static <TYPE> void method5(TYPE element) {
        System.out.println("wrzucono: " + element.getClass().getSimpleName());
    }

    //metoda zwraca ten sam typ, który przyjmuje
    public static <TYPE> TYPE method6(TYPE element) {
        return element;
    }

    //zwróci jakiś typ który dziedziczy po shape
    public static <T extends Shape> T method7(T shape) {
        shape.hello();
        return shape;
    }

    //przekazuje do metody 2 dowolne typy danych i je zwracam
    public static <A> A method8(A first, A second) {
        System.out.println("Do metody przekazano zmienne typu:");
        System.out.println(first.getClass().getSimpleName());
        System.out.println("oraz");
        System.out.println(second.getClass().getSimpleName());

        return first;
    }

    public static <T extends Shape, K extends Orange> boolean method9(T first, K second) {
        return first.equals(second);
    }

}
