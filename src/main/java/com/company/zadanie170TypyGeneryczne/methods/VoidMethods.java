package com.company.zadanie170TypyGeneryczne.methods;

import com.company.zadanie170TypyGeneryczne.shape.Circle;
import com.company.zadanie170TypyGeneryczne.shape.Rectangle;
import com.company.zadanie170TypyGeneryczne.shape.ShapeBox;

public class VoidMethods {

    //wymagamy obiekt klasy "ShapeBox" z "Circle" "w środku"
    public static void method1(ShapeBox<Circle> box) {
        System.out.println(box.getNameOfShape());
    }

    //ShapeBox z czyms w srodku
    //nie weryfikujemy co jest w srodku
    //ograniczeniem zajmuje sie klasa ShapeBox
    public static void method2(ShapeBox<?> box) {

    }

    //wymagana jest klasa dziedzicząca po Rectangle
    public static void method3(ShapeBox<? extends Rectangle> box) {

    }

    //Akceptujemy obiekty klasy "Rectangle" i wszystkie jej nadklasy (bo super)
    public static void method4(ShapeBox<? super Rectangle> box) {

    }

}

