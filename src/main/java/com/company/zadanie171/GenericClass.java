package com.company.zadanie171;

//gdzieś w obrębie tej klasy typy A,B i C będzie używany
public class GenericClass<A,B,C> {
    private A first;
    private B second;
    private C third;

    public GenericClass(A first, B second, C third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public String toString() {
        return "GenericClass{" +
                "first=" + first +
                ", second=" + second +
                ", third=" + third +
                '}';
    }
}
