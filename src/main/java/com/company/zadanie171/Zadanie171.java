package com.company.zadanie171;

import java.util.Arrays;
import java.util.List;

public class Zadanie171 {
    public static void main(String[] args) {

        List<GenericClass<String, Integer, Boolean>> list = Arrays.asList(
                new GenericClass<>("Adam", 12, false),
                new GenericClass<>("Janek", 23, false),
                new GenericClass<>("Ola", 33, true),
                new GenericClass<>("Maria", 21, true)
        );

        list.forEach(System.out::println);

        List<GenericClass<Float, Integer, Integer>> list2 = Arrays.asList(
                new GenericClass<>(1F, 23, 43),
                new GenericClass<>(4F, 5, 6),
                new GenericClass<>(10.234F, 5, 6)
        );
        list2.forEach(System.out::println);

    }
}
